-format alert list target values to device locale
-display smaller price values with more decimals
-check if alert has been deleted in the background when opening it