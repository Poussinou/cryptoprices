Crypto Prices lets you track prices of all cryptocurrencies like Bitcoin, Ethereum, XRP, and Litecoin. You can also view detailed information about every coin, like price history, market cap, and volume.

<b>Supports over 10000 Cryptocurrencies</b>
View prices of coins and tokens like Bitcoin (BTC), Ethereum (ETH), Bitcoin Cash (BCH), Ethereum Classic (ETC), Litecoin (LTC), Dogecoin (DOGE), Stellar (XLM), Tron (TRX) and many more.

<b>Detailed Statistics</b>
Get statistics to every coin, like market cap, volume, 24H-High and Low, and an interactive price chart over different time periods.

<b>Your Favorites</b>
Set coins as favorites to track all prices of your crypto portfolio. Sort favorites in any order you like to view prices even faster.

<b>Your Portfolios</b>
Add coins from all your real or imaginary crypto portfolios and watch their value change.
(This is NOT a crypto wallet. All added coins are imaginary.)

<b>Price alerts</b>
Create your own alerts for all coins and get notified of large price changes of your favorites.

<b>Quick Search</b>
Search through all available coins by name or symbol.

<b>56 Supported Fiat Currencies</b>
US Dollar (USD), Euro (EUR), British Pound (GBP), Australian Dollar (AUD), Canadian Dollar (CAD), Swiss Franc (CHF), Chinese Yuan (CNY), Japanese Yen (JPY), New Zealand Dollar (NZD) and Russian Ruble (RUB) and more.

<b>Feature Overview</b>
• List of all cryptocurrencies 
• Sort coins by marketcap, volume etc.
• Add coins as favorites
• Sort favorites in custom order
• View detailed statistics and price history
• Imaginary portfolios
• Create price alerts for coins
• Get notification on large price movements
• Change the currency of the prices shown
• Password lock
• Search
• Dark mode and color customization

<b>Free and Open source</b>
Crypto Prices is open source and licensed under GPLv3. The source code is available here: https://gitlab.com/cl0n30/cryptoprices
