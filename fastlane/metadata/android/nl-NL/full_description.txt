Cryptoprijzen laat je de prijzen volgen van alle cryptovaluta's zoals Bitcoin, Ethereum, XRP en Litecoin. Je kan ook gedetailleerde informatie bekijken over elke munt zoals prijsgeschiedenis, marktkapitalisatie en volume.

<b>Ondersteunt meer dan 10000 cryptovaluta's</b>
Bekijk prijzen van munten en tokens zoals Bitcoin (BTC), Ethereum (ETH), Bitcoin Cash (BCH), Ethereum Classic (ETC), Litecoin (LTC), Dogecoin (DOGE), Stellar (XLM), Tron (TRX) en veel meer.

<b>Gedetailleerde statistieken</b>
Toegang tot statistieken van elke munt zoals marktkapitalisatie, volume, 24-uurshoog en -laag en een interactieve prijsgrafiek voor verschillende tijdsschalen.

<b>Je favorieten</b>
Markeer munten als favoriet om prijzen bij te houden voor jouw cryptoportefeuille. Sorteer favorieten in de volgorde die jij graag wil om sneller prijzen te bekijken.

<b>Je portefeuille</b>
Voeg munten toe van je echte of denkbeeldige portefeuille en zie ze van waarde veranderen.
(Dit is NIET een cryptoportemonnee. Alle toegevoegde munten zijn denkbeeldig.)

<b>Prijsalarm</b>
Maak je eigen alarm voor alle munten en ontvang een notificatie als er grote prijswijzigingen zijn voor jouw favorieten.

<b>Snel zoeken</b>
Zoek door alle beschikbare munten op naam of symbool.

<b>56 ondersteunde fiatvaluta's</b>
Amerikaanse dollar (USD), euro (EUR), Britse pond (GBP), Australische dollar (AUD), Canadese dollar (CAD), Zwitserse frank (CHF), Chinese yuan (CNY), Japanse yen (JPY), Nieuw-Zeelandse dollar (NZD) en Russische roebel (RUB).

<b>Overzicht van features</b>
• Lijst van alle cryptovaluta's
• Sorteer munten op marktkapitalisatie, volume etc.
• Voeg munten toe aan favorieten
• Sorteer munten op maatwerkvolgorde
• Bekijk gedetailleerde statistieken en prijsgeschiedenis
• Denkbeeldige portefeuille
• Maak voor munten een prijsalarm aan
• Ontvang notificaties van grote prijswijzigingen
• Wijzig de valuta waarin prijzen worden getoond
• Wachtwoord slot
• Zoek
• Donkere modus en kleuraanpassingen

<b>Vrije open source</b>
Cryptoprijzen is open source en heeft de licentie GPLv3. De broncode is hier beschikbaar: https://gitlab.com/cl0n30/cryptoprices
