import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/coin_detail.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/models/market_data.dart';

///Abstract class that defines the api interaction functions
abstract class ApiInteraction {

  ///Return true if API is online
  Future<bool> checkAPIStatus();

  ///Returns a list of all available coins
  Future<List<AvailableCoin>> getAvailableCoins();

  ///Returns a list of supported currencies
  Future<List<String>> getCurrencyList();

  ///Returns the current price of a coin in the specified currency
  ///and the 24H change if changePercentage is true.
  ///Map keys are "price" and "changePercentage"
  Future<Map<String, double>> getCoinPrice(String coinId, String currency, {bool changePercentage = false});

  ///Returns the price of the given coin on the given day at 0:00
  Future<double> getCoinPriceAtDate(String coinId, String currency, DateTime date);

  ///Returns the market data of coins in a specified currency. List is ordered with values from
  ///OrderBy class. Returns one specified page with the specified amount of entries
  Future<List<Coin>> getMarketDataPage(String currency, String order, int entriesPerPage, int pageNo);

  ///Return Coin object with detailed information to a specified currency
  Future<CoinDetail> getCoinData(String coinID, String currency);

  ///Returns the data for multiple coins to a specified currency
  Future<List<CoinDetail>> getMultipleCoinData(List<String> coinIds, currency);

  ///Returns historical market chart of a coin for a specific currency.
  ///TimePeriod values are provided by marketDataDays list
  ///Minutely data will be used for duration within 1 day, (<= 1 day)
  ///Hourly data will be used for duration between 1 day and 90 days,
  ///Daily data will be used for duration above 90 days.
  ///Date in Unix time
  Future<List<MarketData>> getHistoricalMarketChart(String coinID, String currency);

  ///Returns the market data of a coin for a specified period of days
  Future<MarketData> getMarketDataDays(String coinId, String currency, String days);

  ///Returns the market data of a list of coins for a specified period of days
  Future<List<MarketData>> getMultipleMarketDataDays(List<String> coinIds, String currency, String days);

  ///Returns the global crypto market info
  Future<GlobalMarketInfo> getGlobalMarketInfo(String currency);

  ///Combines OrderBy and OrderDirection into a String for network requests
  String getOrderString(OrderBy order, OrderDirection orderDirection);
}

///Contains the keys to the different time periods of the market data of a coin
const List<String> marketDataDays = [
  Constants.MARKETDATAKEYDAY, //minutely data
  Constants.MARKETDATAKEYMONTH, //hourly data
  Constants.MARKETDATAKEYMAX //daily data
];

///Order of the lists
enum OrderBy {
  market_cap,
  volume,
  price_change,
  name,
  symbol
}

enum OrderDirection {
  asc,
  desc
}
