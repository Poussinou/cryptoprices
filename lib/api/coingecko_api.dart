import 'dart:convert';
import 'dart:io';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/coin_detail.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'api_interaction.dart';

///Implementation of ApiInteraction for the CoinGecko API
class CoinGeckoAPI extends ApiInteraction {
  final String _hostURL = "api.coingecko.com";
  final String _apiBaseURL = "/api/v3/";

  //use Singleton
  static final CoinGeckoAPI _instance = CoinGeckoAPI._internal();
  factory CoinGeckoAPI() => _instance;

  CoinGeckoAPI._internal();

  @override
  Future<bool> checkAPIStatus() async {
    try {
      final response = await http.get(Uri.https(_hostURL, _apiBaseURL + "ping"));
      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }
      return true;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<AvailableCoin>> getAvailableCoins() async {
    try {
      final response = await http.get(Uri.https(_hostURL, _apiBaseURL + "coins/list"));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      return compute(parseAvailableCoins, response.body);
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<String>> getCurrencyList() async {
    try {
      final response = await http.get(Uri.https(_hostURL, _apiBaseURL + "simple/supported_vs_currencies"));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      List<String> list = [];

      list = jsonDecode(response.body);

      return list;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<Map<String,double>> getCoinPrice(String coinId, String currency, {bool changePercentage = false}) async {
    try {
      final response = await http.get(Uri.https(
        _hostURL,
        _apiBaseURL + "simple/price",
        {
          "ids": coinId,
          "vs_currencies": currency,
          "include_24hr_change": changePercentage.toString()
        }
      ));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      Map<String, double> map = Map();
      var json = jsonDecode(response.body);

      num price = json[coinId][currency] ?? 0;
      map[Constants.APIGETCOINPRICEKEY] = price.toDouble();

      num priceChange = json[coinId]["$currency\_24h_change"] ?? 0;
      map[Constants.APIGETCOINCHANGEPERCENTAGEKEY] = priceChange.toDouble();

      return map;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<double> getCoinPriceAtDate(String coinId, String currency, DateTime date) async {
    try {
      double price = 0;
      if (date.isSameDate(DateTime.now())) { //if date is now use getCoinPrice
        final response = await getCoinPrice(coinId, currency);
        price = response[Constants.APIGETCOINPRICEKEY]!;
      } else {
        final response = await http.get(Uri.https(
            _hostURL,
            _apiBaseURL + "coins/$coinId/history",
            {
              "date": "${date.day}-${date.month}-${date.year}",
              "localization": "false"
            }
        ));

        if (response.statusCode == 429) { //too many requests
          throw RateLimitationException("Too many requests");
        }

        Map<String, dynamic> data = jsonDecode(response.body);

        if (data["market_data"] == null) //no data for that date
          return -1;

        price = double.parse(data["market_data"]["current_price"][currency].toString());
      }

      return Util.roundToDecimals(price);
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<Coin>> getMarketDataPage(String currency, String order, int entriesPerPage, int pageNo) async {
    try {
      final response = await http.get(Uri.https(
          _hostURL,
          _apiBaseURL + "coins/markets",
          {
            "vs_currency": currency,
            "order": order,
            "per_page": entriesPerPage.toString(),
            "page": pageNo.toString(),
            "sparkline": "false"
          }
      ));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      List<Coin> list = [];

      List data = jsonDecode(response.body);

      for (int i = 0; i < data.length; i++) {
        Coin coin = Coin.fromJson(data[i]);
        coin.lastUpdated = DateTime.now();
        list.add(coin);
      }
      return list;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<CoinDetail> getCoinData(String coinID, String currency) async {
    try {
      final response = await http.get(Uri.https(_hostURL, _apiBaseURL + "coins/$coinID", { "tickers" : "false"}));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      return CoinDetail.fromJson(jsonDecode(response.body), currency)..lastUpdated = DateTime.now();
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<CoinDetail>> getMultipleCoinData(List<String> coinIds, currency) async {
    try {
      var client = http.Client();
      List<http.Response> responses = await Future.wait(coinIds.map((id) => client.get(Uri.https(_hostURL, _apiBaseURL + "coins/$id", { "tickers" : "false"}))));
      client.close();

      return responses.map((response) {
        if (response.statusCode == 429) { //too many requests
          throw RateLimitationException("Too many requests");
        }
        CoinDetail coin = CoinDetail.fromJson(jsonDecode(response.body), currency);
        coin.lastUpdated = DateTime.now();
        return coin;
      }).toList();
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<MarketData>> getHistoricalMarketChart(String coinID, String currency) async {
    try {
      var client = http.Client();
      List<http.Response> responses = await Future.wait(marketDataDays.map((e) => client.get(Uri.https(
          _hostURL,
          _apiBaseURL + "coins/$coinID/market_chart",
          {
            "vs_currency": currency,
            "days": e
          }
      ))
      ));
      client.close();

      List<String> responseBodies = responses.map((response) {
        if (response.statusCode == 429) { //too many requests
          throw RateLimitationException("Too many requests");
        }
        return response.body;
      }).toList();

      List<MarketData> parsed = await compute(parseMarketChart, responseBodies);
      parsed.forEach((value) {
        value.coinId = coinID;
        value.lastUpdated = DateTime.now();
        value.currency = currency;
      });
      return parsed;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<MarketData> getMarketDataDays(String coinId, String currency, String days) async {
    try {
      final response = await http.get(Uri.https(
          _hostURL,
          _apiBaseURL + "coins/$coinId/market_chart",
          {
            "vs_currency": currency,
            "days": days
          }
      ));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      MarketData marketData = await compute(parseMarketData, response.body);
      marketData.coinId = coinId;
      marketData.timePeriodDays = days.toString();
      marketData.lastUpdated = DateTime.now();
      marketData.currency = currency;
      return marketData;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<List<MarketData>> getMultipleMarketDataDays(List<String> coinIds, String currency, String days) async {
    try {
      final client = http.Client();
      final responses = await Future.wait(coinIds.map((coinId) => client.get(Uri.https(
          _hostURL,
          _apiBaseURL + "coins/$coinId/market_chart",
          {
            "vs_currency": currency,
            "days": days
          }
      ))
      ));
      client.close();

      List<String> responseBodies = responses.map((response) {
        if (response.statusCode == 429) { //too many requests
          throw RateLimitationException("Too many requests");
        }
        return response.body;
      }).toList();

      List<MarketData> parsed = await compute(parseMultipleMarketData, responseBodies);
      for (int i = 0; i < parsed.length; i++) {
        parsed[i].coinId = coinIds[i];
        parsed[i].timePeriodDays = days;
        parsed[i].lastUpdated = DateTime.now();
        parsed[i].currency = currency;
      }

      return parsed;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  Future<GlobalMarketInfo> getGlobalMarketInfo(String currency) async {
    try {
      final response = await http.get(Uri.https(_hostURL, _apiBaseURL + "global"));

      if (response.statusCode == 429) { //too many requests
        throw RateLimitationException("Too many requests");
      }

      final json = jsonDecode(response.body);
      GlobalMarketInfo globalMarketInfo = GlobalMarketInfo.fromJson(json['data'], currency);
      return globalMarketInfo;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
  }

  @override
  String getOrderString(OrderBy order, OrderDirection orderDirection) {
    return describeEnum(order) + "_" + describeEnum(orderDirection);
  }
}

///Parses the json responses to a list of market data for different time periods
List<MarketData> parseMarketChart(List<String> responses) {
  List<MarketData> marketChart = [];
  for (int i = 0; i < responses.length; i++) {
    MarketData marketData = MarketData.fromJson(jsonDecode(responses[i]), "", "", marketDataDays[i], "");
    marketChart.add(marketData);
  }
  return marketChart;
}

///Parses the json response to a market data object
MarketData parseMarketData(String response) {
  return MarketData.fromJson(jsonDecode(response), "", "", "", "");
}

///Parses the json responses to a list of market data
List<MarketData> parseMultipleMarketData(List<String> responses) {
  List<MarketData> marketData = [];
  for (int i = 0; i < responses.length; i++) {
    MarketData data = MarketData.fromJson(jsonDecode(responses[i]), "", "", "", "");
    marketData.add(data);
  }
  return marketData;
}

///Parses the json response to a list of AvailableCoins
List<AvailableCoin> parseAvailableCoins(String responseBody) {
  List<AvailableCoin> list = [];

  List availableCoins = jsonDecode(responseBody); //list with all coins

  for (int i = 0; i < availableCoins.length; i++) {
    AvailableCoin coin = AvailableCoin.fromJson(availableCoins[i]);
    list.add(coin);
  }
  return list;
}