import 'package:crypto_prices/database/settings_dao.dart';
import 'package:flutter/cupertino.dart';

///Change Notifier that changes the language and notifies listeners
class LanguageChange extends ChangeNotifier {
  SettingsDAO _settingsDAO = SettingsDAO();
  String langCode = "";

  LanguageChange() {
    langCode = _settingsDAO.getLocaleHive();
  }

  ///Returns the current locale
  Locale? currentLocale() {
    if (langCode.isEmpty) {
      return null;
    }
    return Locale(langCode);
  }

  ///Changes the language of the app and notifies all listeners
  void changeLanguage(String locale) {
    _settingsDAO.setLocale(locale);
    langCode = locale;
    notifyListeners();
  }
}