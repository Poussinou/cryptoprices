import 'package:crypto_prices/models/available_coin.dart';
import 'package:hive/hive.dart';
import 'package:tuple/tuple.dart';

import '../constants.dart';
import 'coin_comparators.dart';

///DAO of the AvailableCoins box
class AvailableCoinsDao {

  ///Get one AvailableCoin by id
  AvailableCoin? getAvailableCoin(String id) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    return box.get(id); //if entry doesn't exist, returns null
  }

  ///Get one AvailableCoin by symbol (symbol may not be unique e.g. ADA has two entries)
  AvailableCoin? getAvailableCoinBySymbol(String symbol) {
    final coins = getAllAvailableCoins();
    AvailableCoin? coin = coins.where((element) => element.symbol == symbol).toList().last;
    return coin;
  }

  ///Get all AvailableCoins from the box
  List<AvailableCoin> getAllAvailableCoins() {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    List<AvailableCoin> list = [];
    for (int i = 0; i < box.length; i++) {
      if (box.getAt(i) is AvailableCoin) {
        list.add(box.getAt(i));
      }
    }
    return list;
  }

  ///Insert one AvailableCoin
  void insertAvailableCoin(AvailableCoin coin) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    box.put(coin.id, coin);
  }

  ///Insert multiple AvailableCoins from a list
  void insertAllAvailableCoins(List<AvailableCoin> list) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    Map<String, AvailableCoin> coins = Map();

    for (int i = 0; i < list.length; i++) {
      coins[list[i].id] = list[i];
    }
    box.putAll(coins);
  }

  ///Deletes all AvailableCoins. Return future to wait for completion
  Future<void> deleteAllAvailableCoins() async {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    await box.clear();
  }

  ///Updates the available coins by deleting unsupported coins and adding new ones.
  ///The search history is preserved, unless a searched coin is now unsupported.
  ///Returns a tuple with the updated list of all coins and the updated search history
  Tuple2<List<AvailableCoin>, List<AvailableCoin>> updateAllAvailableCoins(List<AvailableCoin> newCoins) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    Map<String, AvailableCoin> searchHistoryMap = _availableCoinListToMap(getSearchHistory());
    Map<String, AvailableCoin> newCoinsMap = _availableCoinListToMap(newCoins);

    searchHistoryMap.removeWhere((key, value) => !newCoinsMap.containsKey(key)); //delete unsupported coins
    newCoinsMap.addAll(searchHistoryMap);

    box.clear().then((value) {
      box.putAll(newCoinsMap);
    });

    return Tuple2<List<AvailableCoin>, List<AvailableCoin>>(
      _mapToAvailableCoinList(newCoinsMap),
      _mapToAvailableCoinList(searchHistoryMap)
    );
  }

  ///Adds a coin to the search history
  void addToSearchHistory(AvailableCoin coin) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    AvailableCoin searchedCoin = box.get(coin.id);
    List<AvailableCoin> history = getSearchHistory();

    //delete last entry in history
    if (searchedCoin.lastSearched == null && history.length == Constants.MAXSEARCHHISTORYLENGTH) {
      AvailableCoin last = history.last;
      last.lastSearched = null;
      last.save();
    }

    searchedCoin.lastSearched = DateTime.now();
    searchedCoin.save();
  }

  ///Returns the search history sorted by last search date
  List<AvailableCoin> getSearchHistory() {
    List<AvailableCoin> history = getAllAvailableCoins();
    history = history.where((e) => (e).lastSearched != null).toList();

    return history..sort(CoinComparators.lastSearched);
  }

  ///deletes one coin from the search history
  void deleteSearchedCoin(String coinId) {
    var box = Hive.box(Constants.AVAILABLECOINSBOXNAME);
    AvailableCoin coin = box.get(coinId);
    coin.lastSearched = null;
    coin.save();
  }

  ///Deletes the search history
  void deleteHistory() {
    List<AvailableCoin> history = getAllAvailableCoins();
    history = history.where((e) => (e).lastSearched != null).toList();
    history.map((e) {
      e.lastSearched = null;
      e.save();
    });
  }

  ///Converts a list of AvailableCoin to a map with the ids as keys
  Map<String, AvailableCoin> _availableCoinListToMap(List<AvailableCoin> coins) {
    Map<String, AvailableCoin> map = Map();
    coins.map((e) {
      map[e.id] = e;
    }).toList();

    return map;
  }

  ///Converts a <String, AvailableCoin> map to a list of the AvailableCoin values
  List<AvailableCoin> _mapToAvailableCoinList(Map<String, AvailableCoin> map) {
    List<AvailableCoin> list = [];
    map.forEach((key, value) { list.add(value); });
    return list;
  }
}