import 'package:crypto_prices/database/market_data_dao.dart';
import 'package:crypto_prices/models/coin_detail.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../constants.dart';

///The DAO of the CoinDetailBox
class CoinDetailDao {
  ///Returns one CoinDetail with market data by id
  CoinDetail? getCoinDetail(String id) {
    var box = Hive.box(Constants.COINDETAILBOXNAME);
    return box.get(id); //if entry doesn't exist, returns null
  }

  ///Inserts one CoinDetail
  void insertCoinDetail(CoinDetail coin) {
    var box = Hive.box(Constants.COINDETAILBOXNAME);
    box.put(coin.id, coin);
  }

  ///Inserts one CoinDetail in the CoinDetailBox and its market data in the MarketDataBox
  void insertCoinDetailWithMarketChart(CoinDetail coin, List<MarketData> marketChart) {
    var box = Hive.box(Constants.COINDETAILBOXNAME);
    final marketDataDao = MarketDataDao();
    marketDataDao.insertMultipleMarketData(marketChart);

    coin.marketChart = HiveList<MarketData>(Hive.box(Constants.MARKETDATABOXNAME), objects: marketChart);
    box.put(coin.id, coin);
  }

  ///Sets if the currency for the coins has been changed (in CoinDetailScreen)
  void setCurrencyChanged(String coinId, bool changed) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    box.put(coinId + Constants.COINDETAILCURRENCYCHANGEDKEY, changed);
  }

  ///Returns if the currency for the coins has been changed (in CoinDetailScreen)
  bool getCurrencyChanged(String coinId) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    return (box.get(coinId + Constants.COINDETAILCURRENCYCHANGEDKEY)) == null ? true : box.get(coinId + Constants.COINDETAILCURRENCYCHANGEDKEY);
  }
}