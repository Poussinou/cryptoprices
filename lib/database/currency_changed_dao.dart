import 'package:crypto_prices/constants.dart';
import 'package:hive/hive.dart';

///DAO of the CurrencyChanged box
class CurrencyChangedDao {

  ///Sets if the currency has changed for all widgets
  void setAllCurrencyChanged(bool changed) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    for ( int i = 0; i < box.length; i++) {
      box.putAt(i, changed);
    }
  }
}