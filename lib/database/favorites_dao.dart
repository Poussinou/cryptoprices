import 'package:crypto_prices/database/large_move_alert_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/large_move_alert.dart';
import 'package:crypto_prices/util/workmanager_handler.dart';
import 'package:hive/hive.dart';

import '../constants.dart';

///DAO of the Favorites box
class FavoritesDao {
  ///Returns one FavoriteCoin by id
  FavoriteCoin? getFavorite(String id) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    return box.get(id); //if entry doesn't exist, returns null
  }

  ///Returns all FavoriteCoins from the box
  List<FavoriteCoin> getAllFavorites() {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    List<FavoriteCoin> list = [];
    for (int i = 0; i < box.length; i++) {
      if (box.getAt(i) is FavoriteCoin) {
        list.add(box.getAt(i));
      }
    }
    return list;
  }

  ///Returns the number of entries in the box
  int favoritesCount() {
    return Hive.box(Constants.FAVORITESBOXNAME).length;
  }

  ///Returns a stream that listens for changes to the FavoritesBox
  Stream<BoxEvent> getAllFavoritesWatcher() {
    return Hive.box(Constants.FAVORITESBOXNAME).watch();
  }

  ///Inserts one FavoriteCoin and creates a large move alert
  void insertFavorite(FavoriteCoin fav) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    if (!box.containsKey(fav.id))
      createLargeMoveAlert(fav);
    box.put(fav.id, fav);
  }

  ///Creates a LargeMoveAlert for the given FavoriteCoin, if one doesn't already exists
  void createLargeMoveAlert(FavoriteCoin fav) {
    if (LargeMoveAlertDAO().getLargeMoveAlertByCoin(fav.id) != null)
      return;

    LargeMoveAlert alert = LargeMoveAlert(
      coinId: fav.id,
      coinName: fav.name,
      coinSymbol: fav.symbol,
      currency: SettingsDAO().getCurrencyHive(),
      lastNotificationSent: DateTime.now()
    );

    WorkmanagerHandler().registerLargeMoveAlert(alert);
  }

  ///Inserts multiple FavoriteCoins from a list at the bottom of the list.
  ///Creates a new large move alert for every FavoriteCoin.
  ///If favorites already contain one of the new entries it is skipped
  void insertAllFavorites(List<FavoriteCoin> list) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    Map<String, FavoriteCoin> ids = Map();

    int newListPositionOffset = 0;
    for (int i = 0; i < list.length; i++) {
      if (box.containsKey(list[i].id))
        continue;

      list[i].favoriteListPosition = box.length + newListPositionOffset;
      ids[list[i].id] = list[i];
      createLargeMoveAlert(list[i]);

      newListPositionOffset++;
    }
    box.putAll(ids);
  }

  ///increments the position indices of all entries between the new item position
  ///and the old item position.
  ///Used when moving an item up in the list
  void moveDownAllBetween(int newPos, int oldPos) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    for (int i = 0; i < box.length; i++) {
      FavoriteCoin fav = box.getAt(i);
      if (fav.favoriteListPosition >= newPos && fav.favoriteListPosition < oldPos) {
        fav.favoriteListPosition++;
        fav.save();
      }
    }
  }

  ///decrements the position indices of all entries between the old item position
  ///and the new item position.
  ///Used when moving an item down in the list
  void moveUpAllBetween(int oldPos, int newPos) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    for (int i = 0; i < box.length; i++) {
      FavoriteCoin fav = box.getAt(i);
      if (fav.favoriteListPosition > oldPos && fav.favoriteListPosition <= newPos) {
        fav.favoriteListPosition--;
        fav.save();
      }
    }
  }

  ///Deletes one FavoriteCoin from the box by id and cancels its large move alert
  void deleteFavorite(String id) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    FavoriteCoin old = box.get(id);
    _decrementPositionIndices(old.favoriteListPosition);
    WorkmanagerHandler().deleteLargeMoveAlertAlertByCoinId(id);
    box.delete(id);
  }

  ///Deletes all FavoriteCoins
  void deleteAllEntries() {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    box.clear();
  }

  ///decrements the position indices of all entries below the deleted entry.
  ///Moves them up by one
  void _decrementPositionIndices(int oldIndex) {
    var box = Hive.box(Constants.FAVORITESBOXNAME);
    for (int i = 0; i < box.length; i++) {
      FavoriteCoin fav = box.getAt(i);
      if (fav.favoriteListPosition > oldIndex) {
        fav.favoriteListPosition--;
        fav.save();
      }
    }
  }

  ///Sets if the currency for the coins has been changed (in FavoriteList)
  void setCurrencyChanged(bool changed) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    box.put(Constants.FAVORITESCURRENCYCHANGEDKEY, changed);
  }

  ///Returns if the currency for the coins has been changed (in FavoriteList)
  bool getCurrencyChanged() {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    return (box.get(Constants.FAVORITESCURRENCYCHANGEDKEY)) == null ? true : box.get(Constants.FAVORITESCURRENCYCHANGEDKEY);
  }
}