import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:hive_flutter/hive_flutter.dart';

///DAO of the GlobalMarketInfo box
class GlobalMarketInfoDao {

  ///Gets the global market info
  GlobalMarketInfo? getGlobalMarketInfo() {
    var box = Hive.box(Constants.GLOBALMARKETINFOBOXNAME);
    return box.get(Constants.GLOBALMARKETINFOKEY);
  }

  ///Inserts new global market info
  void insertGlobalMarketInfo(GlobalMarketInfo globalMarketInfo) {
    var box = Hive.box(Constants.GLOBALMARKETINFOBOXNAME);
    box.put(Constants.GLOBALMARKETINFOKEY, globalMarketInfo);
  }
}