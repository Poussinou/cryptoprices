import 'dart:math';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../util/util.dart';
import 'settings_dao.dart';

///DAO of the PortfolioBox
class PortfolioDao {

  ///Returns one Portfolio by ID
  Portfolio? getPortfolio(int id) {
    var box = Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME);
    return box.get(id);
  }

  ///Returns all portfolios
  List<Portfolio> getAllPortfolios() {
    var box = Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME);
    return box.values.toList();
  }

  ///Returns a ValueListenable that listens for changes to the PortfolioBox
  ValueListenable<Box<Portfolio>> getAllPortfoliosWatcher() {
    return Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME).listenable();
  }

  ///Returns a ValueListenable that listens for changes to the given portfolio
  ValueListenable<Box<Portfolio>> getPortfolioWatcher(int portfolioId) {
    return Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME).listenable(keys: [portfolioId]);
  }

  ///Inserts one portfolio
  void insertPortfolio(Portfolio portfolio) {
    var box = Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME);

    if (portfolio.id == 0) {
      portfolio.id = Util.generateId();
    }

    SettingsDAO settingsDao = SettingsDAO();
    if (settingsDao.getDefaultPortfolioIdHive() == Constants.NODEFAULTPORTFOLIOID) {
      settingsDao.setDefaultPortfolioId(portfolio.id);
    }

    box.put(portfolio.id, portfolio);
  }

  ///Inserts multiple portfolios
  void insertMultiplePortfolios(List<Portfolio> portfolios) {
    var box = Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME);
    Map<int, Portfolio> map = {};

    portfolios.forEach((portfolio) {
      if (portfolio.id == 0) {
        portfolio.id = Util.generateId();
      }
      map[portfolio.id] = portfolio;
    });

    SettingsDAO settingsDao = SettingsDAO();
    if (settingsDao.getDefaultPortfolioIdHive() == Constants.NODEFAULTPORTFOLIOID) {
      settingsDao.setDefaultPortfolioId(portfolios.first.id);
    }

    box.putAll(map);
  }

  ///Deletes a portfolio and all connected wallets
  void deletePortfolio(int id) {
    var box = Hive.box<Portfolio>(Constants.PORTFOLIOBOXNAME);
    final portfolio = getPortfolio(id);
    if (portfolio == null) {
      return;
    }
    WalletDao walletDao = WalletDao();
    walletDao.deleteAllPortfolioWallets(id);

    box.delete(id);

    SettingsDAO settingsDao = SettingsDAO();
    if (getAllPortfolios().length == 0) {
      settingsDao.setDefaultPortfolioId(Constants.NODEFAULTPORTFOLIOID);
    } else if (settingsDao.getDefaultPortfolioIdHive() == portfolio.id){
      settingsDao.setDefaultPortfolioId(getAllPortfolios().first.id);
    }
  }

  ///Changes the currency of all portfolios that use the app currency
  void changePortfolioAppCurrency(String newCurrency) {
    final portfolios = getAllPortfolios().where((portfolio) => portfolio.useAppCurrency);
    portfolios.forEach((portfolio) {
      portfolio.currency = newCurrency;
      portfolio.save();
    });
  }
}