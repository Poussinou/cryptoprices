// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(count) =>
      "${Intl.plural(count, one: 'Alert', other: 'Alerts')}";

  static String m1(coinSymbol) => "Delete all ${coinSymbol} Alerts";

  static String m2(date, time) => "${date}, ${time}";

  static String m3(char) => "Invalid character: ${char}";

  static String m4(count) =>
      "${Intl.plural(count, one: '1 hour ago, ', other: '${count} hours ago, ')}";

  static String m5(time) => "Just now, ${time}";

  static String m6(count) =>
      "${Intl.plural(count, one: '1 minute ago, ', other: '${count} minutes ago, ')}";

  static String m7(time) => "Yesterday, ${time}";

  static String m8(coinSymbol) => "${coinSymbol} amount copied to clipboard";

  static String m9(marketCap, currencySymbol) =>
      "Global Market Cap: ${marketCap} ${currencySymbol}";

  static String m10(active) => "Active Cryptocurrencies: ${active}";

  static String m11(volume, currencySymbol) =>
      "24H Volume: ${volume} ${currencySymbol}";

  static String m12(marketCap, currencySymbol) =>
      "Global Market Cap: ${marketCap} ${currencySymbol}";

  static String m13(volume, currencySymbol) =>
      "Global 24H Volume: ${volume} ${currencySymbol}";

  static String m14(count) => "${count} minutes";

  static String m15(coinName, percentage, coinPrice, currencySymbol) =>
      "${coinName} Price changed ${percentage}% in the last 24H. Now at ${coinPrice} ${currencySymbol}";

  static String m16(coinName) => "Large ${coinName} Price Movement";

  static String m17(movementText, targetValue) =>
      "24H change ${movementText} ${targetValue}%";

  static String m18(movementText, targetValue, coinPrice, currencySymbol) =>
      "24H change ${movementText} ${targetValue}%. Now at ${coinPrice} ${currencySymbol}";

  static String m19(movementText, targetValue, currencySymbol) =>
      "Price ${movementText} ${targetValue} ${currencySymbol}";

  static String m20(coinName) => "${coinName} Price Alert";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutAuthorCountry": MessageLookupByLibrary.simpleMessage("Germany"),
        "aboutAuthorMail":
            MessageLookupByLibrary.simpleMessage("Write an email"),
        "aboutAuthorName": MessageLookupByLibrary.simpleMessage("Clone Apps"),
        "aboutChangelog": MessageLookupByLibrary.simpleMessage("Changelog"),
        "aboutCoinGeckoName":
            MessageLookupByLibrary.simpleMessage("CoinGecko API"),
        "aboutCoinGeckoWebsite":
            MessageLookupByLibrary.simpleMessage("Visit website"),
        "aboutGitLab": MessageLookupByLibrary.simpleMessage("GitLab"),
        "aboutGitLabText": MessageLookupByLibrary.simpleMessage(
            "View Source Code, report issues"),
        "aboutGroupAuthor": MessageLookupByLibrary.simpleMessage("Developer"),
        "aboutGroupCoinGecko":
            MessageLookupByLibrary.simpleMessage("Powered by"),
        "aboutGroupSupport": MessageLookupByLibrary.simpleMessage("Support"),
        "aboutLicenses": MessageLookupByLibrary.simpleMessage("Licenses"),
        "aboutLicensesText":
            MessageLookupByLibrary.simpleMessage("Licenses of libraries used"),
        "aboutSupportRate": MessageLookupByLibrary.simpleMessage("Rate"),
        "aboutSupportRateText": MessageLookupByLibrary.simpleMessage(
            "Leave a review in the Google Play Store"),
        "aboutVersion": MessageLookupByLibrary.simpleMessage("Version"),
        "addPortfolio": MessageLookupByLibrary.simpleMessage("Add portfolio"),
        "addTransaction":
            MessageLookupByLibrary.simpleMessage("Add transaction"),
        "addTransactionUrl":
            MessageLookupByLibrary.simpleMessage("Add new url"),
        "alertAbove": MessageLookupByLibrary.simpleMessage("Moves Above"),
        "alertAddTooltip": MessageLookupByLibrary.simpleMessage("Add Alert"),
        "alertBelow": MessageLookupByLibrary.simpleMessage("Moves Below"),
        "alertChange": MessageLookupByLibrary.simpleMessage("Change 24H (%)"),
        "alertCoin": MessageLookupByLibrary.simpleMessage("Coin"),
        "alertCount": m0,
        "alertCurrency": MessageLookupByLibrary.simpleMessage("Currency"),
        "alertCurrentPrice":
            MessageLookupByLibrary.simpleMessage("Current Price"),
        "alertDeleteTooltip":
            MessageLookupByLibrary.simpleMessage("Delete Alert"),
        "alertDialogCancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "alertDialogClose": MessageLookupByLibrary.simpleMessage("Close"),
        "alertDialogCloseAndDelete":
            MessageLookupByLibrary.simpleMessage("Close and delete"),
        "alertDialogContinue": MessageLookupByLibrary.simpleMessage("Continue"),
        "alertDialogDelete": MessageLookupByLibrary.simpleMessage("Delete"),
        "alertDialogDeleteCoinPriceAlertTitle": m1,
        "alertDialogDeletePriceAlertMessage":
            MessageLookupByLibrary.simpleMessage(
                "This action can not be undone"),
        "alertDialogDeletePriceAlertTitle":
            MessageLookupByLibrary.simpleMessage("Delete Alert"),
        "alertDialogGrant": MessageLookupByLibrary.simpleMessage("Grant"),
        "alertDialogRemove": MessageLookupByLibrary.simpleMessage("Remove"),
        "alertDialogRename": MessageLookupByLibrary.simpleMessage("Rename"),
        "alertDialogSelect": MessageLookupByLibrary.simpleMessage("Select"),
        "alertEntryDeleteAllAlertsTooltip":
            MessageLookupByLibrary.simpleMessage("Delete all alerts"),
        "alertEntryOpenDetailTooltip":
            MessageLookupByLibrary.simpleMessage("Show coin details"),
        "alertEqual": MessageLookupByLibrary.simpleMessage("Is Equal"),
        "alertErrorText":
            MessageLookupByLibrary.simpleMessage("Please enter a valid number"),
        "alertFrequency":
            MessageLookupByLibrary.simpleMessage("Alert Frequency"),
        "alertHintText": MessageLookupByLibrary.simpleMessage("Enter a number"),
        "alertOnce": MessageLookupByLibrary.simpleMessage("Once"),
        "alertPrice": MessageLookupByLibrary.simpleMessage("Price"),
        "alertPriceMovement":
            MessageLookupByLibrary.simpleMessage("Price Movement"),
        "alertRepeat": MessageLookupByLibrary.simpleMessage("Repeat"),
        "alertTargetValue":
            MessageLookupByLibrary.simpleMessage("Target Value"),
        "alertTitle": MessageLookupByLibrary.simpleMessage("Alert"),
        "appName": MessageLookupByLibrary.simpleMessage("Crypto Prices"),
        "appVersion": MessageLookupByLibrary.simpleMessage("1.4.1"),
        "authenticateWrongPasswordError":
            MessageLookupByLibrary.simpleMessage("Wrong password"),
        "authenticationContinueWithoutPassword":
            MessageLookupByLibrary.simpleMessage("Continue without password"),
        "authenticationNoPasswordError":
            MessageLookupByLibrary.simpleMessage("Please enter a password"),
        "authenticationRequired":
            MessageLookupByLibrary.simpleMessage("Authentication required"),
        "authenticationTitle":
            MessageLookupByLibrary.simpleMessage("Unlock app"),
        "authenticationUnlockButton":
            MessageLookupByLibrary.simpleMessage("Unlock"),
        "authenticationUseBiometrics":
            MessageLookupByLibrary.simpleMessage("Use biometric data"),
        "autoBackupChangeFileName":
            MessageLookupByLibrary.simpleMessage("Rename file"),
        "autoBackupDate": m2,
        "autoBackupEnterFileNameHint":
            MessageLookupByLibrary.simpleMessage("Enter file name"),
        "autoBackupErrorInvalidCharacter": m3,
        "autoBackupErrorNoFileName":
            MessageLookupByLibrary.simpleMessage("Please enter a name"),
        "autoBackupHours": m4,
        "autoBackupJustNow": m5,
        "autoBackupMinutes": m6,
        "autoBackupNoBackupLocation":
            MessageLookupByLibrary.simpleMessage("No backup location"),
        "autoBackupPermissionRationaleText": MessageLookupByLibrary.simpleMessage(
            "The app needs permission to access the device storage to create backups of your data automatically."),
        "autoBackupPermissionRationaleTitle":
            MessageLookupByLibrary.simpleMessage("Grant storage permission"),
        "autoBackupYesterday": m7,
        "bitcoin": MessageLookupByLibrary.simpleMessage("Bitcoin"),
        "coinAmountCopied": m8,
        "coinsListTabName": MessageLookupByLibrary.simpleMessage("Coins"),
        "collapseAll": MessageLookupByLibrary.simpleMessage("Collapse all"),
        "copyToClipBoard":
            MessageLookupByLibrary.simpleMessage("Copy to clipboard"),
        "copyToClipBoardAddressMessage":
            MessageLookupByLibrary.simpleMessage("Address copied to clipboard"),
        "deletePortfolio":
            MessageLookupByLibrary.simpleMessage("Delete portfolio"),
        "deleteSearchEntryDialogMessage":
            MessageLookupByLibrary.simpleMessage("Delete from search history?"),
        "deleteTransaction":
            MessageLookupByLibrary.simpleMessage("Delete transaction"),
        "deleteWallet": MessageLookupByLibrary.simpleMessage("Delete Wallet"),
        "detailAppbarTitle": MessageLookupByLibrary.simpleMessage("Details"),
        "detailField24HHigh": MessageLookupByLibrary.simpleMessage("24H High"),
        "detailField24HLow": MessageLookupByLibrary.simpleMessage("24H Low"),
        "detailFieldAth": MessageLookupByLibrary.simpleMessage("All-Time High"),
        "detailFieldAtl": MessageLookupByLibrary.simpleMessage("All-Time Low"),
        "detailFieldGenesisDate":
            MessageLookupByLibrary.simpleMessage("Genesis date"),
        "detailFieldHomepage": MessageLookupByLibrary.simpleMessage("Homepage"),
        "detailFieldMarketCap":
            MessageLookupByLibrary.simpleMessage("Market Cap"),
        "detailFieldMarketCapRank":
            MessageLookupByLibrary.simpleMessage("Market Cap Rank"),
        "detailFieldSupply": MessageLookupByLibrary.simpleMessage("Supply"),
        "detailFieldVolume": MessageLookupByLibrary.simpleMessage("Volume"),
        "detailHeadingAbout": MessageLookupByLibrary.simpleMessage("About"),
        "detailHeadingMarketStats":
            MessageLookupByLibrary.simpleMessage("Market Statistics"),
        "donateTitle": MessageLookupByLibrary.simpleMessage("Donate"),
        "editPasswordEnterHint":
            MessageLookupByLibrary.simpleMessage("Enter password"),
        "editPasswordNoPasswordError":
            MessageLookupByLibrary.simpleMessage("Please enter a password"),
        "editPasswordRemove":
            MessageLookupByLibrary.simpleMessage("Remove password"),
        "editPasswordRemoveDialogTitle":
            MessageLookupByLibrary.simpleMessage("Remove password lock"),
        "editPasswordRepeatHint":
            MessageLookupByLibrary.simpleMessage("Repeat password"),
        "editPasswordRepeatPasswordError":
            MessageLookupByLibrary.simpleMessage("Please repeat the password"),
        "editPasswordShowPassword":
            MessageLookupByLibrary.simpleMessage("Show password"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "errorMessageHttp": MessageLookupByLibrary.simpleMessage(
            "Service is currently unavailable"),
        "errorMessageRateLimitation": MessageLookupByLibrary.simpleMessage(
            "Too many requests. Please wait one minute before trying again"),
        "errorMessageRateLimitationShort":
            MessageLookupByLibrary.simpleMessage("Too many requests"),
        "errorMessageSocket":
            MessageLookupByLibrary.simpleMessage("No internet connection"),
        "errorNoDataAvailable":
            MessageLookupByLibrary.simpleMessage("No data available"),
        "ethereumAndTokens":
            MessageLookupByLibrary.simpleMessage("Ethereum (and ERC20 Tokens)"),
        "expandAll": MessageLookupByLibrary.simpleMessage("Expand all"),
        "explorerUrlNoCoinSelected":
            MessageLookupByLibrary.simpleMessage("No coin selected"),
        "explorerUrlSelectACoin":
            MessageLookupByLibrary.simpleMessage("Select a coin"),
        "explorerUrlTitle":
            MessageLookupByLibrary.simpleMessage("Explorer URL"),
        "explorerUrlTransactionUrl":
            MessageLookupByLibrary.simpleMessage("Transaction URL"),
        "explorerUrlTransactionUrlHint":
            MessageLookupByLibrary.simpleMessage("Enter a url"),
        "explorerUrlTransactionUrlInfo": MessageLookupByLibrary.simpleMessage(
            "The transaction id is added directly behind the URL, therefore the URL has to be this format:"),
        "explorerUrlTransactionUrlInfoExample":
            MessageLookupByLibrary.simpleMessage(
                "https://explorer.com/path/to/transaction/"),
        "favoriteRemoved":
            MessageLookupByLibrary.simpleMessage("Favorite removed"),
        "favoritesTabName": MessageLookupByLibrary.simpleMessage("Favorites"),
        "globalMarketCap": m9,
        "globalMarketInfoActiveCrypto": m10,
        "globalMarketInfoGlobalVolume": m11,
        "globalMarketInfoMarketCap": m12,
        "globalMarketInfoMarketCapChange":
            MessageLookupByLibrary.simpleMessage("Market Cap 24H Change: "),
        "globalMarketInfoMarketShare":
            MessageLookupByLibrary.simpleMessage("Coin Market Shares: "),
        "globalMarketInfoTile":
            MessageLookupByLibrary.simpleMessage("Global Market Info"),
        "globalVolume": m13,
        "graphToggleDay": MessageLookupByLibrary.simpleMessage("1D"),
        "graphToggleHour": MessageLookupByLibrary.simpleMessage("1H"),
        "graphToggleMax": MessageLookupByLibrary.simpleMessage("Max"),
        "graphToggleMonth": MessageLookupByLibrary.simpleMessage("1M"),
        "graphToggleWeek": MessageLookupByLibrary.simpleMessage("1W"),
        "graphToggleYear": MessageLookupByLibrary.simpleMessage("1Y"),
        "liberapay": MessageLookupByLibrary.simpleMessage("Liberapay"),
        "loadingDataMessage":
            MessageLookupByLibrary.simpleMessage("Loading Data"),
        "migrationAssistantAlertDialogMessage":
            MessageLookupByLibrary.simpleMessage(
                "This will delete all incorrect data to ensure that the app continues to function correctly.\nThis action cannot be undone."),
        "migrationAssistantBackButton":
            MessageLookupByLibrary.simpleMessage("< Back"),
        "migrationAssistantContinueButton":
            MessageLookupByLibrary.simpleMessage("Continue >"),
        "migrationAssistantEntrySearchCoins":
            MessageLookupByLibrary.simpleMessage("Search coins"),
        "migrationAssistantEntrySelectCoin":
            MessageLookupByLibrary.simpleMessage("Select the correct new coin"),
        "migrationAssistantFinishButton":
            MessageLookupByLibrary.simpleMessage("Finish"),
        "migrationAssistantText1": MessageLookupByLibrary.simpleMessage(
            "The coin identifiers used by the Web-API have changed and an automated migration is not possible."),
        "migrationAssistantText2": MessageLookupByLibrary.simpleMessage(
            "A manual migration is necessary so that the app can continue to access the correct coin information and keep functioning correctly."),
        "migrationAssistantText3": MessageLookupByLibrary.simpleMessage(
            "On the following screens you will have to select the new identifier for each coin that has changed.\nYou will be presented with a number of possible options to choose from."),
        "migrationAssistantTitle":
            MessageLookupByLibrary.simpleMessage("ID Migration Assistant"),
        "minutes": m14,
        "monthlyDonation":
            MessageLookupByLibrary.simpleMessage("Set up monthly donation"),
        "moreInformation":
            MessageLookupByLibrary.simpleMessage("More information"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "never": MessageLookupByLibrary.simpleMessage("Never"),
        "newPortfolio": MessageLookupByLibrary.simpleMessage("New portfolio"),
        "newTransaction":
            MessageLookupByLibrary.simpleMessage("New transaction"),
        "noBiometrics":
            MessageLookupByLibrary.simpleMessage("No biometric data available"),
        "noFavorites": MessageLookupByLibrary.simpleMessage("No favorites yet"),
        "noPortfolios":
            MessageLookupByLibrary.simpleMessage("No portfolios yet"),
        "noPriceAlerts":
            MessageLookupByLibrary.simpleMessage("No price alerts yet"),
        "noWalletTransactions":
            MessageLookupByLibrary.simpleMessage("No wallet transactions yet"),
        "notAvailableInPrivateMode": MessageLookupByLibrary.simpleMessage(
            "Not available in private mode"),
        "notificationLargeMoveAlertChannelName":
            MessageLookupByLibrary.simpleMessage("Large Price Movements"),
        "notificationLargeMoveAlertCoinNamePlaceholder":
            MessageLookupByLibrary.simpleMessage("Favorite"),
        "notificationLargeMoveAlertMessage": m15,
        "notificationLargeMoveAlertTitle": m16,
        "notificationPriceAlertAbove":
            MessageLookupByLibrary.simpleMessage("moved above"),
        "notificationPriceAlertBelow":
            MessageLookupByLibrary.simpleMessage("moved below"),
        "notificationPriceAlertEqual":
            MessageLookupByLibrary.simpleMessage("is equal to"),
        "notificationPriceAlertMessagePercent": m17,
        "notificationPriceAlertMessagePercentWithPrice": m18,
        "notificationPriceAlertMessagePrice": m19,
        "notificationPriceAlertTitle": m20,
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "portfolioEditDefaultPortfolio":
            MessageLookupByLibrary.simpleMessage("Default portfolio"),
        "portfolioEditEditPortfolio":
            MessageLookupByLibrary.simpleMessage("Edit Portfolio"),
        "portfolioEditNameErrorText":
            MessageLookupByLibrary.simpleMessage("Please enter a name"),
        "portfolioEditNameHintText":
            MessageLookupByLibrary.simpleMessage("Enter portfolio name"),
        "portfolioEditUseAppCurrency":
            MessageLookupByLibrary.simpleMessage("Use app currency"),
        "portfolioIsEmpty":
            MessageLookupByLibrary.simpleMessage("Portfolio is empty"),
        "portfolioLocked":
            MessageLookupByLibrary.simpleMessage("Portfolio locked"),
        "portfolioPieChartOther":
            MessageLookupByLibrary.simpleMessage("Other "),
        "portfolioTabName": MessageLookupByLibrary.simpleMessage("Portfolio"),
        "portfolioTotalValue":
            MessageLookupByLibrary.simpleMessage("Total Value: "),
        "priceAlertsTabName":
            MessageLookupByLibrary.simpleMessage("Price Alerts"),
        "privateMode": MessageLookupByLibrary.simpleMessage("Private mode"),
        "qrScannerBarcodeType":
            MessageLookupByLibrary.simpleMessage("Barcode type: "),
        "qrScannerData": MessageLookupByLibrary.simpleMessage("Data: "),
        "qrScannerNoPermission":
            MessageLookupByLibrary.simpleMessage("No Permission"),
        "qrScannerScanCode":
            MessageLookupByLibrary.simpleMessage("Scan a code"),
        "rateLimitInfoCoinGeckoPrices":
            MessageLookupByLibrary.simpleMessage("Coingecko API Prices"),
        "rateLimitInfoExplanation":
            MessageLookupByLibrary.simpleMessage("Explanation"),
        "rateLimitInfoExplanationFirst": MessageLookupByLibrary.simpleMessage(
            "The free version of the Coingecko API that this app uses is limiting the number of requests that can be sent to 50 per minute."),
        "rateLimitInfoExplanationSecond": MessageLookupByLibrary.simpleMessage(
            "If you have many favorites or wallets, this number can be exceeded easily, for example by frequent refreshing."),
        "rateLimitInfoSolution":
            MessageLookupByLibrary.simpleMessage("Solution"),
        "rateLimitInfoSolutionFirst": MessageLookupByLibrary.simpleMessage(
            "If this error occurs, you need to wait approximately one minute before you can refresh the data again."),
        "rateLimitInfoSolutionFourth": MessageLookupByLibrary.simpleMessage(
            "If you want to help pay for an API key and support the development of this app, you can do so through the donation links below."),
        "rateLimitInfoSolutionSecond": MessageLookupByLibrary.simpleMessage(
            "The only other option is to pay for an API key."),
        "rateLimitInfoSolutionThird": MessageLookupByLibrary.simpleMessage(
            "At the moment, the cheapest paid version starts at \$129 per month, therefore I, unfortunately, can\'t afford to pay for it alone."),
        "rateLimitInfoTitle":
            MessageLookupByLibrary.simpleMessage("About Rate Limitation"),
        "searchFieldHintText": MessageLookupByLibrary.simpleMessage("Search"),
        "searchToolTip": MessageLookupByLibrary.simpleMessage("Search"),
        "selectCurrencyTitle":
            MessageLookupByLibrary.simpleMessage("Select Currency"),
        "selectLanguageTitle":
            MessageLookupByLibrary.simpleMessage("Select Language"),
        "selectStartScreenTitle":
            MessageLookupByLibrary.simpleMessage("Select start screen"),
        "settingsAbout": MessageLookupByLibrary.simpleMessage("About"),
        "settingsAppearance":
            MessageLookupByLibrary.simpleMessage("Appearance"),
        "settingsAppearanceApp": MessageLookupByLibrary.simpleMessage("App"),
        "settingsAppearanceColor":
            MessageLookupByLibrary.simpleMessage("Color"),
        "settingsAppearanceColorDialogTitle":
            MessageLookupByLibrary.simpleMessage("Select a color"),
        "settingsAppearanceColorSubtitle": MessageLookupByLibrary.simpleMessage(
            "Select the primary color of the app"),
        "settingsAppearanceDateFormat":
            MessageLookupByLibrary.simpleMessage("Date format"),
        "settingsAppearanceDateFormatTitle":
            MessageLookupByLibrary.simpleMessage("Select date format"),
        "settingsAppearanceNumberFormat":
            MessageLookupByLibrary.simpleMessage("Number format"),
        "settingsAppearanceNumberFormatTitle":
            MessageLookupByLibrary.simpleMessage("Select number format"),
        "settingsAppearanceNumbers":
            MessageLookupByLibrary.simpleMessage("Numbers"),
        "settingsAppearanceTheme":
            MessageLookupByLibrary.simpleMessage("Theme"),
        "settingsAppearanceThemeAmoled":
            MessageLookupByLibrary.simpleMessage("AMOLED theme"),
        "settingsAppearanceThemeDark":
            MessageLookupByLibrary.simpleMessage("Dark theme"),
        "settingsAppearanceThemeDialogTitle":
            MessageLookupByLibrary.simpleMessage("Select a theme"),
        "settingsAppearanceThemeLight":
            MessageLookupByLibrary.simpleMessage("Light theme"),
        "settingsAppearanceThemeSystem":
            MessageLookupByLibrary.simpleMessage("System default"),
        "settingsAppearanceThemeSystemAmoled":
            MessageLookupByLibrary.simpleMessage("System default (AMOLED)"),
        "settingsAutoBackupFileName":
            MessageLookupByLibrary.simpleMessage("Backup file name"),
        "settingsAutoBackupLocation":
            MessageLookupByLibrary.simpleMessage("Backup location"),
        "settingsAutoBackupOverwriteSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Overwrite the old file when making a new backup"),
        "settingsAutoBackupOverwriteTitle":
            MessageLookupByLibrary.simpleMessage("Overwrite old file"),
        "settingsCalculator":
            MessageLookupByLibrary.simpleMessage("Calculator"),
        "settingsCurrency": MessageLookupByLibrary.simpleMessage("Currency"),
        "settingsEnglish": MessageLookupByLibrary.simpleMessage("English"),
        "settingsGerman": MessageLookupByLibrary.simpleMessage("Deutsch"),
        "settingsGroupTools": MessageLookupByLibrary.simpleMessage("Tools"),
        "settingsImportExport": MessageLookupByLibrary.simpleMessage("Backup"),
        "settingsImportExportAutoBackup":
            MessageLookupByLibrary.simpleMessage("Automatic backup"),
        "settingsImportExportExportFileSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Export favorites, alerts and portfolios"),
        "settingsImportExportExportFileTitle":
            MessageLookupByLibrary.simpleMessage("Export"),
        "settingsImportExportExportSuccessful":
            MessageLookupByLibrary.simpleMessage("Export successful"),
        "settingsImportExportImportFileSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Import data from a json file"),
        "settingsImportExportImportFileTitle":
            MessageLookupByLibrary.simpleMessage("Import from file"),
        "settingsImportExportImportPortfolioName":
            MessageLookupByLibrary.simpleMessage("Import Portfolio"),
        "settingsImportExportImportSuccessful":
            MessageLookupByLibrary.simpleMessage("Import successful"),
        "settingsImportExportOverwriteAlertMessage":
            MessageLookupByLibrary.simpleMessage(
                "Existing data will be permanently overwritten.\nDo you wish to continue?"),
        "settingsImportExportOverwriteAlertTitle":
            MessageLookupByLibrary.simpleMessage("Overwrite data"),
        "settingsImportExportWrongFormat":
            MessageLookupByLibrary.simpleMessage("Wrong file format"),
        "settingsLanguage": MessageLookupByLibrary.simpleMessage("Language"),
        "settingsNotifications":
            MessageLookupByLibrary.simpleMessage("Notifications"),
        "settingsNotificationsEnableAlertsText":
            MessageLookupByLibrary.simpleMessage(
                "Show notification for large price moves of favorites"),
        "settingsNotificationsEnableMoveAlerts":
            MessageLookupByLibrary.simpleMessage("Enable alerts"),
        "settingsNotificationsLargePriceMoveAlerts":
            MessageLookupByLibrary.simpleMessage("Large Price Movements"),
        "settingsNotificationsMoveThreshold":
            MessageLookupByLibrary.simpleMessage("Price move threshold"),
        "settingsNotificationsMoveThresholdHint":
            MessageLookupByLibrary.simpleMessage("Enter a number"),
        "settingsNotificationsShowPriceSubTitle":
            MessageLookupByLibrary.simpleMessage(
                "Show the coin price in 24H change alerts"),
        "settingsNotificationsShowPriceTitle":
            MessageLookupByLibrary.simpleMessage(
                "Show price in percentage alerts"),
        "settingsPortfolio": MessageLookupByLibrary.simpleMessage("Portfolio"),
        "settingsPortfolioAddFavoriteTitle":
            MessageLookupByLibrary.simpleMessage("Add to coin favorites"),
        "settingsPortfolioAddToFavoriteSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Add the new wallet to favorites"),
        "settingsPortfolioChangePercentagePeriodTitle":
            MessageLookupByLibrary.simpleMessage("Change Percentage Period"),
        "settingsPortfolioTransactions":
            MessageLookupByLibrary.simpleMessage("Transactions"),
        "settingsPortfolioTransactionsExplorerUrlsSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Change transaction urls of block explorers"),
        "settingsPortfolioTransactionsExplorerUrlsTitle":
            MessageLookupByLibrary.simpleMessage("Block-Explorer URLs"),
        "settingsPortfolioTransactionsPortfolioCurrencySubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Display the transaction value in the portfolio currency"),
        "settingsPortfolioTransactionsPortfolioCurrencyTitle":
            MessageLookupByLibrary.simpleMessage("Value in portfolio currency"),
        "settingsSecurity": MessageLookupByLibrary.simpleMessage("Security"),
        "settingsSecurityAuthOnAppStart":
            MessageLookupByLibrary.simpleMessage("Authenticate on app start"),
        "settingsSecurityAuthOnAppStartSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Open password screen when starting the app"),
        "settingsSecurityBiometricUnlock":
            MessageLookupByLibrary.simpleMessage("Allow biometric unlock"),
        "settingsSecurityBiometricUnlockSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Allow biometric authentication to unlock the app"),
        "settingsSecurityChangePassword":
            MessageLookupByLibrary.simpleMessage("Change Password"),
        "settingsSecurityKeepUnlocked": MessageLookupByLibrary.simpleMessage(
            "Keep app unlocked after closing"),
        "settingsSecurityKeepUnlockedDialogTitle":
            MessageLookupByLibrary.simpleMessage("Keep app unlocked for"),
        "settingsSecurityLockNow":
            MessageLookupByLibrary.simpleMessage("Lock now"),
        "settingsSecurityPasswordLock":
            MessageLookupByLibrary.simpleMessage("Password lock"),
        "settingsSecurityPasswordLockSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Lock the app with a password or biometric data"),
        "settingsSecurityPrivateModeSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Hide all balances in the app"),
        "settingsStartScreen":
            MessageLookupByLibrary.simpleMessage("Start screen"),
        "settingsTabName": MessageLookupByLibrary.simpleMessage("Settings"),
        "showCoinDetails":
            MessageLookupByLibrary.simpleMessage("Show coin details"),
        "showWallet": MessageLookupByLibrary.simpleMessage("Show wallet"),
        "sortByToolTip": MessageLookupByLibrary.simpleMessage("Sort by"),
        "sortMenuOptionMarketCap":
            MessageLookupByLibrary.simpleMessage("Market Cap"),
        "sortMenuOptionName": MessageLookupByLibrary.simpleMessage("Name"),
        "sortMenuOptionNameAsc":
            MessageLookupByLibrary.simpleMessage("Name (A-Z)"),
        "sortMenuOptionNameDesc":
            MessageLookupByLibrary.simpleMessage("Name (Z-A)"),
        "sortMenuOptionPriceChange":
            MessageLookupByLibrary.simpleMessage("Price Change"),
        "sortMenuOptionSymbol": MessageLookupByLibrary.simpleMessage("Symbol"),
        "sortMenuOptionTotalValue":
            MessageLookupByLibrary.simpleMessage("Total value"),
        "sortMenuOptionTotalValueAsc":
            MessageLookupByLibrary.simpleMessage("Lowest value"),
        "sortMenuOptionTotalValueDesc":
            MessageLookupByLibrary.simpleMessage("Highest value"),
        "sortMenuOptionVolume": MessageLookupByLibrary.simpleMessage("Volume"),
        "sortingDirectionTooltip":
            MessageLookupByLibrary.simpleMessage("Sorting direction"),
        "timePeriodDay": MessageLookupByLibrary.simpleMessage("Day"),
        "timePeriodHour": MessageLookupByLibrary.simpleMessage("Hour"),
        "timePeriodMax": MessageLookupByLibrary.simpleMessage("Max"),
        "timePeriodMonth": MessageLookupByLibrary.simpleMessage("Month"),
        "timePeriodWeek": MessageLookupByLibrary.simpleMessage("Week"),
        "timePeriodYear": MessageLookupByLibrary.simpleMessage("Year"),
        "transaction": MessageLookupByLibrary.simpleMessage("Transaction"),
        "transactionEditCoinAmountHint":
            MessageLookupByLibrary.simpleMessage("Enter a number"),
        "transactionEditCoinPrice":
            MessageLookupByLibrary.simpleMessage("Coin Price"),
        "transactionEditCoinPriceHint":
            MessageLookupByLibrary.simpleMessage("Enter a coin price"),
        "transactionEditCurrencyAmountHint":
            MessageLookupByLibrary.simpleMessage("Enter a number"),
        "transactionEditDescription":
            MessageLookupByLibrary.simpleMessage("Description"),
        "transactionEditDescriptionHint":
            MessageLookupByLibrary.simpleMessage("Enter a description"),
        "transactionEditDestinationAddress":
            MessageLookupByLibrary.simpleMessage("Destination Address"),
        "transactionEditDestinationAddressHint":
            MessageLookupByLibrary.simpleMessage(
                "Enter the destination address"),
        "transactionEditFee":
            MessageLookupByLibrary.simpleMessage("Transaction Fee"),
        "transactionEditMoreOptions":
            MessageLookupByLibrary.simpleMessage("More options"),
        "transactionEditNoDataForDate": MessageLookupByLibrary.simpleMessage(
            "No data for the selected date"),
        "transactionEditNoExplorerUrl":
            MessageLookupByLibrary.simpleMessage("No explorer URL set"),
        "transactionEditNoTransactionId":
            MessageLookupByLibrary.simpleMessage("No transaction id"),
        "transactionEditPriceFromDate": MessageLookupByLibrary.simpleMessage(
            "Use price from transaction date"),
        "transactionEditQrCodeTooltip":
            MessageLookupByLibrary.simpleMessage("Get from QR-Code"),
        "transactionEditSelectPortfolio":
            MessageLookupByLibrary.simpleMessage("Please select a portfolio"),
        "transactionEditSetUrl":
            MessageLookupByLibrary.simpleMessage("Set URL"),
        "transactionEditSourceAddress":
            MessageLookupByLibrary.simpleMessage("Source Address"),
        "transactionEditSourceAddressHint":
            MessageLookupByLibrary.simpleMessage("Enter the source address"),
        "transactionEditTransactionDate":
            MessageLookupByLibrary.simpleMessage("Transaction Date"),
        "transactionEditTransactionId":
            MessageLookupByLibrary.simpleMessage("Transaction ID"),
        "transactionEditTransactionIdHint":
            MessageLookupByLibrary.simpleMessage("Enter the transaction id"),
        "transactionEditTransactionName":
            MessageLookupByLibrary.simpleMessage("Transaction Name"),
        "transactionEditTransactionNameHint":
            MessageLookupByLibrary.simpleMessage("Enter a transaction name"),
        "transactionEditTransactionVolume":
            MessageLookupByLibrary.simpleMessage("Transaction Volume"),
        "transactionEditViewInExplorer":
            MessageLookupByLibrary.simpleMessage("View in explorer"),
        "transactionReceived": MessageLookupByLibrary.simpleMessage("Received"),
        "transactionSent": MessageLookupByLibrary.simpleMessage("Sent"),
        "transactionUrlsListTitle":
            MessageLookupByLibrary.simpleMessage("Transaction URLs"),
        "undo": MessageLookupByLibrary.simpleMessage("Undo"),
        "urlLaunchAlertDialogMessage": MessageLookupByLibrary.simpleMessage(
            "Link could not be opened.\nCheck if you have installed an internet browser."),
        "urlLaunchAlertDialogTitle":
            MessageLookupByLibrary.simpleMessage("Could not open link"),
        "useOwnCoinPriceSubtitle": MessageLookupByLibrary.simpleMessage(
            "Use the transaction coin price for currency value in the list"),
        "useOwnCoinPriceTitle":
            MessageLookupByLibrary.simpleMessage("Use transaction coin price"),
        "valueCopied":
            MessageLookupByLibrary.simpleMessage("Value copied to clipboard"),
        "walletDetailTransactions":
            MessageLookupByLibrary.simpleMessage("Transactions")
      };
}
