// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Crypto Prices`
  String get appName {
    return Intl.message(
      'Crypto Prices',
      name: 'appName',
      desc: '',
      args: [],
    );
  }

  /// `1.4.1`
  String get appVersion {
    return Intl.message(
      '1.4.1',
      name: 'appVersion',
      desc: '',
      args: [],
    );
  }

  /// `Coins`
  String get coinsListTabName {
    return Intl.message(
      'Coins',
      name: 'coinsListTabName',
      desc: '',
      args: [],
    );
  }

  /// `Favorites`
  String get favoritesTabName {
    return Intl.message(
      'Favorites',
      name: 'favoritesTabName',
      desc: '',
      args: [],
    );
  }

  /// `Portfolio`
  String get portfolioTabName {
    return Intl.message(
      'Portfolio',
      name: 'portfolioTabName',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settingsTabName {
    return Intl.message(
      'Settings',
      name: 'settingsTabName',
      desc: '',
      args: [],
    );
  }

  /// `Price Alerts`
  String get priceAlertsTabName {
    return Intl.message(
      'Price Alerts',
      name: 'priceAlertsTabName',
      desc: '',
      args: [],
    );
  }

  /// `Sorting direction`
  String get sortingDirectionTooltip {
    return Intl.message(
      'Sorting direction',
      name: 'sortingDirectionTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Sort by`
  String get sortByToolTip {
    return Intl.message(
      'Sort by',
      name: 'sortByToolTip',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get searchToolTip {
    return Intl.message(
      'Search',
      name: 'searchToolTip',
      desc: '',
      args: [],
    );
  }

  /// `Market Cap`
  String get sortMenuOptionMarketCap {
    return Intl.message(
      'Market Cap',
      name: 'sortMenuOptionMarketCap',
      desc: '',
      args: [],
    );
  }

  /// `Volume`
  String get sortMenuOptionVolume {
    return Intl.message(
      'Volume',
      name: 'sortMenuOptionVolume',
      desc: '',
      args: [],
    );
  }

  /// `Price Change`
  String get sortMenuOptionPriceChange {
    return Intl.message(
      'Price Change',
      name: 'sortMenuOptionPriceChange',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get sortMenuOptionName {
    return Intl.message(
      'Name',
      name: 'sortMenuOptionName',
      desc: '',
      args: [],
    );
  }

  /// `Symbol`
  String get sortMenuOptionSymbol {
    return Intl.message(
      'Symbol',
      name: 'sortMenuOptionSymbol',
      desc: '',
      args: [],
    );
  }

  /// `Total value`
  String get sortMenuOptionTotalValue {
    return Intl.message(
      'Total value',
      name: 'sortMenuOptionTotalValue',
      desc: '',
      args: [],
    );
  }

  /// `Highest value`
  String get sortMenuOptionTotalValueDesc {
    return Intl.message(
      'Highest value',
      name: 'sortMenuOptionTotalValueDesc',
      desc: '',
      args: [],
    );
  }

  /// `Lowest value`
  String get sortMenuOptionTotalValueAsc {
    return Intl.message(
      'Lowest value',
      name: 'sortMenuOptionTotalValueAsc',
      desc: '',
      args: [],
    );
  }

  /// `Name (Z-A)`
  String get sortMenuOptionNameDesc {
    return Intl.message(
      'Name (Z-A)',
      name: 'sortMenuOptionNameDesc',
      desc: '',
      args: [],
    );
  }

  /// `Name (A-Z)`
  String get sortMenuOptionNameAsc {
    return Intl.message(
      'Name (A-Z)',
      name: 'sortMenuOptionNameAsc',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get searchFieldHintText {
    return Intl.message(
      'Search',
      name: 'searchFieldHintText',
      desc: '',
      args: [],
    );
  }

  /// `Delete from search history?`
  String get deleteSearchEntryDialogMessage {
    return Intl.message(
      'Delete from search history?',
      name: 'deleteSearchEntryDialogMessage',
      desc: '',
      args: [],
    );
  }

  /// `Collapse all`
  String get collapseAll {
    return Intl.message(
      'Collapse all',
      name: 'collapseAll',
      desc: '',
      args: [],
    );
  }

  /// `Expand all`
  String get expandAll {
    return Intl.message(
      'Expand all',
      name: 'expandAll',
      desc: '',
      args: [],
    );
  }

  /// `Private mode`
  String get privateMode {
    return Intl.message(
      'Private mode',
      name: 'privateMode',
      desc: '',
      args: [],
    );
  }

  /// `Loading Data`
  String get loadingDataMessage {
    return Intl.message(
      'Loading Data',
      name: 'loadingDataMessage',
      desc: '',
      args: [],
    );
  }

  /// `No favorites yet`
  String get noFavorites {
    return Intl.message(
      'No favorites yet',
      name: 'noFavorites',
      desc: '',
      args: [],
    );
  }

  /// `No price alerts yet`
  String get noPriceAlerts {
    return Intl.message(
      'No price alerts yet',
      name: 'noPriceAlerts',
      desc: '',
      args: [],
    );
  }

  /// `Favorite removed`
  String get favoriteRemoved {
    return Intl.message(
      'Favorite removed',
      name: 'favoriteRemoved',
      desc: '',
      args: [],
    );
  }

  /// `Undo`
  String get undo {
    return Intl.message(
      'Undo',
      name: 'undo',
      desc: '',
      args: [],
    );
  }

  /// `1H`
  String get graphToggleHour {
    return Intl.message(
      '1H',
      name: 'graphToggleHour',
      desc: '',
      args: [],
    );
  }

  /// `1D`
  String get graphToggleDay {
    return Intl.message(
      '1D',
      name: 'graphToggleDay',
      desc: '',
      args: [],
    );
  }

  /// `1W`
  String get graphToggleWeek {
    return Intl.message(
      '1W',
      name: 'graphToggleWeek',
      desc: '',
      args: [],
    );
  }

  /// `1M`
  String get graphToggleMonth {
    return Intl.message(
      '1M',
      name: 'graphToggleMonth',
      desc: '',
      args: [],
    );
  }

  /// `1Y`
  String get graphToggleYear {
    return Intl.message(
      '1Y',
      name: 'graphToggleYear',
      desc: '',
      args: [],
    );
  }

  /// `Max`
  String get graphToggleMax {
    return Intl.message(
      'Max',
      name: 'graphToggleMax',
      desc: '',
      args: [],
    );
  }

  /// `Hour`
  String get timePeriodHour {
    return Intl.message(
      'Hour',
      name: 'timePeriodHour',
      desc: '',
      args: [],
    );
  }

  /// `Day`
  String get timePeriodDay {
    return Intl.message(
      'Day',
      name: 'timePeriodDay',
      desc: '',
      args: [],
    );
  }

  /// `Week`
  String get timePeriodWeek {
    return Intl.message(
      'Week',
      name: 'timePeriodWeek',
      desc: '',
      args: [],
    );
  }

  /// `Month`
  String get timePeriodMonth {
    return Intl.message(
      'Month',
      name: 'timePeriodMonth',
      desc: '',
      args: [],
    );
  }

  /// `Year`
  String get timePeriodYear {
    return Intl.message(
      'Year',
      name: 'timePeriodYear',
      desc: '',
      args: [],
    );
  }

  /// `Max`
  String get timePeriodMax {
    return Intl.message(
      'Max',
      name: 'timePeriodMax',
      desc: '',
      args: [],
    );
  }

  /// `Could not open link`
  String get urlLaunchAlertDialogTitle {
    return Intl.message(
      'Could not open link',
      name: 'urlLaunchAlertDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Link could not be opened.\nCheck if you have installed an internet browser.`
  String get urlLaunchAlertDialogMessage {
    return Intl.message(
      'Link could not be opened.\nCheck if you have installed an internet browser.',
      name: 'urlLaunchAlertDialogMessage',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get alertDialogClose {
    return Intl.message(
      'Close',
      name: 'alertDialogClose',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get alertDialogDelete {
    return Intl.message(
      'Delete',
      name: 'alertDialogDelete',
      desc: '',
      args: [],
    );
  }

  /// `Close and delete`
  String get alertDialogCloseAndDelete {
    return Intl.message(
      'Close and delete',
      name: 'alertDialogCloseAndDelete',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get alertDialogCancel {
    return Intl.message(
      'Cancel',
      name: 'alertDialogCancel',
      desc: '',
      args: [],
    );
  }

  /// `Select`
  String get alertDialogSelect {
    return Intl.message(
      'Select',
      name: 'alertDialogSelect',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get alertDialogContinue {
    return Intl.message(
      'Continue',
      name: 'alertDialogContinue',
      desc: '',
      args: [],
    );
  }

  /// `Delete Alert`
  String get alertDialogDeletePriceAlertTitle {
    return Intl.message(
      'Delete Alert',
      name: 'alertDialogDeletePriceAlertTitle',
      desc: '',
      args: [],
    );
  }

  /// `This action can not be undone`
  String get alertDialogDeletePriceAlertMessage {
    return Intl.message(
      'This action can not be undone',
      name: 'alertDialogDeletePriceAlertMessage',
      desc: '',
      args: [],
    );
  }

  /// `Delete all {coinSymbol} Alerts`
  String alertDialogDeleteCoinPriceAlertTitle(Object coinSymbol) {
    return Intl.message(
      'Delete all $coinSymbol Alerts',
      name: 'alertDialogDeleteCoinPriceAlertTitle',
      desc: '',
      args: [coinSymbol],
    );
  }

  /// `Remove`
  String get alertDialogRemove {
    return Intl.message(
      'Remove',
      name: 'alertDialogRemove',
      desc: '',
      args: [],
    );
  }

  /// `Rename`
  String get alertDialogRename {
    return Intl.message(
      'Rename',
      name: 'alertDialogRename',
      desc: '',
      args: [],
    );
  }

  /// `Grant`
  String get alertDialogGrant {
    return Intl.message(
      'Grant',
      name: 'alertDialogGrant',
      desc: '',
      args: [],
    );
  }

  /// `Details`
  String get detailAppbarTitle {
    return Intl.message(
      'Details',
      name: 'detailAppbarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Market Statistics`
  String get detailHeadingMarketStats {
    return Intl.message(
      'Market Statistics',
      name: 'detailHeadingMarketStats',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get detailHeadingAbout {
    return Intl.message(
      'About',
      name: 'detailHeadingAbout',
      desc: '',
      args: [],
    );
  }

  /// `Market Cap Rank`
  String get detailFieldMarketCapRank {
    return Intl.message(
      'Market Cap Rank',
      name: 'detailFieldMarketCapRank',
      desc: '',
      args: [],
    );
  }

  /// `Market Cap`
  String get detailFieldMarketCap {
    return Intl.message(
      'Market Cap',
      name: 'detailFieldMarketCap',
      desc: '',
      args: [],
    );
  }

  /// `Volume`
  String get detailFieldVolume {
    return Intl.message(
      'Volume',
      name: 'detailFieldVolume',
      desc: '',
      args: [],
    );
  }

  /// `Supply`
  String get detailFieldSupply {
    return Intl.message(
      'Supply',
      name: 'detailFieldSupply',
      desc: '',
      args: [],
    );
  }

  /// `24H High`
  String get detailField24HHigh {
    return Intl.message(
      '24H High',
      name: 'detailField24HHigh',
      desc: '',
      args: [],
    );
  }

  /// `24H Low`
  String get detailField24HLow {
    return Intl.message(
      '24H Low',
      name: 'detailField24HLow',
      desc: '',
      args: [],
    );
  }

  /// `All-Time High`
  String get detailFieldAth {
    return Intl.message(
      'All-Time High',
      name: 'detailFieldAth',
      desc: '',
      args: [],
    );
  }

  /// `All-Time Low`
  String get detailFieldAtl {
    return Intl.message(
      'All-Time Low',
      name: 'detailFieldAtl',
      desc: '',
      args: [],
    );
  }

  /// `Genesis date`
  String get detailFieldGenesisDate {
    return Intl.message(
      'Genesis date',
      name: 'detailFieldGenesisDate',
      desc: '',
      args: [],
    );
  }

  /// `Homepage`
  String get detailFieldHomepage {
    return Intl.message(
      'Homepage',
      name: 'detailFieldHomepage',
      desc: '',
      args: [],
    );
  }

  /// `Transactions`
  String get walletDetailTransactions {
    return Intl.message(
      'Transactions',
      name: 'walletDetailTransactions',
      desc: '',
      args: [],
    );
  }

  /// `Received`
  String get transactionReceived {
    return Intl.message(
      'Received',
      name: 'transactionReceived',
      desc: '',
      args: [],
    );
  }

  /// `Sent`
  String get transactionSent {
    return Intl.message(
      'Sent',
      name: 'transactionSent',
      desc: '',
      args: [],
    );
  }

  /// `No wallet transactions yet`
  String get noWalletTransactions {
    return Intl.message(
      'No wallet transactions yet',
      name: 'noWalletTransactions',
      desc: '',
      args: [],
    );
  }

  /// `Add transaction`
  String get addTransaction {
    return Intl.message(
      'Add transaction',
      name: 'addTransaction',
      desc: '',
      args: [],
    );
  }

  /// `Delete Wallet`
  String get deleteWallet {
    return Intl.message(
      'Delete Wallet',
      name: 'deleteWallet',
      desc: '',
      args: [],
    );
  }

  /// `Show coin details`
  String get showCoinDetails {
    return Intl.message(
      'Show coin details',
      name: 'showCoinDetails',
      desc: '',
      args: [],
    );
  }

  /// `Show wallet`
  String get showWallet {
    return Intl.message(
      'Show wallet',
      name: 'showWallet',
      desc: '',
      args: [],
    );
  }

  /// `Other `
  String get portfolioPieChartOther {
    return Intl.message(
      'Other ',
      name: 'portfolioPieChartOther',
      desc: '',
      args: [],
    );
  }

  /// `Total Value: `
  String get portfolioTotalValue {
    return Intl.message(
      'Total Value: ',
      name: 'portfolioTotalValue',
      desc: '',
      args: [],
    );
  }

  /// `Portfolio is empty`
  String get portfolioIsEmpty {
    return Intl.message(
      'Portfolio is empty',
      name: 'portfolioIsEmpty',
      desc: '',
      args: [],
    );
  }

  /// `No portfolios yet`
  String get noPortfolios {
    return Intl.message(
      'No portfolios yet',
      name: 'noPortfolios',
      desc: '',
      args: [],
    );
  }

  /// `Add portfolio`
  String get addPortfolio {
    return Intl.message(
      'Add portfolio',
      name: 'addPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Edit Portfolio`
  String get portfolioEditEditPortfolio {
    return Intl.message(
      'Edit Portfolio',
      name: 'portfolioEditEditPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `New portfolio`
  String get newPortfolio {
    return Intl.message(
      'New portfolio',
      name: 'newPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Delete portfolio`
  String get deletePortfolio {
    return Intl.message(
      'Delete portfolio',
      name: 'deletePortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Enter portfolio name`
  String get portfolioEditNameHintText {
    return Intl.message(
      'Enter portfolio name',
      name: 'portfolioEditNameHintText',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a name`
  String get portfolioEditNameErrorText {
    return Intl.message(
      'Please enter a name',
      name: 'portfolioEditNameErrorText',
      desc: '',
      args: [],
    );
  }

  /// `Use app currency`
  String get portfolioEditUseAppCurrency {
    return Intl.message(
      'Use app currency',
      name: 'portfolioEditUseAppCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Default portfolio`
  String get portfolioEditDefaultPortfolio {
    return Intl.message(
      'Default portfolio',
      name: 'portfolioEditDefaultPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Value copied to clipboard`
  String get valueCopied {
    return Intl.message(
      'Value copied to clipboard',
      name: 'valueCopied',
      desc: '',
      args: [],
    );
  }

  /// `{coinSymbol} amount copied to clipboard`
  String coinAmountCopied(Object coinSymbol) {
    return Intl.message(
      '$coinSymbol amount copied to clipboard',
      name: 'coinAmountCopied',
      desc: '',
      args: [coinSymbol],
    );
  }

  /// `Portfolio locked`
  String get portfolioLocked {
    return Intl.message(
      'Portfolio locked',
      name: 'portfolioLocked',
      desc: '',
      args: [],
    );
  }

  /// `Transaction`
  String get transaction {
    return Intl.message(
      'Transaction',
      name: 'transaction',
      desc: '',
      args: [],
    );
  }

  /// `New transaction`
  String get newTransaction {
    return Intl.message(
      'New transaction',
      name: 'newTransaction',
      desc: '',
      args: [],
    );
  }

  /// `View in explorer`
  String get transactionEditViewInExplorer {
    return Intl.message(
      'View in explorer',
      name: 'transactionEditViewInExplorer',
      desc: '',
      args: [],
    );
  }

  /// `Delete transaction`
  String get deleteTransaction {
    return Intl.message(
      'Delete transaction',
      name: 'deleteTransaction',
      desc: '',
      args: [],
    );
  }

  /// `Coin Price`
  String get transactionEditCoinPrice {
    return Intl.message(
      'Coin Price',
      name: 'transactionEditCoinPrice',
      desc: '',
      args: [],
    );
  }

  /// `No data for the selected date`
  String get transactionEditNoDataForDate {
    return Intl.message(
      'No data for the selected date',
      name: 'transactionEditNoDataForDate',
      desc: '',
      args: [],
    );
  }

  /// `Enter a coin price`
  String get transactionEditCoinPriceHint {
    return Intl.message(
      'Enter a coin price',
      name: 'transactionEditCoinPriceHint',
      desc: '',
      args: [],
    );
  }

  /// `Use price from transaction date`
  String get transactionEditPriceFromDate {
    return Intl.message(
      'Use price from transaction date',
      name: 'transactionEditPriceFromDate',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Volume`
  String get transactionEditTransactionVolume {
    return Intl.message(
      'Transaction Volume',
      name: 'transactionEditTransactionVolume',
      desc: '',
      args: [],
    );
  }

  /// `Enter a number`
  String get transactionEditCoinAmountHint {
    return Intl.message(
      'Enter a number',
      name: 'transactionEditCoinAmountHint',
      desc: '',
      args: [],
    );
  }

  /// `Enter a number`
  String get transactionEditCurrencyAmountHint {
    return Intl.message(
      'Enter a number',
      name: 'transactionEditCurrencyAmountHint',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Date`
  String get transactionEditTransactionDate {
    return Intl.message(
      'Transaction Date',
      name: 'transactionEditTransactionDate',
      desc: '',
      args: [],
    );
  }

  /// `More options`
  String get transactionEditMoreOptions {
    return Intl.message(
      'More options',
      name: 'transactionEditMoreOptions',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Name`
  String get transactionEditTransactionName {
    return Intl.message(
      'Transaction Name',
      name: 'transactionEditTransactionName',
      desc: '',
      args: [],
    );
  }

  /// `Enter a transaction name`
  String get transactionEditTransactionNameHint {
    return Intl.message(
      'Enter a transaction name',
      name: 'transactionEditTransactionNameHint',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get transactionEditDescription {
    return Intl.message(
      'Description',
      name: 'transactionEditDescription',
      desc: '',
      args: [],
    );
  }

  /// `Enter a description`
  String get transactionEditDescriptionHint {
    return Intl.message(
      'Enter a description',
      name: 'transactionEditDescriptionHint',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Fee`
  String get transactionEditFee {
    return Intl.message(
      'Transaction Fee',
      name: 'transactionEditFee',
      desc: '',
      args: [],
    );
  }

  /// `Source Address`
  String get transactionEditSourceAddress {
    return Intl.message(
      'Source Address',
      name: 'transactionEditSourceAddress',
      desc: '',
      args: [],
    );
  }

  /// `Enter the source address`
  String get transactionEditSourceAddressHint {
    return Intl.message(
      'Enter the source address',
      name: 'transactionEditSourceAddressHint',
      desc: '',
      args: [],
    );
  }

  /// `Destination Address`
  String get transactionEditDestinationAddress {
    return Intl.message(
      'Destination Address',
      name: 'transactionEditDestinationAddress',
      desc: '',
      args: [],
    );
  }

  /// `Enter the destination address`
  String get transactionEditDestinationAddressHint {
    return Intl.message(
      'Enter the destination address',
      name: 'transactionEditDestinationAddressHint',
      desc: '',
      args: [],
    );
  }

  /// `Transaction ID`
  String get transactionEditTransactionId {
    return Intl.message(
      'Transaction ID',
      name: 'transactionEditTransactionId',
      desc: '',
      args: [],
    );
  }

  /// `Enter the transaction id`
  String get transactionEditTransactionIdHint {
    return Intl.message(
      'Enter the transaction id',
      name: 'transactionEditTransactionIdHint',
      desc: '',
      args: [],
    );
  }

  /// `Get from QR-Code`
  String get transactionEditQrCodeTooltip {
    return Intl.message(
      'Get from QR-Code',
      name: 'transactionEditQrCodeTooltip',
      desc: '',
      args: [],
    );
  }

  /// `No transaction id`
  String get transactionEditNoTransactionId {
    return Intl.message(
      'No transaction id',
      name: 'transactionEditNoTransactionId',
      desc: '',
      args: [],
    );
  }

  /// `No explorer URL set`
  String get transactionEditNoExplorerUrl {
    return Intl.message(
      'No explorer URL set',
      name: 'transactionEditNoExplorerUrl',
      desc: '',
      args: [],
    );
  }

  /// `Set URL`
  String get transactionEditSetUrl {
    return Intl.message(
      'Set URL',
      name: 'transactionEditSetUrl',
      desc: '',
      args: [],
    );
  }

  /// `Please select a portfolio`
  String get transactionEditSelectPortfolio {
    return Intl.message(
      'Please select a portfolio',
      name: 'transactionEditSelectPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Scan a code`
  String get qrScannerScanCode {
    return Intl.message(
      'Scan a code',
      name: 'qrScannerScanCode',
      desc: '',
      args: [],
    );
  }

  /// `Barcode type: `
  String get qrScannerBarcodeType {
    return Intl.message(
      'Barcode type: ',
      name: 'qrScannerBarcodeType',
      desc: '',
      args: [],
    );
  }

  /// `Data: `
  String get qrScannerData {
    return Intl.message(
      'Data: ',
      name: 'qrScannerData',
      desc: '',
      args: [],
    );
  }

  /// `No Permission`
  String get qrScannerNoPermission {
    return Intl.message(
      'No Permission',
      name: 'qrScannerNoPermission',
      desc: '',
      args: [],
    );
  }

  /// `Alert`
  String get alertTitle {
    return Intl.message(
      'Alert',
      name: 'alertTitle',
      desc: '',
      args: [],
    );
  }

  /// `{count, plural, one{Alert} other{Alerts}}`
  String alertCount(num count) {
    return Intl.plural(
      count,
      one: 'Alert',
      other: 'Alerts',
      name: 'alertCount',
      desc: '',
      args: [count],
    );
  }

  /// `Coin`
  String get alertCoin {
    return Intl.message(
      'Coin',
      name: 'alertCoin',
      desc: '',
      args: [],
    );
  }

  /// `Currency`
  String get alertCurrency {
    return Intl.message(
      'Currency',
      name: 'alertCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Current Price`
  String get alertCurrentPrice {
    return Intl.message(
      'Current Price',
      name: 'alertCurrentPrice',
      desc: '',
      args: [],
    );
  }

  /// `Target Value`
  String get alertTargetValue {
    return Intl.message(
      'Target Value',
      name: 'alertTargetValue',
      desc: '',
      args: [],
    );
  }

  /// `Enter a number`
  String get alertHintText {
    return Intl.message(
      'Enter a number',
      name: 'alertHintText',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid number`
  String get alertErrorText {
    return Intl.message(
      'Please enter a valid number',
      name: 'alertErrorText',
      desc: '',
      args: [],
    );
  }

  /// `Price`
  String get alertPrice {
    return Intl.message(
      'Price',
      name: 'alertPrice',
      desc: '',
      args: [],
    );
  }

  /// `Change 24H (%)`
  String get alertChange {
    return Intl.message(
      'Change 24H (%)',
      name: 'alertChange',
      desc: '',
      args: [],
    );
  }

  /// `Price Movement`
  String get alertPriceMovement {
    return Intl.message(
      'Price Movement',
      name: 'alertPriceMovement',
      desc: '',
      args: [],
    );
  }

  /// `Moves Above`
  String get alertAbove {
    return Intl.message(
      'Moves Above',
      name: 'alertAbove',
      desc: '',
      args: [],
    );
  }

  /// `Is Equal`
  String get alertEqual {
    return Intl.message(
      'Is Equal',
      name: 'alertEqual',
      desc: '',
      args: [],
    );
  }

  /// `Moves Below`
  String get alertBelow {
    return Intl.message(
      'Moves Below',
      name: 'alertBelow',
      desc: '',
      args: [],
    );
  }

  /// `Alert Frequency`
  String get alertFrequency {
    return Intl.message(
      'Alert Frequency',
      name: 'alertFrequency',
      desc: '',
      args: [],
    );
  }

  /// `Once`
  String get alertOnce {
    return Intl.message(
      'Once',
      name: 'alertOnce',
      desc: '',
      args: [],
    );
  }

  /// `Repeat`
  String get alertRepeat {
    return Intl.message(
      'Repeat',
      name: 'alertRepeat',
      desc: '',
      args: [],
    );
  }

  /// `Delete Alert`
  String get alertDeleteTooltip {
    return Intl.message(
      'Delete Alert',
      name: 'alertDeleteTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Add Alert`
  String get alertAddTooltip {
    return Intl.message(
      'Add Alert',
      name: 'alertAddTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Show coin details`
  String get alertEntryOpenDetailTooltip {
    return Intl.message(
      'Show coin details',
      name: 'alertEntryOpenDetailTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Delete all alerts`
  String get alertEntryDeleteAllAlertsTooltip {
    return Intl.message(
      'Delete all alerts',
      name: 'alertEntryDeleteAllAlertsTooltip',
      desc: '',
      args: [],
    );
  }

  /// `{coinName} Price Alert`
  String notificationPriceAlertTitle(Object coinName) {
    return Intl.message(
      '$coinName Price Alert',
      name: 'notificationPriceAlertTitle',
      desc: '',
      args: [coinName],
    );
  }

  /// `Price {movementText} {targetValue} {currencySymbol}`
  String notificationPriceAlertMessagePrice(
      Object movementText, Object targetValue, Object currencySymbol) {
    return Intl.message(
      'Price $movementText $targetValue $currencySymbol',
      name: 'notificationPriceAlertMessagePrice',
      desc: '',
      args: [movementText, targetValue, currencySymbol],
    );
  }

  /// `24H change {movementText} {targetValue}%`
  String notificationPriceAlertMessagePercent(
      Object movementText, Object targetValue) {
    return Intl.message(
      '24H change $movementText $targetValue%',
      name: 'notificationPriceAlertMessagePercent',
      desc: '',
      args: [movementText, targetValue],
    );
  }

  /// `24H change {movementText} {targetValue}%. Now at {coinPrice} {currencySymbol}`
  String notificationPriceAlertMessagePercentWithPrice(Object movementText,
      Object targetValue, Object coinPrice, Object currencySymbol) {
    return Intl.message(
      '24H change $movementText $targetValue%. Now at $coinPrice $currencySymbol',
      name: 'notificationPriceAlertMessagePercentWithPrice',
      desc: '',
      args: [movementText, targetValue, coinPrice, currencySymbol],
    );
  }

  /// `moved above`
  String get notificationPriceAlertAbove {
    return Intl.message(
      'moved above',
      name: 'notificationPriceAlertAbove',
      desc: '',
      args: [],
    );
  }

  /// `is equal to`
  String get notificationPriceAlertEqual {
    return Intl.message(
      'is equal to',
      name: 'notificationPriceAlertEqual',
      desc: '',
      args: [],
    );
  }

  /// `moved below`
  String get notificationPriceAlertBelow {
    return Intl.message(
      'moved below',
      name: 'notificationPriceAlertBelow',
      desc: '',
      args: [],
    );
  }

  /// `Large Price Movements`
  String get notificationLargeMoveAlertChannelName {
    return Intl.message(
      'Large Price Movements',
      name: 'notificationLargeMoveAlertChannelName',
      desc: '',
      args: [],
    );
  }

  /// `Large {coinName} Price Movement`
  String notificationLargeMoveAlertTitle(Object coinName) {
    return Intl.message(
      'Large $coinName Price Movement',
      name: 'notificationLargeMoveAlertTitle',
      desc: '',
      args: [coinName],
    );
  }

  /// `{coinName} Price changed {percentage}% in the last 24H. Now at {coinPrice} {currencySymbol}`
  String notificationLargeMoveAlertMessage(Object coinName, Object percentage,
      Object coinPrice, Object currencySymbol) {
    return Intl.message(
      '$coinName Price changed $percentage% in the last 24H. Now at $coinPrice $currencySymbol',
      name: 'notificationLargeMoveAlertMessage',
      desc: '',
      args: [coinName, percentage, coinPrice, currencySymbol],
    );
  }

  /// `Favorite`
  String get notificationLargeMoveAlertCoinNamePlaceholder {
    return Intl.message(
      'Favorite',
      name: 'notificationLargeMoveAlertCoinNamePlaceholder',
      desc: '',
      args: [],
    );
  }

  /// `Global Market Cap: {marketCap} {currencySymbol}`
  String globalMarketCap(Object marketCap, Object currencySymbol) {
    return Intl.message(
      'Global Market Cap: $marketCap $currencySymbol',
      name: 'globalMarketCap',
      desc: '',
      args: [marketCap, currencySymbol],
    );
  }

  /// `Global 24H Volume: {volume} {currencySymbol}`
  String globalVolume(Object volume, Object currencySymbol) {
    return Intl.message(
      'Global 24H Volume: $volume $currencySymbol',
      name: 'globalVolume',
      desc: '',
      args: [volume, currencySymbol],
    );
  }

  /// `Global Market Info`
  String get globalMarketInfoTile {
    return Intl.message(
      'Global Market Info',
      name: 'globalMarketInfoTile',
      desc: '',
      args: [],
    );
  }

  /// `Global Market Cap: {marketCap} {currencySymbol}`
  String globalMarketInfoMarketCap(Object marketCap, Object currencySymbol) {
    return Intl.message(
      'Global Market Cap: $marketCap $currencySymbol',
      name: 'globalMarketInfoMarketCap',
      desc: '',
      args: [marketCap, currencySymbol],
    );
  }

  /// `Market Cap 24H Change: `
  String get globalMarketInfoMarketCapChange {
    return Intl.message(
      'Market Cap 24H Change: ',
      name: 'globalMarketInfoMarketCapChange',
      desc: '',
      args: [],
    );
  }

  /// `24H Volume: {volume} {currencySymbol}`
  String globalMarketInfoGlobalVolume(Object volume, Object currencySymbol) {
    return Intl.message(
      '24H Volume: $volume $currencySymbol',
      name: 'globalMarketInfoGlobalVolume',
      desc: '',
      args: [volume, currencySymbol],
    );
  }

  /// `Active Cryptocurrencies: {active}`
  String globalMarketInfoActiveCrypto(Object active) {
    return Intl.message(
      'Active Cryptocurrencies: $active',
      name: 'globalMarketInfoActiveCrypto',
      desc: '',
      args: [active],
    );
  }

  /// `Coin Market Shares: `
  String get globalMarketInfoMarketShare {
    return Intl.message(
      'Coin Market Shares: ',
      name: 'globalMarketInfoMarketShare',
      desc: '',
      args: [],
    );
  }

  /// `Currency`
  String get settingsCurrency {
    return Intl.message(
      'Currency',
      name: 'settingsCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Select Currency`
  String get selectCurrencyTitle {
    return Intl.message(
      'Select Currency',
      name: 'selectCurrencyTitle',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get settingsLanguage {
    return Intl.message(
      'Language',
      name: 'settingsLanguage',
      desc: '',
      args: [],
    );
  }

  /// `Select Language`
  String get selectLanguageTitle {
    return Intl.message(
      'Select Language',
      name: 'selectLanguageTitle',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get settingsEnglish {
    return Intl.message(
      'English',
      name: 'settingsEnglish',
      desc: '',
      args: [],
    );
  }

  /// `Deutsch`
  String get settingsGerman {
    return Intl.message(
      'Deutsch',
      name: 'settingsGerman',
      desc: '',
      args: [],
    );
  }

  /// `Start screen`
  String get settingsStartScreen {
    return Intl.message(
      'Start screen',
      name: 'settingsStartScreen',
      desc: '',
      args: [],
    );
  }

  /// `Select start screen`
  String get selectStartScreenTitle {
    return Intl.message(
      'Select start screen',
      name: 'selectStartScreenTitle',
      desc: '',
      args: [],
    );
  }

  /// `Portfolio`
  String get settingsPortfolio {
    return Intl.message(
      'Portfolio',
      name: 'settingsPortfolio',
      desc: '',
      args: [],
    );
  }

  /// `Add to coin favorites`
  String get settingsPortfolioAddFavoriteTitle {
    return Intl.message(
      'Add to coin favorites',
      name: 'settingsPortfolioAddFavoriteTitle',
      desc: '',
      args: [],
    );
  }

  /// `Add the new wallet to favorites`
  String get settingsPortfolioAddToFavoriteSubtitle {
    return Intl.message(
      'Add the new wallet to favorites',
      name: 'settingsPortfolioAddToFavoriteSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Change Percentage Period`
  String get settingsPortfolioChangePercentagePeriodTitle {
    return Intl.message(
      'Change Percentage Period',
      name: 'settingsPortfolioChangePercentagePeriodTitle',
      desc: '',
      args: [],
    );
  }

  /// `Transactions`
  String get settingsPortfolioTransactions {
    return Intl.message(
      'Transactions',
      name: 'settingsPortfolioTransactions',
      desc: '',
      args: [],
    );
  }

  /// `Value in portfolio currency`
  String get settingsPortfolioTransactionsPortfolioCurrencyTitle {
    return Intl.message(
      'Value in portfolio currency',
      name: 'settingsPortfolioTransactionsPortfolioCurrencyTitle',
      desc: '',
      args: [],
    );
  }

  /// `Display the transaction value in the portfolio currency`
  String get settingsPortfolioTransactionsPortfolioCurrencySubtitle {
    return Intl.message(
      'Display the transaction value in the portfolio currency',
      name: 'settingsPortfolioTransactionsPortfolioCurrencySubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Use transaction coin price`
  String get useOwnCoinPriceTitle {
    return Intl.message(
      'Use transaction coin price',
      name: 'useOwnCoinPriceTitle',
      desc: '',
      args: [],
    );
  }

  /// `Use the transaction coin price for currency value in the list`
  String get useOwnCoinPriceSubtitle {
    return Intl.message(
      'Use the transaction coin price for currency value in the list',
      name: 'useOwnCoinPriceSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Block-Explorer URLs`
  String get settingsPortfolioTransactionsExplorerUrlsTitle {
    return Intl.message(
      'Block-Explorer URLs',
      name: 'settingsPortfolioTransactionsExplorerUrlsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Change transaction urls of block explorers`
  String get settingsPortfolioTransactionsExplorerUrlsSubtitle {
    return Intl.message(
      'Change transaction urls of block explorers',
      name: 'settingsPortfolioTransactionsExplorerUrlsSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Transaction URLs`
  String get transactionUrlsListTitle {
    return Intl.message(
      'Transaction URLs',
      name: 'transactionUrlsListTitle',
      desc: '',
      args: [],
    );
  }

  /// `Add new url`
  String get addTransactionUrl {
    return Intl.message(
      'Add new url',
      name: 'addTransactionUrl',
      desc: '',
      args: [],
    );
  }

  /// `Explorer URL`
  String get explorerUrlTitle {
    return Intl.message(
      'Explorer URL',
      name: 'explorerUrlTitle',
      desc: '',
      args: [],
    );
  }

  /// `Select a coin`
  String get explorerUrlSelectACoin {
    return Intl.message(
      'Select a coin',
      name: 'explorerUrlSelectACoin',
      desc: '',
      args: [],
    );
  }

  /// `Transaction URL`
  String get explorerUrlTransactionUrl {
    return Intl.message(
      'Transaction URL',
      name: 'explorerUrlTransactionUrl',
      desc: '',
      args: [],
    );
  }

  /// `Enter a url`
  String get explorerUrlTransactionUrlHint {
    return Intl.message(
      'Enter a url',
      name: 'explorerUrlTransactionUrlHint',
      desc: '',
      args: [],
    );
  }

  /// `The transaction id is added directly behind the URL, therefore the URL has to be this format:`
  String get explorerUrlTransactionUrlInfo {
    return Intl.message(
      'The transaction id is added directly behind the URL, therefore the URL has to be this format:',
      name: 'explorerUrlTransactionUrlInfo',
      desc: '',
      args: [],
    );
  }

  /// `https://explorer.com/path/to/transaction/`
  String get explorerUrlTransactionUrlInfoExample {
    return Intl.message(
      'https://explorer.com/path/to/transaction/',
      name: 'explorerUrlTransactionUrlInfoExample',
      desc: '',
      args: [],
    );
  }

  /// `No coin selected`
  String get explorerUrlNoCoinSelected {
    return Intl.message(
      'No coin selected',
      name: 'explorerUrlNoCoinSelected',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get settingsNotifications {
    return Intl.message(
      'Notifications',
      name: 'settingsNotifications',
      desc: '',
      args: [],
    );
  }

  /// `Show price in percentage alerts`
  String get settingsNotificationsShowPriceTitle {
    return Intl.message(
      'Show price in percentage alerts',
      name: 'settingsNotificationsShowPriceTitle',
      desc: '',
      args: [],
    );
  }

  /// `Show the coin price in 24H change alerts`
  String get settingsNotificationsShowPriceSubTitle {
    return Intl.message(
      'Show the coin price in 24H change alerts',
      name: 'settingsNotificationsShowPriceSubTitle',
      desc: '',
      args: [],
    );
  }

  /// `Large Price Movements`
  String get settingsNotificationsLargePriceMoveAlerts {
    return Intl.message(
      'Large Price Movements',
      name: 'settingsNotificationsLargePriceMoveAlerts',
      desc: '',
      args: [],
    );
  }

  /// `Enable alerts`
  String get settingsNotificationsEnableMoveAlerts {
    return Intl.message(
      'Enable alerts',
      name: 'settingsNotificationsEnableMoveAlerts',
      desc: '',
      args: [],
    );
  }

  /// `Show notification for large price moves of favorites`
  String get settingsNotificationsEnableAlertsText {
    return Intl.message(
      'Show notification for large price moves of favorites',
      name: 'settingsNotificationsEnableAlertsText',
      desc: '',
      args: [],
    );
  }

  /// `Price move threshold`
  String get settingsNotificationsMoveThreshold {
    return Intl.message(
      'Price move threshold',
      name: 'settingsNotificationsMoveThreshold',
      desc: '',
      args: [],
    );
  }

  /// `Enter a number`
  String get settingsNotificationsMoveThresholdHint {
    return Intl.message(
      'Enter a number',
      name: 'settingsNotificationsMoveThresholdHint',
      desc: '',
      args: [],
    );
  }

  /// `Security`
  String get settingsSecurity {
    return Intl.message(
      'Security',
      name: 'settingsSecurity',
      desc: '',
      args: [],
    );
  }

  /// `Hide all balances in the app`
  String get settingsSecurityPrivateModeSubtitle {
    return Intl.message(
      'Hide all balances in the app',
      name: 'settingsSecurityPrivateModeSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Password lock`
  String get settingsSecurityPasswordLock {
    return Intl.message(
      'Password lock',
      name: 'settingsSecurityPasswordLock',
      desc: '',
      args: [],
    );
  }

  /// `Lock the app with a password or biometric data`
  String get settingsSecurityPasswordLockSubtitle {
    return Intl.message(
      'Lock the app with a password or biometric data',
      name: 'settingsSecurityPasswordLockSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Change Password`
  String get settingsSecurityChangePassword {
    return Intl.message(
      'Change Password',
      name: 'settingsSecurityChangePassword',
      desc: '',
      args: [],
    );
  }

  /// `Allow biometric unlock`
  String get settingsSecurityBiometricUnlock {
    return Intl.message(
      'Allow biometric unlock',
      name: 'settingsSecurityBiometricUnlock',
      desc: '',
      args: [],
    );
  }

  /// `Allow biometric authentication to unlock the app`
  String get settingsSecurityBiometricUnlockSubtitle {
    return Intl.message(
      'Allow biometric authentication to unlock the app',
      name: 'settingsSecurityBiometricUnlockSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Authenticate on app start`
  String get settingsSecurityAuthOnAppStart {
    return Intl.message(
      'Authenticate on app start',
      name: 'settingsSecurityAuthOnAppStart',
      desc: '',
      args: [],
    );
  }

  /// `Open password screen when starting the app`
  String get settingsSecurityAuthOnAppStartSubtitle {
    return Intl.message(
      'Open password screen when starting the app',
      name: 'settingsSecurityAuthOnAppStartSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Keep app unlocked after closing`
  String get settingsSecurityKeepUnlocked {
    return Intl.message(
      'Keep app unlocked after closing',
      name: 'settingsSecurityKeepUnlocked',
      desc: '',
      args: [],
    );
  }

  /// `Keep app unlocked for`
  String get settingsSecurityKeepUnlockedDialogTitle {
    return Intl.message(
      'Keep app unlocked for',
      name: 'settingsSecurityKeepUnlockedDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `{count} minutes`
  String minutes(Object count) {
    return Intl.message(
      '$count minutes',
      name: 'minutes',
      desc: '',
      args: [count],
    );
  }

  /// `Never`
  String get never {
    return Intl.message(
      'Never',
      name: 'never',
      desc: '',
      args: [],
    );
  }

  /// `Lock now`
  String get settingsSecurityLockNow {
    return Intl.message(
      'Lock now',
      name: 'settingsSecurityLockNow',
      desc: '',
      args: [],
    );
  }

  /// `Appearance`
  String get settingsAppearance {
    return Intl.message(
      'Appearance',
      name: 'settingsAppearance',
      desc: '',
      args: [],
    );
  }

  /// `App`
  String get settingsAppearanceApp {
    return Intl.message(
      'App',
      name: 'settingsAppearanceApp',
      desc: '',
      args: [],
    );
  }

  /// `Color`
  String get settingsAppearanceColor {
    return Intl.message(
      'Color',
      name: 'settingsAppearanceColor',
      desc: '',
      args: [],
    );
  }

  /// `Select the primary color of the app`
  String get settingsAppearanceColorSubtitle {
    return Intl.message(
      'Select the primary color of the app',
      name: 'settingsAppearanceColorSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Select a color`
  String get settingsAppearanceColorDialogTitle {
    return Intl.message(
      'Select a color',
      name: 'settingsAppearanceColorDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get settingsAppearanceTheme {
    return Intl.message(
      'Theme',
      name: 'settingsAppearanceTheme',
      desc: '',
      args: [],
    );
  }

  /// `Select a theme`
  String get settingsAppearanceThemeDialogTitle {
    return Intl.message(
      'Select a theme',
      name: 'settingsAppearanceThemeDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `System default`
  String get settingsAppearanceThemeSystem {
    return Intl.message(
      'System default',
      name: 'settingsAppearanceThemeSystem',
      desc: '',
      args: [],
    );
  }

  /// `System default (AMOLED)`
  String get settingsAppearanceThemeSystemAmoled {
    return Intl.message(
      'System default (AMOLED)',
      name: 'settingsAppearanceThemeSystemAmoled',
      desc: '',
      args: [],
    );
  }

  /// `Light theme`
  String get settingsAppearanceThemeLight {
    return Intl.message(
      'Light theme',
      name: 'settingsAppearanceThemeLight',
      desc: '',
      args: [],
    );
  }

  /// `Dark theme`
  String get settingsAppearanceThemeDark {
    return Intl.message(
      'Dark theme',
      name: 'settingsAppearanceThemeDark',
      desc: '',
      args: [],
    );
  }

  /// `AMOLED theme`
  String get settingsAppearanceThemeAmoled {
    return Intl.message(
      'AMOLED theme',
      name: 'settingsAppearanceThemeAmoled',
      desc: '',
      args: [],
    );
  }

  /// `Numbers`
  String get settingsAppearanceNumbers {
    return Intl.message(
      'Numbers',
      name: 'settingsAppearanceNumbers',
      desc: '',
      args: [],
    );
  }

  /// `Date format`
  String get settingsAppearanceDateFormat {
    return Intl.message(
      'Date format',
      name: 'settingsAppearanceDateFormat',
      desc: '',
      args: [],
    );
  }

  /// `Select date format`
  String get settingsAppearanceDateFormatTitle {
    return Intl.message(
      'Select date format',
      name: 'settingsAppearanceDateFormatTitle',
      desc: '',
      args: [],
    );
  }

  /// `Number format`
  String get settingsAppearanceNumberFormat {
    return Intl.message(
      'Number format',
      name: 'settingsAppearanceNumberFormat',
      desc: '',
      args: [],
    );
  }

  /// `Select number format`
  String get settingsAppearanceNumberFormatTitle {
    return Intl.message(
      'Select number format',
      name: 'settingsAppearanceNumberFormatTitle',
      desc: '',
      args: [],
    );
  }

  /// `Backup`
  String get settingsImportExport {
    return Intl.message(
      'Backup',
      name: 'settingsImportExport',
      desc: '',
      args: [],
    );
  }

  /// `Import from file`
  String get settingsImportExportImportFileTitle {
    return Intl.message(
      'Import from file',
      name: 'settingsImportExportImportFileTitle',
      desc: '',
      args: [],
    );
  }

  /// `Import data from a json file`
  String get settingsImportExportImportFileSubtitle {
    return Intl.message(
      'Import data from a json file',
      name: 'settingsImportExportImportFileSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Export`
  String get settingsImportExportExportFileTitle {
    return Intl.message(
      'Export',
      name: 'settingsImportExportExportFileTitle',
      desc: '',
      args: [],
    );
  }

  /// `Export favorites, alerts and portfolios`
  String get settingsImportExportExportFileSubtitle {
    return Intl.message(
      'Export favorites, alerts and portfolios',
      name: 'settingsImportExportExportFileSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Import successful`
  String get settingsImportExportImportSuccessful {
    return Intl.message(
      'Import successful',
      name: 'settingsImportExportImportSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `Export successful`
  String get settingsImportExportExportSuccessful {
    return Intl.message(
      'Export successful',
      name: 'settingsImportExportExportSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `Wrong file format`
  String get settingsImportExportWrongFormat {
    return Intl.message(
      'Wrong file format',
      name: 'settingsImportExportWrongFormat',
      desc: '',
      args: [],
    );
  }

  /// `Import Portfolio`
  String get settingsImportExportImportPortfolioName {
    return Intl.message(
      'Import Portfolio',
      name: 'settingsImportExportImportPortfolioName',
      desc: '',
      args: [],
    );
  }

  /// `Overwrite data`
  String get settingsImportExportOverwriteAlertTitle {
    return Intl.message(
      'Overwrite data',
      name: 'settingsImportExportOverwriteAlertTitle',
      desc: '',
      args: [],
    );
  }

  /// `Existing data will be permanently overwritten.\nDo you wish to continue?`
  String get settingsImportExportOverwriteAlertMessage {
    return Intl.message(
      'Existing data will be permanently overwritten.\nDo you wish to continue?',
      name: 'settingsImportExportOverwriteAlertMessage',
      desc: '',
      args: [],
    );
  }

  /// `Automatic backup`
  String get settingsImportExportAutoBackup {
    return Intl.message(
      'Automatic backup',
      name: 'settingsImportExportAutoBackup',
      desc: '',
      args: [],
    );
  }

  /// `Grant storage permission`
  String get autoBackupPermissionRationaleTitle {
    return Intl.message(
      'Grant storage permission',
      name: 'autoBackupPermissionRationaleTitle',
      desc: '',
      args: [],
    );
  }

  /// `The app needs permission to access the device storage to create backups of your data automatically.`
  String get autoBackupPermissionRationaleText {
    return Intl.message(
      'The app needs permission to access the device storage to create backups of your data automatically.',
      name: 'autoBackupPermissionRationaleText',
      desc: '',
      args: [],
    );
  }

  /// `Backup location`
  String get settingsAutoBackupLocation {
    return Intl.message(
      'Backup location',
      name: 'settingsAutoBackupLocation',
      desc: '',
      args: [],
    );
  }

  /// `No backup location`
  String get autoBackupNoBackupLocation {
    return Intl.message(
      'No backup location',
      name: 'autoBackupNoBackupLocation',
      desc: '',
      args: [],
    );
  }

  /// `Just now, {time}`
  String autoBackupJustNow(Object time) {
    return Intl.message(
      'Just now, $time',
      name: 'autoBackupJustNow',
      desc: '',
      args: [time],
    );
  }

  /// `{count, plural, one{1 minute ago, } other{{count} minutes ago, }}`
  String autoBackupMinutes(num count) {
    return Intl.plural(
      count,
      one: '1 minute ago, ',
      other: '$count minutes ago, ',
      name: 'autoBackupMinutes',
      desc: '',
      args: [count],
    );
  }

  /// `{count, plural, one{1 hour ago, } other{{count} hours ago, }}`
  String autoBackupHours(num count) {
    return Intl.plural(
      count,
      one: '1 hour ago, ',
      other: '$count hours ago, ',
      name: 'autoBackupHours',
      desc: '',
      args: [count],
    );
  }

  /// `Yesterday, {time}`
  String autoBackupYesterday(Object time) {
    return Intl.message(
      'Yesterday, $time',
      name: 'autoBackupYesterday',
      desc: '',
      args: [time],
    );
  }

  /// `{date}, {time}`
  String autoBackupDate(Object date, Object time) {
    return Intl.message(
      '$date, $time',
      name: 'autoBackupDate',
      desc: '',
      args: [date, time],
    );
  }

  /// `Backup file name`
  String get settingsAutoBackupFileName {
    return Intl.message(
      'Backup file name',
      name: 'settingsAutoBackupFileName',
      desc: '',
      args: [],
    );
  }

  /// `Rename file`
  String get autoBackupChangeFileName {
    return Intl.message(
      'Rename file',
      name: 'autoBackupChangeFileName',
      desc: '',
      args: [],
    );
  }

  /// `Enter file name`
  String get autoBackupEnterFileNameHint {
    return Intl.message(
      'Enter file name',
      name: 'autoBackupEnterFileNameHint',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a name`
  String get autoBackupErrorNoFileName {
    return Intl.message(
      'Please enter a name',
      name: 'autoBackupErrorNoFileName',
      desc: '',
      args: [],
    );
  }

  /// `Invalid character: {char}`
  String autoBackupErrorInvalidCharacter(Object char) {
    return Intl.message(
      'Invalid character: $char',
      name: 'autoBackupErrorInvalidCharacter',
      desc: '',
      args: [char],
    );
  }

  /// `Overwrite old file`
  String get settingsAutoBackupOverwriteTitle {
    return Intl.message(
      'Overwrite old file',
      name: 'settingsAutoBackupOverwriteTitle',
      desc: '',
      args: [],
    );
  }

  /// `Overwrite the old file when making a new backup`
  String get settingsAutoBackupOverwriteSubtitle {
    return Intl.message(
      'Overwrite the old file when making a new backup',
      name: 'settingsAutoBackupOverwriteSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get settingsAbout {
    return Intl.message(
      'About',
      name: 'settingsAbout',
      desc: '',
      args: [],
    );
  }

  /// `Changelog`
  String get aboutChangelog {
    return Intl.message(
      'Changelog',
      name: 'aboutChangelog',
      desc: '',
      args: [],
    );
  }

  /// `Version`
  String get aboutVersion {
    return Intl.message(
      'Version',
      name: 'aboutVersion',
      desc: '',
      args: [],
    );
  }

  /// `GitLab`
  String get aboutGitLab {
    return Intl.message(
      'GitLab',
      name: 'aboutGitLab',
      desc: '',
      args: [],
    );
  }

  /// `View Source Code, report issues`
  String get aboutGitLabText {
    return Intl.message(
      'View Source Code, report issues',
      name: 'aboutGitLabText',
      desc: '',
      args: [],
    );
  }

  /// `Licenses`
  String get aboutLicenses {
    return Intl.message(
      'Licenses',
      name: 'aboutLicenses',
      desc: '',
      args: [],
    );
  }

  /// `Licenses of libraries used`
  String get aboutLicensesText {
    return Intl.message(
      'Licenses of libraries used',
      name: 'aboutLicensesText',
      desc: '',
      args: [],
    );
  }

  /// `Powered by`
  String get aboutGroupCoinGecko {
    return Intl.message(
      'Powered by',
      name: 'aboutGroupCoinGecko',
      desc: '',
      args: [],
    );
  }

  /// `CoinGecko API`
  String get aboutCoinGeckoName {
    return Intl.message(
      'CoinGecko API',
      name: 'aboutCoinGeckoName',
      desc: '',
      args: [],
    );
  }

  /// `Visit website`
  String get aboutCoinGeckoWebsite {
    return Intl.message(
      'Visit website',
      name: 'aboutCoinGeckoWebsite',
      desc: '',
      args: [],
    );
  }

  /// `Developer`
  String get aboutGroupAuthor {
    return Intl.message(
      'Developer',
      name: 'aboutGroupAuthor',
      desc: '',
      args: [],
    );
  }

  /// `Clone Apps`
  String get aboutAuthorName {
    return Intl.message(
      'Clone Apps',
      name: 'aboutAuthorName',
      desc: '',
      args: [],
    );
  }

  /// `Germany`
  String get aboutAuthorCountry {
    return Intl.message(
      'Germany',
      name: 'aboutAuthorCountry',
      desc: '',
      args: [],
    );
  }

  /// `Write an email`
  String get aboutAuthorMail {
    return Intl.message(
      'Write an email',
      name: 'aboutAuthorMail',
      desc: '',
      args: [],
    );
  }

  /// `Support`
  String get aboutGroupSupport {
    return Intl.message(
      'Support',
      name: 'aboutGroupSupport',
      desc: '',
      args: [],
    );
  }

  /// `Rate`
  String get aboutSupportRate {
    return Intl.message(
      'Rate',
      name: 'aboutSupportRate',
      desc: '',
      args: [],
    );
  }

  /// `Leave a review in the Google Play Store`
  String get aboutSupportRateText {
    return Intl.message(
      'Leave a review in the Google Play Store',
      name: 'aboutSupportRateText',
      desc: '',
      args: [],
    );
  }

  /// `Tools`
  String get settingsGroupTools {
    return Intl.message(
      'Tools',
      name: 'settingsGroupTools',
      desc: '',
      args: [],
    );
  }

  /// `Calculator`
  String get settingsCalculator {
    return Intl.message(
      'Calculator',
      name: 'settingsCalculator',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Enter password`
  String get editPasswordEnterHint {
    return Intl.message(
      'Enter password',
      name: 'editPasswordEnterHint',
      desc: '',
      args: [],
    );
  }

  /// `Repeat password`
  String get editPasswordRepeatHint {
    return Intl.message(
      'Repeat password',
      name: 'editPasswordRepeatHint',
      desc: '',
      args: [],
    );
  }

  /// `Remove password`
  String get editPasswordRemove {
    return Intl.message(
      'Remove password',
      name: 'editPasswordRemove',
      desc: '',
      args: [],
    );
  }

  /// `Show password`
  String get editPasswordShowPassword {
    return Intl.message(
      'Show password',
      name: 'editPasswordShowPassword',
      desc: '',
      args: [],
    );
  }

  /// `Remove password lock`
  String get editPasswordRemoveDialogTitle {
    return Intl.message(
      'Remove password lock',
      name: 'editPasswordRemoveDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a password`
  String get editPasswordNoPasswordError {
    return Intl.message(
      'Please enter a password',
      name: 'editPasswordNoPasswordError',
      desc: '',
      args: [],
    );
  }

  /// `Please repeat the password`
  String get editPasswordRepeatPasswordError {
    return Intl.message(
      'Please repeat the password',
      name: 'editPasswordRepeatPasswordError',
      desc: '',
      args: [],
    );
  }

  /// `Bitcoin`
  String get bitcoin {
    return Intl.message(
      'Bitcoin',
      name: 'bitcoin',
      desc: '',
      args: [],
    );
  }

  /// `Ethereum (and ERC20 Tokens)`
  String get ethereumAndTokens {
    return Intl.message(
      'Ethereum (and ERC20 Tokens)',
      name: 'ethereumAndTokens',
      desc: '',
      args: [],
    );
  }

  /// `Liberapay`
  String get liberapay {
    return Intl.message(
      'Liberapay',
      name: 'liberapay',
      desc: '',
      args: [],
    );
  }

  /// `Donate`
  String get donateTitle {
    return Intl.message(
      'Donate',
      name: 'donateTitle',
      desc: '',
      args: [],
    );
  }

  /// `Copy to clipboard`
  String get copyToClipBoard {
    return Intl.message(
      'Copy to clipboard',
      name: 'copyToClipBoard',
      desc: '',
      args: [],
    );
  }

  /// `Address copied to clipboard`
  String get copyToClipBoardAddressMessage {
    return Intl.message(
      'Address copied to clipboard',
      name: 'copyToClipBoardAddressMessage',
      desc: '',
      args: [],
    );
  }

  /// `Set up monthly donation`
  String get monthlyDonation {
    return Intl.message(
      'Set up monthly donation',
      name: 'monthlyDonation',
      desc: '',
      args: [],
    );
  }

  /// `About Rate Limitation`
  String get rateLimitInfoTitle {
    return Intl.message(
      'About Rate Limitation',
      name: 'rateLimitInfoTitle',
      desc: '',
      args: [],
    );
  }

  /// `Explanation`
  String get rateLimitInfoExplanation {
    return Intl.message(
      'Explanation',
      name: 'rateLimitInfoExplanation',
      desc: '',
      args: [],
    );
  }

  /// `The free version of the Coingecko API that this app uses is limiting the number of requests that can be sent to 50 per minute.`
  String get rateLimitInfoExplanationFirst {
    return Intl.message(
      'The free version of the Coingecko API that this app uses is limiting the number of requests that can be sent to 50 per minute.',
      name: 'rateLimitInfoExplanationFirst',
      desc: '',
      args: [],
    );
  }

  /// `If you have many favorites or wallets, this number can be exceeded easily, for example by frequent refreshing.`
  String get rateLimitInfoExplanationSecond {
    return Intl.message(
      'If you have many favorites or wallets, this number can be exceeded easily, for example by frequent refreshing.',
      name: 'rateLimitInfoExplanationSecond',
      desc: '',
      args: [],
    );
  }

  /// `Solution`
  String get rateLimitInfoSolution {
    return Intl.message(
      'Solution',
      name: 'rateLimitInfoSolution',
      desc: '',
      args: [],
    );
  }

  /// `If this error occurs, you need to wait approximately one minute before you can refresh the data again.`
  String get rateLimitInfoSolutionFirst {
    return Intl.message(
      'If this error occurs, you need to wait approximately one minute before you can refresh the data again.',
      name: 'rateLimitInfoSolutionFirst',
      desc: '',
      args: [],
    );
  }

  /// `The only other option is to pay for an API key.`
  String get rateLimitInfoSolutionSecond {
    return Intl.message(
      'The only other option is to pay for an API key.',
      name: 'rateLimitInfoSolutionSecond',
      desc: '',
      args: [],
    );
  }

  /// `At the moment, the cheapest paid version starts at $129 per month, therefore I, unfortunately, can't afford to pay for it alone.`
  String get rateLimitInfoSolutionThird {
    return Intl.message(
      'At the moment, the cheapest paid version starts at \$129 per month, therefore I, unfortunately, can\'t afford to pay for it alone.',
      name: 'rateLimitInfoSolutionThird',
      desc: '',
      args: [],
    );
  }

  /// `If you want to help pay for an API key and support the development of this app, you can do so through the donation links below.`
  String get rateLimitInfoSolutionFourth {
    return Intl.message(
      'If you want to help pay for an API key and support the development of this app, you can do so through the donation links below.',
      name: 'rateLimitInfoSolutionFourth',
      desc: '',
      args: [],
    );
  }

  /// `Coingecko API Prices`
  String get rateLimitInfoCoinGeckoPrices {
    return Intl.message(
      'Coingecko API Prices',
      name: 'rateLimitInfoCoinGeckoPrices',
      desc: '',
      args: [],
    );
  }

  /// `ID Migration Assistant`
  String get migrationAssistantTitle {
    return Intl.message(
      'ID Migration Assistant',
      name: 'migrationAssistantTitle',
      desc: '',
      args: [],
    );
  }

  /// `This will delete all incorrect data to ensure that the app continues to function correctly.\nThis action cannot be undone.`
  String get migrationAssistantAlertDialogMessage {
    return Intl.message(
      'This will delete all incorrect data to ensure that the app continues to function correctly.\nThis action cannot be undone.',
      name: 'migrationAssistantAlertDialogMessage',
      desc: '',
      args: [],
    );
  }

  /// `The coin identifiers used by the Web-API have changed and an automated migration is not possible.`
  String get migrationAssistantText1 {
    return Intl.message(
      'The coin identifiers used by the Web-API have changed and an automated migration is not possible.',
      name: 'migrationAssistantText1',
      desc: '',
      args: [],
    );
  }

  /// `A manual migration is necessary so that the app can continue to access the correct coin information and keep functioning correctly.`
  String get migrationAssistantText2 {
    return Intl.message(
      'A manual migration is necessary so that the app can continue to access the correct coin information and keep functioning correctly.',
      name: 'migrationAssistantText2',
      desc: '',
      args: [],
    );
  }

  /// `On the following screens you will have to select the new identifier for each coin that has changed.\nYou will be presented with a number of possible options to choose from.`
  String get migrationAssistantText3 {
    return Intl.message(
      'On the following screens you will have to select the new identifier for each coin that has changed.\nYou will be presented with a number of possible options to choose from.',
      name: 'migrationAssistantText3',
      desc: '',
      args: [],
    );
  }

  /// `Continue >`
  String get migrationAssistantContinueButton {
    return Intl.message(
      'Continue >',
      name: 'migrationAssistantContinueButton',
      desc: '',
      args: [],
    );
  }

  /// `< Back`
  String get migrationAssistantBackButton {
    return Intl.message(
      '< Back',
      name: 'migrationAssistantBackButton',
      desc: '',
      args: [],
    );
  }

  /// `Finish`
  String get migrationAssistantFinishButton {
    return Intl.message(
      'Finish',
      name: 'migrationAssistantFinishButton',
      desc: '',
      args: [],
    );
  }

  /// `Select the correct new coin`
  String get migrationAssistantEntrySelectCoin {
    return Intl.message(
      'Select the correct new coin',
      name: 'migrationAssistantEntrySelectCoin',
      desc: '',
      args: [],
    );
  }

  /// `Search coins`
  String get migrationAssistantEntrySearchCoins {
    return Intl.message(
      'Search coins',
      name: 'migrationAssistantEntrySearchCoins',
      desc: '',
      args: [],
    );
  }

  /// `Unlock app`
  String get authenticationTitle {
    return Intl.message(
      'Unlock app',
      name: 'authenticationTitle',
      desc: '',
      args: [],
    );
  }

  /// `Unlock`
  String get authenticationUnlockButton {
    return Intl.message(
      'Unlock',
      name: 'authenticationUnlockButton',
      desc: '',
      args: [],
    );
  }

  /// `Use biometric data`
  String get authenticationUseBiometrics {
    return Intl.message(
      'Use biometric data',
      name: 'authenticationUseBiometrics',
      desc: '',
      args: [],
    );
  }

  /// `Continue without password`
  String get authenticationContinueWithoutPassword {
    return Intl.message(
      'Continue without password',
      name: 'authenticationContinueWithoutPassword',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a password`
  String get authenticationNoPasswordError {
    return Intl.message(
      'Please enter a password',
      name: 'authenticationNoPasswordError',
      desc: '',
      args: [],
    );
  }

  /// `Wrong password`
  String get authenticateWrongPasswordError {
    return Intl.message(
      'Wrong password',
      name: 'authenticateWrongPasswordError',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `No internet connection`
  String get errorMessageSocket {
    return Intl.message(
      'No internet connection',
      name: 'errorMessageSocket',
      desc: '',
      args: [],
    );
  }

  /// `Service is currently unavailable`
  String get errorMessageHttp {
    return Intl.message(
      'Service is currently unavailable',
      name: 'errorMessageHttp',
      desc: '',
      args: [],
    );
  }

  /// `Too many requests. Please wait one minute before trying again`
  String get errorMessageRateLimitation {
    return Intl.message(
      'Too many requests. Please wait one minute before trying again',
      name: 'errorMessageRateLimitation',
      desc: '',
      args: [],
    );
  }

  /// `Too many requests`
  String get errorMessageRateLimitationShort {
    return Intl.message(
      'Too many requests',
      name: 'errorMessageRateLimitationShort',
      desc: '',
      args: [],
    );
  }

  /// `No data available`
  String get errorNoDataAvailable {
    return Intl.message(
      'No data available',
      name: 'errorNoDataAvailable',
      desc: '',
      args: [],
    );
  }

  /// `More information`
  String get moreInformation {
    return Intl.message(
      'More information',
      name: 'moreInformation',
      desc: '',
      args: [],
    );
  }

  /// `Not available in private mode`
  String get notAvailableInPrivateMode {
    return Intl.message(
      'Not available in private mode',
      name: 'notAvailableInPrivateMode',
      desc: '',
      args: [],
    );
  }

  /// `Authentication required`
  String get authenticationRequired {
    return Intl.message(
      'Authentication required',
      name: 'authenticationRequired',
      desc: '',
      args: [],
    );
  }

  /// `No biometric data available`
  String get noBiometrics {
    return Intl.message(
      'No biometric data available',
      name: 'noBiometrics',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'de'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
