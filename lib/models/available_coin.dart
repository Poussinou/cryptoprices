import 'package:hive/hive.dart';

part 'available_coin.g.dart';

///AvailableCoin class used to show the available coins in the search
@HiveType(typeId: 4)
class AvailableCoin extends HiveObject {
  ///The ID of the coin
  @HiveField(0)
  String id;

  ///The symbol of the coin
  @HiveField(1, defaultValue: "")
  String symbol;

  ///The name of the coin
  @HiveField(2, defaultValue: "")
  String name;

  ///When was this coin last searched for.
  ///Null if it has never been searched
  @HiveField(3)
  DateTime? lastSearched;

  AvailableCoin(this.id, {this.symbol = "", this.name = "", this.lastSearched});

  ///Converts json to a AvailableCoin object
  AvailableCoin.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        symbol = json['symbol'],
        name = json['name'];

}