// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coin.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CoinAdapter extends TypeAdapter<Coin> {
  @override
  final int typeId = 0;

  @override
  Coin read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Coin(
      fields[0] as String,
      symbol: fields[1] == null ? '' : fields[1] as String,
      name: fields[2] == null ? '' : fields[2] as String,
      imageURL: fields[4] == null ? '' : fields[4] as String,
      price: fields[7] == null ? 0 : fields[7] as double,
      marketCap: fields[14] == null ? 0 : fields[14] as double,
      marketCapRank: fields[30] == null ? 0 : fields[30] as int,
      totalVolume: fields[16] == null ? 0 : fields[16] as double,
      priceChangePercentage24h: fields[21] == null ? 0 : fields[21] as double,
      lastUpdated: fields[27] as DateTime?,
    );
  }

  @override
  void write(BinaryWriter writer, Coin obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.symbol)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.imageURL)
      ..writeByte(7)
      ..write(obj.price)
      ..writeByte(14)
      ..write(obj.marketCap)
      ..writeByte(30)
      ..write(obj.marketCapRank)
      ..writeByte(16)
      ..write(obj.totalVolume)
      ..writeByte(21)
      ..write(obj.priceChangePercentage24h)
      ..writeByte(27)
      ..write(obj.lastUpdated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CoinAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
