import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:hive/hive.dart';

part 'coin_detail.g.dart';

///Coin class with detailed information, including market data
@HiveType(typeId: 1)
class CoinDetail extends Coin {
  ///URL of the coin homepage
  @HiveField(3, defaultValue: "")
  String homepage;

  ///Genesis date of the coin
  @HiveField(6)
  DateTime? genesisDate;

  ///All-time high price
  @HiveField(8, defaultValue: 0)
  double atHigh;

  ///Price change since All-Time high in percent
  @HiveField(9, defaultValue: 0)
  double atHighChangePercentage;

  ///Date of the All-Time high
  @HiveField(10)
  DateTime? atHighDate;

  ///All-Time low price
  @HiveField(11, defaultValue: 0)
  double atLow;

  ///Price change since the All-Time low in percent
  @HiveField(12, defaultValue: 0)
  double atLowChangePercentage;

  ///Date of the All-Time low
  @HiveField(13)
  DateTime? atLowDate;

  ///The highest price in the last 24H
  @HiveField(17, defaultValue: 0)
  double high24h;

  ///The lowst price in the last 24H
  @HiveField(18, defaultValue: 0)
  double low24h;

  ///The total price change in the last 24H in a specific currency
  @HiveField(19, defaultValue: 0)
  double priceChange24hCurrency;

  ///The price change in the last 60 minutes in percent
  @HiveField(20, defaultValue: 0)
  double priceChangePercentage60m;

  ///The price change in the last 7 day in percent
  @HiveField(22, defaultValue: 0)
  double priceChangePercentage7d;

  ///The price change in the last 14 day in percent
  @HiveField(23, defaultValue: 0)
  double priceChangePercentage14d;

  ///The price change in the last 30 days in percent
  @HiveField(24, defaultValue: 0)
  double priceChangePercentage30d;

  ///The price change in the last year in percent
  @HiveField(25, defaultValue: 0)
  double priceChangePercentage1y;

  ///The current supply of the coin
  @HiveField(26, defaultValue: 0)
  double circulatingSupply;

  ///Map with market data values for the corresponding time period in marketDataDays
  @Deprecated("Deprecated since v1.3 / database version 2. Instead of CoinDetail objects, MarketData objects are saved in the MarketDataBox.")
  @HiveField(28, defaultValue: {})
  Map<String, MarketData> oldMarketChart; //Keys are marketDataDays from API "1", "7", "30", "365", "max"

  ///The market data associated with this coin.
  ///Each MarketData object belongs to one time period in marketDataDays
  @HiveField(29)
  HiveList<MarketData>? marketChart;

  CoinDetail(
      String id,
  {   String symbol = "",
      String name = "",
      String imageURL = "",

      double price = 0,

      double marketCap = 0,

      double totalVolume = 0,

      double priceChangePercentage24h = 0,

      DateTime? lastUpdated,

      this.homepage = "",
      this.genesisDate,

      this.atHigh = 0,
      this.atHighChangePercentage = 0,
      this.atHighDate,

      this.atLow = 0,
      this.atLowChangePercentage = 0,
      this.atLowDate,


      this.high24h = 0,
      this.low24h = 0,

      this.priceChange24hCurrency = 0,
      this.priceChangePercentage60m = 0,

      this.priceChangePercentage7d = 0,
      this.priceChangePercentage14d = 0,
      this.priceChangePercentage30d = 0,
      this.priceChangePercentage1y = 0,

      this.circulatingSupply = 0,

      this.oldMarketChart = const {},
      this.marketChart
      }) : super(
      id,
      symbol: symbol,
      name: name,
      imageURL: imageURL,
      price: price,
      marketCap: marketCap,
      totalVolume: totalVolume,
      priceChangePercentage24h: priceChangePercentage24h,
      lastUpdated: lastUpdated
  );

  ///Converts json to a CoinDetail object
  CoinDetail.fromJson(Map<String, dynamic> json, String setCurrency)
      : homepage = json['links']['homepage'][0]  ?? "",
        genesisDate = DateTime.parse(json['genesis_date'] ?? Constants.DATETIMEDEFAULT),

        atHigh = (json['market_data']['ath'][setCurrency] ?? 0).toDouble(),
        atHighChangePercentage = (json['market_data']['ath_change_percentage'][setCurrency] ?? 0).toDouble(),
        atHighDate = DateTime.parse(json['market_data']['ath_date'][setCurrency] ?? Constants.DATETIMEDEFAULT),

        atLow = (json['market_data']['atl'][setCurrency] ?? 0).toDouble(),
        atLowChangePercentage = (json['market_data']['atl_change_percentage'][setCurrency] ?? 0).toDouble(),
        atLowDate = DateTime.parse(json['market_data']['atl_date'][setCurrency]  ?? Constants.DATETIMEDEFAULT),

        high24h = (json['market_data']['high_24h'][setCurrency] ?? 0).toDouble(),
        low24h = (json['market_data']['low_24h'][setCurrency] ?? 0).toDouble(),

        priceChange24hCurrency = (json['market_data']['price_change_24h_in_currency'][setCurrency] ?? 0).toDouble(),
        priceChangePercentage60m = (json['market_data']['price_change_percentage_1h_in_currency'][setCurrency] ?? 0).toDouble(),
        priceChangePercentage7d = (json['market_data']['price_change_percentage_7d_in_currency'][setCurrency] ?? 0).toDouble(),
        priceChangePercentage14d = (json['market_data']['price_change_percentage_14d_in_currency'][setCurrency] ?? 0).toDouble(),
        priceChangePercentage30d = (json['market_data']['price_change_percentage_30d_in_currency'][setCurrency] ?? 0).toDouble(),
        priceChangePercentage1y = (json['market_data']['price_change_percentage_1y_in_currency'][setCurrency] ?? 0).toDouble(),

        circulatingSupply = (json['market_data']['circulating_supply'] ?? 0).toDouble(),

        oldMarketChart = {},

        super.fromDetailedJson(json, setCurrency);

  ///Gets the price change percentage over the entire time period of the market data
  double get priceChangePercentageMax {
    double firstPrice = marketChart!.where((element) => element.timePeriodDays == Constants.MARKETDATAKEYMAX).first.prices.first.price;
    double currentPrice = price;
    return ((currentPrice - firstPrice) / firstPrice) * 100;
  }
}