import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/large_move_alert.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/models/wallet.dart';

///The user data that uses this coin id
class CoinIdMigrationData {
  String oldCoinId;

  String oldCoinName;

  String oldCoinSymbol;

  List<FavoriteCoin> favorites;

  List<ExplorerTransactionUrl> explorerTransactionUrls;

  List<LargeMoveAlert> largeMoveAlerts;

  List<PriceAlert> priceAlerts;

  List<PriceAlertListEntry> priceAlertListEntries;

  List<Wallet> wallets;

  List<Transaction> transactions;

  CoinIdMigrationData(
    this.oldCoinId,
  {
    this.oldCoinName = "",
    this.oldCoinSymbol = "",
    this.favorites = const [],
    this.explorerTransactionUrls = const [],
    this.largeMoveAlerts = const [],
    this.priceAlerts = const [],
    this.priceAlertListEntries = const [],
    this.wallets = const [],
    this.transactions = const []
  }
  );
}