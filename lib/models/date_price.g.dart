// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'date_price.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DatePriceAdapter extends TypeAdapter<DatePrice> {
  @override
  final int typeId = 3;

  @override
  DatePrice read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DatePrice(
      fields[0] as DateTime,
      price: fields[1] == null ? 0 : fields[1] as double,
    );
  }

  @override
  void write(BinaryWriter writer, DatePrice obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.date)
      ..writeByte(1)
      ..write(obj.price);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DatePriceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
