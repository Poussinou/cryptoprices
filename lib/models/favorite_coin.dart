import 'package:hive/hive.dart';

part 'favorite_coin.g.dart';

///Contains the id of a favorite coin and its position in the favorite list
@HiveType(typeId: 5)
class FavoriteCoin extends HiveObject{
  ///The ID of the coin
  @HiveField(0)
  String id;

  ///The name of the coin
  @HiveField(2, defaultValue: "")
  String name;

  ///The symbol of the coin
  @HiveField(3, defaultValue: "")
  String symbol;

  ///The position in the favorite list of this coin
  @HiveField(1, defaultValue: 0)
  int favoriteListPosition;

  FavoriteCoin(this.id, {this.name = "", this.symbol = "",this.favoriteListPosition = 0});

  ///Converts a FavoriteCoin object to json
  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "symbol": symbol,
    "favoriteListPosition": favoriteListPosition,
  };

  ///Converts json to a FavoriteCoin object
  FavoriteCoin.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"] ?? "",
        symbol = json["symbol"] ?? "",
        favoriteListPosition = json["favoriteListPosition"];

}