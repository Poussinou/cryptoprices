// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_coin.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavoriteCoinAdapter extends TypeAdapter<FavoriteCoin> {
  @override
  final int typeId = 5;

  @override
  FavoriteCoin read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FavoriteCoin(
      fields[0] as String,
      name: fields[2] == null ? '' : fields[2] as String,
      symbol: fields[3] == null ? '' : fields[3] as String,
      favoriteListPosition: fields[1] == null ? 0 : fields[1] as int,
    );
  }

  @override
  void write(BinaryWriter writer, FavoriteCoin obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.symbol)
      ..writeByte(1)
      ..write(obj.favoriteListPosition);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavoriteCoinAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
