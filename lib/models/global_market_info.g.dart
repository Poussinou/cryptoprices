// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global_market_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class GlobalMarketInfoAdapter extends TypeAdapter<GlobalMarketInfo> {
  @override
  final int typeId = 9;

  @override
  GlobalMarketInfo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return GlobalMarketInfo(
      fields[0] as double,
      marketCapChangePercentage24H: fields[1] == null ? 0 : fields[1] as double,
      total24HVolume: fields[2] == null ? 0 : fields[2] as double,
      activeCryptoCurrencies: fields[3] == null ? 0 : fields[3] as int,
      marketCapCoinPercentages:
          fields[4] == null ? {} : (fields[4] as Map).cast<String, double>(),
    );
  }

  @override
  void write(BinaryWriter writer, GlobalMarketInfo obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.totalMarketCap)
      ..writeByte(1)
      ..write(obj.marketCapChangePercentage24H)
      ..writeByte(2)
      ..write(obj.total24HVolume)
      ..writeByte(3)
      ..write(obj.activeCryptoCurrencies)
      ..writeByte(4)
      ..write(obj.marketCapCoinPercentages);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GlobalMarketInfoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
