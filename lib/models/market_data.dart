import 'package:crypto_prices/util/util.dart';
import 'package:hive/hive.dart';

import 'date_price.dart';

part 'market_data.g.dart';

///Contains the prices of a coin at specific dates
@HiveType(typeId: 2)
class MarketData extends HiveObject{
  ///The ID of the market data
  @HiveField(1, defaultValue: "")
  String id;

  ///The ID of the coin to which this data belongs
  @HiveField(2)
  String coinId;

  ///The coin prices at all dates of the time period.
  ///Last element is the newest
  @HiveField(0)
  List<DatePrice> prices = [];

  ///The time period that this data in from in days.
  ///The values are constants (MARKETDATAKEY...)
  @HiveField(3)
  String timePeriodDays;

  ///The currency of the prices
  @HiveField(5, defaultValue: "")
  String currency;

  ///Whn was this data last updated in the database
  @HiveField(4)
  DateTime? lastUpdated;

  MarketData(
    this.id,
    this.coinId,
    this.prices,
    this.timePeriodDays,
    this.currency,
  {
    this.lastUpdated
  }
  );

  MarketData.fromJson(Map<String, dynamic> json, String id, String coinId, String timePeriodDays, String currency)
      : this.id = id,
        this.coinId = coinId,
        this.timePeriodDays = timePeriodDays,
        this.currency = currency
  {
    if (json["prices"] == null) {
      prices.add(DatePrice(
          DateTime.now(),
          price: 0.0
      ));
    } else {
      for (int i = 0; i < (json['prices'] as List).length; i++) {
        prices.add(DatePrice(
            DateTime.fromMillisecondsSinceEpoch(json['prices'][i][0]),
            price: Util.roundToDecimals((json['prices'][i][1] ?? 0).toDouble())
        ));
      }
    }
  }

  ///Get the highest price from the prices list
  double getHighestPrice() {
    double max = 0;
    for (int i = 0; i < prices.length; i++) {
      if (prices[i].price > max) {
        max = prices[i].price;
      }
    }
    return max;
  }

  ///Get the lowest price from the prices list
  double getLowestPrice() {
    double min = prices[0].price;
    for (int i = 0; i < prices.length; i++) {
      if (prices[i].price < min) {
        min = prices[i].price;
      }
    }
    return min;
  }

  ///Get the latest price from the prices list
  double get latestPrice {
    return prices.last.price;
  }

  ///Get the oldest price from the prices list
  double get oldestPrice {
    return prices.first.price;
  }

  ///Returns all prices from the given date to now
  List<DatePrice> getPricesSinceDate(DateTime date) {
    List<DatePrice> list = [];
    DateTime currentDate = DateTime.now();
    for (int i = prices.length - 1; i >= 0; i--) {
      if (date.isBefore(currentDate)) {
        list.insert(0, prices[i]);
        if (i > 0) {
          currentDate = prices[i - 1].date;
        }
      } else {
        break;
      }
    }
    return list;
  }
}