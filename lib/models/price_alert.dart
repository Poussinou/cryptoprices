import 'package:crypto_prices/database/converters.dart';
import 'package:isar/isar.dart';

import '../constants.dart';

///Price alert class with alert information
@Collection()
class PriceAlert {
  ///The ID of this alert
  int id;

  ///The ID of the coin that this alert belongs to
  @Index() //for faster querying
  String coinId;

  ///The name of the coin that this alert belongs to
  String coinName;

  ///The symbol of the coin that this alert belongs to
  String coinSymbol;

  ///The currency that is used for API requests
  ///and that should be displayed in the notification
  String currency;

  ///The value that this alert should be triggered at
  double targetValue;

  ///The type of the target value
  @TargetValueTypeConverter()
  TargetValueType targetValueType;

  ///How should the coin price move around the target value
  ///for alert to be triggered
  @PriceMovementConverter()
  PriceMovement priceMovement;

  ///How frequent should th alert be triggered
  @AlertFrequencyConverter()
  AlertFrequency alertFrequency;

  ///the date on which the alert was added
  DateTime? dateAdded;

  ///The target value from the last API request.
  ///Used to check for movement.
  double lastTargetValue;

  PriceAlert({
    this.id = 0,
    this.coinId = "",
    this.coinName = "",
    this.coinSymbol = "",
    this.currency = "",
    this.targetValue = 0,
    this.targetValueType = TargetValueType.price,
    this.priceMovement = PriceMovement.above,
    this.alertFrequency = AlertFrequency.once,
    this.dateAdded,
    this.lastTargetValue = 0
  });

  ///Converts a PriceAlert object to json
  Map<String, dynamic> toJson() => {
    "id": id,
    "coinId": coinId,
    "coinName": coinName,
    "coinSymbol": coinSymbol,
    "currency": currency,
    "targetValue": targetValue,
    "targetValueType": TargetValueTypeConverter().toIsar(targetValueType), //Convert to position index in enum
    "priceMovement": PriceMovementConverter().toIsar(priceMovement),
    "alertFrequency": AlertFrequencyConverter().toIsar(alertFrequency),
    "dateAdded": dateAdded?.toIso8601String(),
    "lastTargetValue": lastTargetValue
  };

  ///Converts json to a PriceAlert object
  PriceAlert.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        coinId = json["coinId"],
        coinName = json["coinName"],
        coinSymbol = json["coinSymbol"],
        currency = json["currency"],
        targetValue = json["targetValue"],
        targetValueType = TargetValueTypeConverter().fromIsar(json["targetValueType"]),
        priceMovement = PriceMovementConverter().fromIsar(json["priceMovement"]),
        alertFrequency = AlertFrequencyConverter().fromIsar(json["alertFrequency"]),
        dateAdded = DateTime.parse(json["dateAdded"] ?? Constants.DATETIMEDEFAULT),
        lastTargetValue = json["lastTargetValue"];

}

///The different types that the target value can have
enum TargetValueType {
  price,
  percentage
}

///Return only the value of the enum as String without "TargetValueType."
extension TargetValueTypeToString on TargetValueType {
  String toShortString() {
    return this.toString().split('.').last;
  }
}

///How should the price move around the target value to trigger an alert
enum PriceMovement {
  above,
  equal,
  below
}

///Return only the value of the enum as String without "PriceMovement."
extension PriceMovementToString on PriceMovement {
  String toShortString() {
    return this.toString().split('.').last;
  }
}

///How many times an alert should be repeated
enum AlertFrequency {
  once,
  repeating
}

///Return only the value of the enum as String without "AlertFrequency."
extension AlertFrequencyToString on AlertFrequency {
  String toShortString() {
    return this.toString().split('.').last;
  }
}