// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TransactionAdapter extends TypeAdapter<Transaction> {
  @override
  final int typeId = 7;

  @override
  Transaction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Transaction(
      fields[0] as int,
      fields[1] as String,
      fields[17] == null ? 0 : fields[17] as int,
      coinName: fields[2] == null ? '' : fields[2] as String,
      coinSymbol: fields[3] == null ? '' : fields[3] as String,
      coinVolume: fields[4] == null ? 0 : fields[4] as double,
      currencyVolume: fields[11] == null ? 0 : fields[11] as double,
      coinPrice: fields[5] == null ? 0 : fields[5] as double,
      actualCoinPrice: fields[14] == null ? 0 : fields[14] as double,
      currency: fields[12] == null ? '' : fields[12] as String,
      transactionDate: fields[6] as DateTime?,
      transactionName: fields[7] == null ? '' : fields[7] as String,
      description: fields[8] == null ? '' : fields[8] as String,
      feeCoin: fields[15] == null ? 0 : fields[15] as double,
      feeCurrency: fields[16] == null ? 0 : fields[16] as double,
      sourceAddress: fields[9] == null ? '' : fields[9] as String,
      destinationAddress: fields[10] == null ? '' : fields[10] as String,
      transactionId: fields[13] == null ? '' : fields[13] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Transaction obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.coinId)
      ..writeByte(17)
      ..write(obj.connectedWalletId)
      ..writeByte(2)
      ..write(obj.coinName)
      ..writeByte(3)
      ..write(obj.coinSymbol)
      ..writeByte(4)
      ..write(obj.coinVolume)
      ..writeByte(11)
      ..write(obj.currencyVolume)
      ..writeByte(5)
      ..write(obj.coinPrice)
      ..writeByte(14)
      ..write(obj.actualCoinPrice)
      ..writeByte(12)
      ..write(obj.currency)
      ..writeByte(6)
      ..write(obj.transactionDate)
      ..writeByte(7)
      ..write(obj.transactionName)
      ..writeByte(8)
      ..write(obj.description)
      ..writeByte(15)
      ..write(obj.feeCoin)
      ..writeByte(16)
      ..write(obj.feeCurrency)
      ..writeByte(9)
      ..write(obj.sourceAddress)
      ..writeByte(10)
      ..write(obj.destinationAddress)
      ..writeByte(13)
      ..write(obj.transactionId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TransactionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
