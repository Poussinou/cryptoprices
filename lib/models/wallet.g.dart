// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wallet.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WalletAdapter extends TypeAdapter<Wallet> {
  @override
  final int typeId = 6;

  @override
  Wallet read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Wallet(
      fields[11] == null ? 0 : fields[11] as int,
      fields[1] as String,
      fields[10] == null ? 0 : fields[10] as int,
      coinName: fields[2] == null ? '' : fields[2] as String,
      coinSymbol: fields[3] == null ? '' : fields[3] as String,
      currentCoinPrice: fields[4] == null ? 0 : fields[4] as double,
      lastUpdated: fields[7] as DateTime?,
      transactions: (fields[8] as HiveList?)?.castHiveList(),
      listPosition: fields[9] == null ? 0 : fields[9] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Wallet obj) {
    writer
      ..writeByte(9)
      ..writeByte(11)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.coinId)
      ..writeByte(10)
      ..write(obj.connectedPortfolioId)
      ..writeByte(2)
      ..write(obj.coinName)
      ..writeByte(3)
      ..write(obj.coinSymbol)
      ..writeByte(4)
      ..write(obj.currentCoinPrice)
      ..writeByte(7)
      ..write(obj.lastUpdated)
      ..writeByte(8)
      ..write(obj.transactions)
      ..writeByte(9)
      ..write(obj.listPosition);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WalletAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
