import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:crypto_prices/database/favorites_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/util/workmanager_handler.dart';

import '../constants.dart';

class ImportExport {

  ///Imports favorites, alerts, portfolios, wallets and transactions from a json file at the provided path
  Future<void> importData(String importFilePath) async {
    File import = File(importFilePath);
    String jsonString = await import.readAsString();

    Map<String, dynamic> json = JsonDecoder().convert(jsonString);

    String appVersion = json[Constants.APPVERSIONKEY] ?? "";
    int importDatabaseVersion = json[Constants.DATABASEVERSIONKEY] ?? Constants.DATABASEVERSIONMULTIPLEPORTFOLIOS;
    DateTime exportTime = DateTime.parse(json[Constants.EXPORTTIMEKEY] ?? DateTime.now().toIso8601String());

    if (importDatabaseVersion != Constants.DATABASEVERSION) {
      _legacyImport(json);
    } else {
      _standardImport(json);
    }
  }

  ///Imports data from the current database version
  void _standardImport(Map<String, dynamic> json) {
    List favoritesJson = json[Constants.EXPORTJSONFAVORITEKEY] ?? [];
    List alertsJson = json[Constants.EXPORTJSONALERTSKEY] ?? [];
    List transactionsJson = json[Constants.EXPORTTRANSACTIONSKEY] ?? [];
    List walletsJson = json[Constants.EXPORTWALLETSKEY] ?? [];
    List portfoliosJson = json[Constants.EXPORTPORTFOLIOSKEY] ?? [];
    FavoritesDao favoritesDao = FavoritesDao();
    SettingsDAO settingsDAO = SettingsDAO();

    List<FavoriteCoin> favorites = favoritesJson.map((e) {
      return FavoriteCoin.fromJson(e);
    }).toList();
    favoritesDao.insertAllFavorites(favorites);

    WorkmanagerHandler handler = WorkmanagerHandler();

    List<PriceAlert> alerts = alertsJson.map((e) {
      final alert = PriceAlert.fromJson(e);
      handler.registerPriceAlert(alert);
      return alert;
    }).toList();
    PriceAlertDao().insertAllPriceAlerts(alerts);

    final portfolios = portfoliosJson.map((e) => Portfolio.fromJson(e)).toList();
    PortfolioDao portfolioDao = PortfolioDao();
    int defaultPortfolioId = settingsDAO.getDefaultPortfolioIdHive();
    portfolios.forEach((portfolio) {
      if (portfolioDao.getPortfolio(portfolio.id) != null) {
        portfolioDao.deletePortfolio(portfolio.id); //overwrite all existing portfolios
      }
    });
    if (portfolios.where((portfolio) => portfolio.id == defaultPortfolioId).isNotEmpty) { //keep the old default portfolio
      settingsDAO.setDefaultPortfolioId(defaultPortfolioId);
    }
    portfolioDao.insertMultiplePortfolios(portfolios);

    final addToFavorites = settingsDAO.getAddWalletToFavoritesHive();
    if (addToFavorites) { //don't add all coins to favorites
      settingsDAO.setAddWalletToFavorites(false);
    }

    List<Wallet> wallets = walletsJson.map((e) => Wallet.fromJson(e)).toList();
    WalletDao walletDAO = WalletDao();
    wallets.forEach((wallet) {
      walletDAO.insertWallet(wallet);
    });

    final transactions = transactionsJson.map((e) => Transaction.fromJson(e)).toList();
    transactions.forEach((transaction) {
      final connectedWallet = walletDAO.getWallet(transaction.connectedWalletId);
      if (connectedWallet != null) {
        int portfolioId = connectedWallet.connectedPortfolioId;
        portfolioDao.getPortfolio(portfolioId)!.insertTransaction(transaction);
      }
    });

    settingsDAO.setAddWalletToFavorites(addToFavorites);
  }

  ///Imports data from a previous database version
  void _legacyImport(Map<String, dynamic> json) {
    int importDatabaseVersion = json[Constants.DATABASEVERSIONKEY] ?? Constants.DATABASEVERSIONMULTIPLEPORTFOLIOS;

    //Create new portfolio for old data
    if (importDatabaseVersion == Constants.DATABASEVERSIONMULTIPLEPORTFOLIOS) {
      List favoritesJson = json[Constants.EXPORTJSONFAVORITEKEY] ?? [];
      List alertsJson = json[Constants.EXPORTJSONALERTSKEY] ?? [];
      List transactionsJson = json[Constants.EXPORTTRANSACTIONSKEY] ?? [];
      List walletsJson = json[Constants.OLDEXPORTWALLETSKEY] ?? [];

      FavoritesDao favoritesDao = FavoritesDao();
      SettingsDAO settingsDAO = SettingsDAO();

      List<FavoriteCoin> favorites = favoritesJson.map((e) {
        return FavoriteCoin.fromJson(e);
      }).toList();
      favoritesDao.insertAllFavorites(favorites);

      WorkmanagerHandler handler = WorkmanagerHandler();

      List<PriceAlert> alerts = alertsJson.map((e) {
        final alert = PriceAlert.fromJson(e);
        handler.registerPriceAlert(alert);
        return alert;
      }).toList();
      PriceAlertDao().insertAllPriceAlerts(alerts);

      WalletDao walletDao = WalletDao();
      PortfolioDao portfolioDao = PortfolioDao();

      final wallets = walletsJson.map((e) => Wallet.fromJson(e)).toList();
      final transactions = transactionsJson.map((e) => Transaction.fromJson(e)).toList();

      final newPortfolio = Portfolio(
        Util.generateId(),
        S.current.settingsImportExportImportPortfolioName,
        currency: SettingsDAO().getCurrencyHive()
      );

      portfolioDao.insertPortfolio(newPortfolio);

      final addToFavorites = settingsDAO.getAddWalletToFavoritesHive();
      if (addToFavorites) { //don't add all coins to favorites
        settingsDAO.setAddWalletToFavorites(false);
      }

      wallets.forEach((wallet) {
        wallet.id = Util.generateId(); //new id so existing wallet is not overwritten
        wallet.connectedPortfolioId = newPortfolio.id;
      });
      walletDao.insertMultipleWallets(wallets);

      transactions.forEach((transaction) {
        transaction.id = Util.generateId(); //new id so existing transaction is not overwritten
        newPortfolio.insertTransaction(transaction);
      });

      settingsDAO.setAddWalletToFavorites(addToFavorites);
    }
  }

  ///True if importing this file will overwrite data
  Future<bool> willOverwriteData(String importFilePath) async {
    File import = File(importFilePath);
    String jsonString = await import.readAsString();

    Map<String, dynamic> json = JsonDecoder().convert(jsonString);

    int importDatabaseVersion = json[Constants.DATABASEVERSIONKEY] ?? Constants.DATABASEVERSIONMULTIPLEPORTFOLIOS;

    List alertsJson = json[Constants.EXPORTJSONALERTSKEY] ?? [];
    List transactionsJson = json[Constants.EXPORTTRANSACTIONSKEY] ?? [];
    List portfoliosJson = [];
    List walletsJson = [];
    if (importDatabaseVersion != Constants.DATABASEVERSION) {
      walletsJson = json[Constants.OLDEXPORTWALLETSKEY] ?? [];
    } else {
      walletsJson = json[Constants.EXPORTWALLETSKEY] ?? [];
      portfoliosJson = json[Constants.EXPORTPORTFOLIOSKEY] ?? [];
    }

    for (int i = 0; i < alertsJson.length; i++) {
      final alert = PriceAlert.fromJson(alertsJson[i]);
      if (PriceAlertDao().getPriceAlert(alert.id) != null) {
        return true;
      }
    }

    for (int i = 0; i < portfoliosJson.length; i++) {
      final portfolio = Portfolio.fromJson(portfoliosJson[i]);
      if (PortfolioDao().getPortfolio(portfolio.id) != null) {
        return true;
      }
    }

    for (int i = 0; i < walletsJson.length; i++) {
      final wallet = Wallet.fromJson(walletsJson[i]);
      if (WalletDao().getWallet(wallet.id) != null) {
        return true;
      }
    }

    for (int i = 0; i < transactionsJson.length; i++) {
      final transaction = Transaction.fromJson(transactionsJson[i]);
      if (TransactionDao().getTransaction(transaction.id) != null) {
        return true;
      }
    }
    return false;
  }

  ///Exports favorites, alerts, portfolios, wallets and transactions to a json file
  ///at the provided path
  Future<void> exportData(String exportPath) async {
    List<FavoriteCoin> favorites = FavoritesDao().getAllFavorites();
    final favsJson = favorites.map((e) => e.toJson()).toList();

    List<PriceAlert> priceAlerts = PriceAlertDao().getAllPriceAlerts();
    final alertsJson = priceAlerts.map((e) => e.toJson()).toList();

    List<Transaction> transactions = TransactionDao().getAllTransactions();
    final transactionsJson = transactions.map((e) => e.toJson()).toList();

    List<Wallet> wallets = WalletDao().getAllWallets();
    final walletsJson = wallets.map((e) => e.toJson()).toList();

    List<Portfolio> portfolios = PortfolioDao().getAllPortfolios();
    final portfoliosJson = portfolios.map((e) => e.toJson()).toList();

    final appVersion = SettingsDAO().getCurrentAppVersionHive();
    final databaseVersion = Constants.DATABASEVERSION;

    Map<String, dynamic> exportData = {
      Constants.APPVERSIONKEY: appVersion,
      Constants.DATABASEVERSIONKEY: databaseVersion,
      Constants.EXPORTTIMEKEY: DateTime.now().toIso8601String(),
      Constants.EXPORTJSONFAVORITEKEY: favsJson,
      Constants.EXPORTJSONALERTSKEY: alertsJson,
      Constants.EXPORTTRANSACTIONSKEY: transactionsJson,
      Constants.EXPORTWALLETSKEY: walletsJson,
      Constants.EXPORTPORTFOLIOSKEY: portfoliosJson
    };

    String json = JsonEncoder().convert(exportData);

    File exportFile = File(exportPath);
    await exportFile.writeAsString(json);
  }
}