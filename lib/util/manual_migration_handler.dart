import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/explorer_transaction_url_dao.dart';
import 'package:crypto_prices/database/favorites_dao.dart';
import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/coin_id_migration_data.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/models/wallet.dart';

///Provides all user data that has to be manually migrated and handles the migration
class ManualMigrationHandler {
  List<AvailableCoin> _supportedCoins = [];
  List<FavoriteCoin> _favorites = [];
  List<ExplorerTransactionUrl> _explorerTransactionUrls = [];
  List<PriceAlert> _priceAlerts = [];
  List<PriceAlertListEntry> _priceAlertListEntries = [];
  List<Wallet> _wallets = [];
  List<Transaction> _transactions = [];

  AvailableCoinsDao _availableCoinsDao = AvailableCoinsDao();
  FavoritesDao _favoritesDao = FavoritesDao();
  ExplorerTransactionUrlDao _explorerTransactionUrlDao = ExplorerTransactionUrlDao();
  PriceAlertDao _priceAlertDao = PriceAlertDao();
  PriceAlertListEntryDao _priceAlertListEntryDao = PriceAlertListEntryDao();
  WalletDao _walletDao = WalletDao();
  TransactionDao _transactionDao = TransactionDao();

  ManualMigrationHandler() {
    _supportedCoins = _availableCoinsDao.getAllAvailableCoins();
    _favorites = _favoritesDao.getAllFavorites();
    _explorerTransactionUrls = _explorerTransactionUrlDao.getAllCoinExplorerTransactionUrls();
    _priceAlerts = _priceAlertDao.getAllPriceAlerts();
    _priceAlertListEntries = _priceAlertListEntryDao.getAllEntries();
    _wallets = _walletDao.getAllWallets();
    _transactions = _transactionDao.getAllTransactions();
  }

  ///Returns all coin ids that are supported by the API
  List<String> getSupportedCoinIds() {
    return _supportedCoins.map((e) => e.id).toList();
  }

  ///Returns all coin ids that are used by the saved user data (i.e. favorites, wallets, etc.)
  List<String> getAllUsedCoinIds() {
    List<String> coinIds = [];

    _favorites.forEach((element) {
      if (!coinIds.contains(element.id))
        coinIds.add(element.id);
    });
    _explorerTransactionUrls.forEach((element) {
      if (!coinIds.contains(element.coinId))
        coinIds.add(element.coinId);
    });
    _priceAlerts.forEach((element) {
      if (!coinIds.contains(element.coinId))
        coinIds.add(element.coinId);
    });
    _priceAlertListEntries.forEach((element) {
      if (!coinIds.contains(element.coinId))
        coinIds.add(element.coinId);
    });
    _wallets.forEach((element) {
      if (!coinIds.contains(element.coinId))
        coinIds.add(element.coinId);
    });
    _transactions.forEach((element) {
      if (!coinIds.contains(element.coinId))
        coinIds.add(element.coinId);
    });
    return coinIds;
  }

  ///True if manual data migration is necessary
  bool isManualMigrationNeeded() {
    final supportedCoinIds = getSupportedCoinIds();
    final idsToMigrate = getAllUsedCoinIds().where((coinId) => !supportedCoinIds.contains(coinId));
    return idsToMigrate.length != 0;
  }

  ///Returns data with the given coin id that has to be migrated
  CoinIdMigrationData getCoinIdDataToMigrate(String coinId) {
    if (getSupportedCoinIds().contains(coinId)) {
      return CoinIdMigrationData(coinId);
    } else {
      final coin = CoinIdMigrationData(coinId);
      coin.favorites = _favorites.where((element) => element.id == coinId).toList();
      coin.explorerTransactionUrls = _explorerTransactionUrls.where((element) => element.coinId == coinId).toList();
      coin.priceAlerts = _priceAlerts.where((element) => element.coinId == coinId).toList();
      coin.priceAlertListEntries = _priceAlertListEntries.where((element) => element.coinId == coinId).toList();
      coin.wallets = _wallets.where((element) => element.coinId == coinId).toList();
      coin.transactions = _transactions.where((element) => element.coinId == coinId).toList();
      _setCoinNameAndSymbol(coin);
      return coin;
    }
  }


  ///Returns all data that uses unsupported coin ids
  List<CoinIdMigrationData> getAllDataToMigrate() {
    final supportedCoinIds = getSupportedCoinIds();
    final idsToMigrate = getAllUsedCoinIds().where((coinId) => !supportedCoinIds.contains(coinId));
    List<CoinIdMigrationData> coinData = [];

    idsToMigrate.forEach((coinId) {
      final coin = CoinIdMigrationData(coinId);
      coin.favorites = _favorites.where((element) => element.id == coinId).toList();
      coin.explorerTransactionUrls = _explorerTransactionUrls.where((element) => element.coinId == coinId).toList();
      coin.priceAlerts = _priceAlerts.where((element) => element.coinId == coinId).toList();
      coin.priceAlertListEntries = _priceAlertListEntries.where((element) => element.coinId == coinId).toList();
      coin.wallets = _wallets.where((element) => element.coinId == coinId).toList();
      coin.transactions = _transactions.where((element) => element.coinId == coinId).toList();
      _setCoinNameAndSymbol(coin);
      coinData.add(coin);
    });
    return coinData;
  }

  ///Get the name and symbol of the coin from its data if possible
  CoinIdMigrationData _setCoinNameAndSymbol(CoinIdMigrationData coin) {
    if (coin.favorites.isNotEmpty) {
      coin.oldCoinName = coin.favorites.first.name;
      coin.oldCoinSymbol = coin.favorites.first.symbol;
      return coin;
    }
    if (coin.priceAlerts.isNotEmpty) {
      coin.oldCoinName = coin.priceAlerts.first.coinName;
      coin.oldCoinSymbol = coin.priceAlerts.first.coinSymbol;
      return coin;
    }
    if (coin.priceAlertListEntries.isNotEmpty) {
      coin.oldCoinName = coin.priceAlertListEntries.first.coinName;
      coin.oldCoinSymbol = coin.priceAlertListEntries.first.coinSymbol;
      return coin;
    }
    if (coin.wallets.isNotEmpty) {
      coin.oldCoinName = coin.wallets.first.coinName;
      coin.oldCoinSymbol = coin.wallets.first.coinSymbol;
      return coin;
    }
    if (coin.transactions.isNotEmpty) {
      coin.oldCoinName = coin.transactions.first.coinName;
      coin.oldCoinSymbol = coin.transactions.first.coinSymbol;
      return coin;
    }
    return coin;
  }

  ///Migrates data from the old, unsupported coin ids to a new coin id
  void migrateCoinId(String oldCoinId, String newCoinId) {
    final data = getCoinIdDataToMigrate(oldCoinId);
    final newCoin = _supportedCoins.where((element) => element.id == newCoinId).first;
    data.favorites.forEach((element) {
      element.id = newCoinId;
      element.name = newCoin.name;
      element.symbol = newCoin.symbol;
      _favoritesDao.deleteFavorite(oldCoinId);
      _favoritesDao.insertFavorite(element);
    });
    data.explorerTransactionUrls.forEach((element) {
      element.coinId = newCoinId;
      _explorerTransactionUrlDao.deleteCoinExplorerTransactionUrl(oldCoinId);
      _explorerTransactionUrlDao.insertCoinExplorerTransactionUrl(element);
    });
    data.priceAlerts.forEach((element) {
      element.coinId = newCoinId;
      element.coinName = newCoin.name;
      element.coinSymbol = newCoin.symbol;
      _priceAlertDao.insertPriceAlert(element);
    });
    data.priceAlertListEntries.forEach((element) {
      element.coinId = newCoinId;
      element.coinName = newCoin.name;
      element.coinSymbol = newCoin.symbol;
      _priceAlertListEntryDao.insertEntry(element);
    });
    data.wallets.forEach((element) {
      element.coinId = newCoinId;
      element.coinName = newCoin.name;
      element.coinSymbol = newCoin.symbol;
      element.save();
    });
    data.transactions.forEach((element) {
      element.coinId = newCoinId;
      element.coinName = newCoin.name;
      element.coinSymbol = newCoin.symbol;
      element.save();
    });
  }

  ///Deletes all data that uses the given coin id
  void deleteCoinIdData(String coinId) {
    final data = getCoinIdDataToMigrate(coinId);
    data.favorites.forEach((element) {
      _favoritesDao.deleteFavorite(coinId);
    });
    data.explorerTransactionUrls.forEach((element) {
      _explorerTransactionUrlDao.deleteCoinExplorerTransactionUrl(coinId);
    });
    data.priceAlerts.forEach((element) {
      _priceAlertDao.deletePriceAlert(element.id);
    });
    data.priceAlertListEntries.forEach((element) {
      _priceAlertListEntryDao.deleteEntry(coinId);
    });
    data.wallets.forEach((element) {
      _walletDao.deleteWallet(element.id);
    });
    data.transactions.forEach((element) {
      _transactionDao.deleteTransaction(element.id);
    });
  }
}
