///Exception that is thrown when being rate limited by an API
class RateLimitationException implements Exception {
  ///Description of the error
  final String message;

  RateLimitationException(this.message);
}