import 'dart:io';

import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/large_move_alert_dao.dart';
import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/isar.g.dart';
import 'package:crypto_prices/main.dart';
import 'package:crypto_prices/models/large_move_alert.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences_android/shared_preferences_android.dart';
import 'package:workmanager/workmanager.dart';

import 'notification_handler.dart';

///callback function for the Workmanager
void callbackDispatcher() {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferencesAndroid.registerWith(); //fix for https://github.com/flutter/flutter/issues/98473#issuecomment-1060952450
  Workmanager().executeTask((task, inputData) async {
    switch (task) {
      case Constants.PRICEALERTTASKNAME: {
        int alertId = inputData![Constants.PRICEALERTDATAKEYID];

        isar = await openIsar();
        PriceAlertDao _priceAlertDao = PriceAlertDao();

        PriceAlert alert = _priceAlertDao.getPriceAlert(alertId)!;
        double lastTargetValue = alert.lastTargetValue;

        ApiInteraction api = CoinGeckoAPI();

        WorkmanagerHandler workmanagerHandler = WorkmanagerHandler();
        NotificationHandler notificationHandler = NotificationHandler();
        notificationHandler.initialize(onSelectNotification);

        Map<String, double> priceResponse;
        double currentTargetValue = 0;
        if (alert.targetValueType == TargetValueType.percentage) {
          try {
            priceResponse = await api.getCoinPrice(alert.coinId, alert.currency, changePercentage: true);
            currentTargetValue = priceResponse[Constants.APIGETCOINCHANGEPERCENTAGEKEY]!;
            var rounded = currentTargetValue.toStringAsFixed(2);
            currentTargetValue = double.parse(rounded);
          }
          on SocketException {
            print("SocketException");
            return Future.value(false);

          }
          on HttpException {
            print("HTTPException");
            return Future.value(false);
          }
          on RateLimitationException {
            print("RateLimitationException");
            return Future.value(false);
          }
        } else {
          try {
            priceResponse = await api.getCoinPrice(alert.coinId, alert.currency);
            currentTargetValue = priceResponse[Constants.APIGETCOINPRICEKEY]!;
          }
          on SocketException {
            print("SocketException");
            return Future.value(false);
          }
          on HttpException {
            print("HTTPException");
            return Future.value(false);
          }
          on RateLimitationException {
            print("RateLimitationException");
            return Future.value(false);
          }
        }

        switch (alert.alertFrequency) {
          case AlertFrequency.once: { //delete alert after notification

            switch (alert.priceMovement) {
              case PriceMovement.above: {
                //if value has moved above target from last time it was checked
                if (currentTargetValue > alert.targetValue && alert.targetValue >= lastTargetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);

                  workmanagerHandler.deleteAlert(alertId);
                } else {
                  alert.lastTargetValue = currentTargetValue;
                  _priceAlertDao.insertPriceAlert(alert);
                }
                break;
              }

              case PriceMovement.equal: {
                if (currentTargetValue == alert.targetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);

                  workmanagerHandler.deleteAlert(alertId);
                }
                break;
              }

              case PriceMovement.below: {
                //if value has moved below target from last time it was checked
                if (currentTargetValue < alert.targetValue && alert.targetValue <= lastTargetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);

                  workmanagerHandler.deleteAlert(alertId);
                } else {
                  alert.lastTargetValue = currentTargetValue;
                  _priceAlertDao.insertPriceAlert(alert);
                }
                break;
              }
            }
            break;
          }

          case AlertFrequency.repeating: {

            switch (alert.priceMovement) {
              case PriceMovement.above: {
                //if value has moved above target from last time it was checked
                if (currentTargetValue > alert.targetValue && alert.targetValue >= lastTargetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);
                }
                //update lastTargetValue in every repetition
                alert.lastTargetValue = currentTargetValue;
                _priceAlertDao.insertPriceAlert(alert);

                break;
              }

              case PriceMovement.equal: {
                //always send alert when price is equal
                if (currentTargetValue == alert.targetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);
                }
                break;
              }

              case PriceMovement.below: {
                //if value has moved below target from last time it was checked
                if (currentTargetValue < alert.targetValue && alert.targetValue <= lastTargetValue) {
                  await notificationHandler.showPriceAlertNotification(alert, pricePercentage: priceResponse);
                }
                //update lastTargetValue in every repetition
                alert.lastTargetValue = currentTargetValue;
                _priceAlertDao.insertPriceAlert(alert);

                break;
              }
            }
            break;
          }
        }
        break;
      }

      case Constants.LARGEMOVEALERTTASKNAME : {
        SettingsDAO settingsDAO = SettingsDAO();
        bool notificationsEnabled = await settingsDAO.getLargeMoveAlertsEnabledPrefs();

        if (!notificationsEnabled)
          return Future.value(true);

        int alertId = inputData![Constants.LARGEMOVEALERTDATAKEYID];

        isar = await openIsar();
        LargeMoveAlertDAO largeMoveAlertDao = LargeMoveAlertDAO();

        LargeMoveAlert alert = largeMoveAlertDao.getLargeMoveAlert(alertId)!;

        ApiInteraction api = CoinGeckoAPI();

        NotificationHandler notificationHandler = NotificationHandler();
        notificationHandler.initialize(onSelectNotification);

        Map<String, double> priceResponse;
        double currentPercentage = 0;
        double currentPrice = 0;

        String currency = await settingsDAO.getCurrencyPrefs();
        alert.currency = currency;

        try {
          priceResponse = await api.getCoinPrice(alert.coinId, alert.currency, changePercentage: true);
          currentPercentage = priceResponse[Constants.APIGETCOINCHANGEPERCENTAGEKEY]!;
          currentPrice = priceResponse[Constants.APIGETCOINPRICEKEY]!;
        }
        on SocketException {
          print("SocketException");
          return Future.value(false);

        }
        on HttpException {
          print("HTTPException");
          return Future.value(false);
        }
        on RateLimitationException {
          print("RateLimitationException");
          return Future.value(false);
        }

        double largeMoveAlertThreshold = await settingsDAO.getLargeMoveAlertThresholdPrefs();

        if (currentPercentage.abs() >= largeMoveAlertThreshold
            && (alert.lastNotificationSent == null
                || DateTime.now().difference(alert.lastNotificationSent!).inHours >= 12)
        ) {
          await notificationHandler.showLargeMoveAlertNotification(alert, currentPercentage, currentPrice);
          alert.lastNotificationSent = DateTime.now();
          largeMoveAlertDao.insertLargeMoveAlert(alert);
        }

        break;
      }
    }

    return Future.value(true);
  });
}

///Provides functions to use the Workmanager plugin to schedule tasks
class WorkmanagerHandler {

  ///Initialized the Workmanager plugin
  void initializeWorkmanager() {
    Workmanager().initialize(
        callbackDispatcher
    );
  }

  ///Registers a PriceAlert task that checks the price every 15 minutes (default (minimum) period)
  void registerPriceAlert(PriceAlert alert) {
    PriceAlertDao().insertPriceAlert(alert);
    Workmanager().registerPeriodicTask(
        alert.id.toString(),
        Constants.PRICEALERTTASKNAME,
        constraints: Constraints(networkType: NetworkType.connected),
        existingWorkPolicy: ExistingWorkPolicy.replace,
        inputData: {
          Constants.PRICEALERTDATAKEYID: alert.id
        }
    );
  }

  ///Registers a LargeMoveAlert task that checks the 24H price change percentage every 15 minutes
  void registerLargeMoveAlert(LargeMoveAlert alert) {
    LargeMoveAlertDAO().insertLargeMoveAlert(alert);
    Workmanager().registerPeriodicTask(
        alert.id.toString(),
        Constants.LARGEMOVEALERTTASKNAME,
        constraints: Constraints(networkType: NetworkType.connected),
        existingWorkPolicy: ExistingWorkPolicy.replace,
        inputData: {
          Constants.LARGEMOVEALERTDATAKEYID: alert.id
        }
    );
  }

  ///Deletes a PriceAlert and cancels its workmanager task
  void deleteAlert(int alertId) {
    PriceAlertDao().deletePriceAlert(alertId);
    Workmanager().cancelByUniqueName(alertId.toString());
  }

  ///Deletes all PriceAlerts of a specific coin and cancels their workmanager tasks
  void deleteAllCoinAlerts(String coinId) {
    PriceAlertDao priceAlertDao = PriceAlertDao();

    List<int> ids = priceAlertDao.getAllCoinPriceAlertIds(coinId);
    for (int i = 0; i < ids.length; i++) {
      Workmanager().cancelByUniqueName(ids[i].toString());
    }

    priceAlertDao.deleteAllCoinPriceAlerts(coinId);
  }

  ///Deletes a LargeMoveAlert and cancels its workmanager task
  void deleteLargeMoveAlertAlertByCoinId(String coinId) {
    LargeMoveAlertDAO largeMoveAlertDAO = LargeMoveAlertDAO();

    int alertId = largeMoveAlertDAO.getLargeMoveAlertByCoin(coinId)!.id;
    Workmanager().cancelByUniqueName(alertId.toString());
    largeMoveAlertDAO.deleteLargeMoveAlert(alertId);
  }

  ///Restarts all saved alerts
  void restartAllAlerts() {
    Workmanager workmanager = Workmanager();
    workmanager.cancelAll();

    final priceAlerts = PriceAlertDao().getAllPriceAlerts();
    priceAlerts.forEach((alert) {
      workmanager.registerPeriodicTask(
        alert.id.toString(),
        Constants.PRICEALERTTASKNAME,
        constraints: Constraints(networkType: NetworkType.connected),
        existingWorkPolicy: ExistingWorkPolicy.replace,
        inputData: {
          Constants.PRICEALERTDATAKEYID: alert.id
        }
      );
    });

    final largeMoveAlerts = LargeMoveAlertDAO().getAllLargeMoveAlerts();
    largeMoveAlerts.forEach((alert) {
      workmanager.registerPeriodicTask(
        alert.id.toString(),
        Constants.LARGEMOVEALERTTASKNAME,
        constraints: Constraints(networkType: NetworkType.connected),
        existingWorkPolicy: ExistingWorkPolicy.replace,
        inputData: {
          Constants.LARGEMOVEALERTDATAKEYID: alert.id
        }
      );
    });
  }
}