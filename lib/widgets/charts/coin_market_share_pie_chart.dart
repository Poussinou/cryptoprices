import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/coins_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';

///Pie chart tht show coin market share
class CoinMarketSharePieChart extends StatefulWidget {
  final GlobalMarketInfo globalMarketInfo;

  CoinMarketSharePieChart({
    Key? key,
    required this.globalMarketInfo
  }) : super(key: key);

  @override
  _CoinMarketSharePieChartState createState() => _CoinMarketSharePieChartState();
}

class _CoinMarketSharePieChartState extends State<CoinMarketSharePieChart> {
  final _boldText = TextStyle(fontWeight: FontWeight.bold);
  final _biggerFont = TextStyle(fontSize: 18);

  ///Map with coin symbol keys and percentage values
  Map<String, double> _coinPercentages = {};

  ///Map with the color of the portfolios. Keys are the coin ids
  Map<String, Color> _graphColors = {};

  late Future<Map<String, Color>> _getGraphColors;

  ///Top 20 coins sorted by marketcap
  List<Coin> _top20 = [];

  @override
  void initState() {
    super.initState();

    final sortedCoins = CoinsDao().getAllCoins()..sort(CoinComparators.marketCapDesc);
    _top20 = sortedCoins.take(20).toList();

    _coinPercentages = widget.globalMarketInfo.marketCapCoinPercentages;
    _getGraphColors = _coinColors(_coinPercentages);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map<String, Color>>(
      future: _getGraphColors,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          _graphColors = snapshot.data!;
          return _buildChartWithColors(_graphColors);
        } else if (snapshot.hasError) {
          return _buildChartWithColors({});
        }
        return AspectRatio(
          aspectRatio: 16/13,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: CircularProgressIndicator(),
              ),
            ],
          )
        );
      });
  }

  ///Builds the pie chart with the given colors for the coin sections
  Widget _buildChartWithColors(Map<String, Color> colors) {
    return Column(
      children: [
        AspectRatio(
          aspectRatio: 16/13,
          child: PieChart(
            PieChartData(
              borderData: FlBorderData(
                show: false
              ),
              centerSpaceRadius: MediaQuery.of(context).size.width / 4,
              sections: _showingSections(_coinPercentages, colors),
              sectionsSpace: 10,
              startDegreeOffset: 270
            )
          )
        ),
        Container(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _coinPercentages.length + 1,
            itemBuilder: (context, i) {
              final keys = _coinPercentages.keys.toList();
              final values = _coinPercentages.values.toList();
              if (i < _coinPercentages.length) {
                final coin = _top20.where((element) => element.symbol == keys[i]).first;
                return Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(Icons.circle, color: colors[keys[i]] ?? Colors.grey),
                    SizedBox(width: 10),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "${coin.name} (${keys[i].toUpperCase()})",
                            style: _biggerFont,
                          ),
                          SizedBox(width: 10),
                          Text(
                            "${Util.formatPercentage(values[i])}%",
                            style: _biggerFont,
                          )
                        ],
                      ),
                    )
                  ],
                );
              } else { //other
                return Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(Icons.circle, color: Colors.grey,),
                    SizedBox(width: 10),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            S.of(context).portfolioPieChartOther,
                            style: _biggerFont,
                          ),
                          SizedBox(width: 10),
                          Text(
                            "${Util.formatPercentage(_getOtherPercentage(_coinPercentages))}%",
                            style: _biggerFont,
                          )
                        ],
                      ),
                    )
                  ],
                );
              }
            },
          ),
        )
      ],
    );
  }

  ///Returns the list of PieChartSectionData of all sections in the pie chart
  List<PieChartSectionData> _showingSections(Map<String, double> coinPercentages, Map<String, Color> colors) {
    List<PieChartSectionData> data = [];
    double displayedPercentage = 0;
    final fontSize = 16.0;
    final radius = 35.0;

    coinPercentages.forEach((key, value) {
      if (value >= 2.0) {
        data.add(PieChartSectionData(
          color: colors[key] ?? Colors.grey,
          value: value,
          title: key.toUpperCase(),
          radius: radius,
          titleStyle: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: (currentTheme.isThemeDark()) ? Colors.white : Colors.black
          )
        ));
        displayedPercentage += value;
      }
    });
    data.add(PieChartSectionData(
      color: Colors.grey,
      value: 100 - displayedPercentage,
      title: S.of(context).portfolioPieChartOther,
      radius: radius,
      titleStyle: TextStyle(
        fontSize: fontSize,
        fontWeight: FontWeight.bold,
        color: (currentTheme.isThemeDark()) ? Colors.white : Colors.black
      )
    ));

    return data;
  }

  ///Gets the total value of all portfolios that are too small to be displayed (< 1% share)
  double _getOtherPercentage(Map<String, double> coinPercentages) {
    double totalValue = 0;
    coinPercentages.forEach((key, value) {
      totalValue += value;
    });
    return 100 - totalValue;
  }

  ///Returns a map with the portfolio colors. Colors are the dominant color of each coin icon
  Future<Map<String, Color>> _coinColors(Map<String, double> coinPercentages) async {
    Map<String, Color> colors = {};
    final keys = coinPercentages.keys.toList();
    for (int i = 0; i < coinPercentages.length; i++) {
      final coin = _top20.where((element) => element.symbol == keys[i]).first;
      final image = NetworkImage(ImageUrls().getLargeImageUrl(coin.id));
      final paletteGenerator = await PaletteGenerator.fromImageProvider(image);
      colors[coinPercentages.keys.toList()[i]] = paletteGenerator.dominantColor?.color ?? Colors.grey;
    }
    return colors;
  }
}