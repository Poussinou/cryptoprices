import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';

class PortfolioPieChart extends StatefulWidget {
  final List<Wallet> wallets;

  final double totalValue;

  PortfolioPieChart({
    Key? key,
    required this.wallets,
    required this.totalValue
  }) : super(key: key);

  @override
  _PortfolioPieChartState createState() => _PortfolioPieChartState();
}

class _PortfolioPieChartState extends State<PortfolioPieChart> {
  final _boldText = TextStyle(fontWeight: FontWeight.bold);
  final _normalFont = TextStyle(fontSize: 16);

  int _isSelectedIndex = -1;

  ///Map with the color of the wallets. Keys are the coin ids
  Map<String, Color> _graphColors = {};

  List<Wallet> _wallets = [];

  late Future<Map<String, Color>> _getGraphColors;

  @override
  void initState() {
    super.initState();

    _wallets = List.of(widget.wallets); //copy list to avoid sorting the widget list through reference
    _getGraphColors = _walletColors(_wallets);
  }

  @override
  Widget build(BuildContext context) {
    //portfolio has changed
    if (!_walletsEqual(_wallets, widget.wallets)) {
      _wallets = List.of(widget.wallets);
      _getGraphColors = _walletColors(_wallets);
    }

    return FutureBuilder<Map<String, Color>>(
      future: _getGraphColors,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return AspectRatio(
            aspectRatio: 16/10,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: CircularProgressIndicator(),
                ),
              ],
            )
          );
        }
        if (snapshot.hasData) {
          _graphColors = snapshot.data!;
          return _buildChartWithColors(_graphColors);
        } else if (snapshot.hasError) {
          return _buildChartWithColors({});
        }
        return AspectRatio(
          aspectRatio: 16/10,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: CircularProgressIndicator(),
              ),
            ],
          )
        );
      });
  }

  ///Builds the pie chart with the given colors for the wallet sections
  Widget _buildChartWithColors(Map<String, Color> colors) {
    return AspectRatio(
      aspectRatio: 16/10,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Container(
              child: PieChart(
                PieChartData(
                  borderData: FlBorderData(
                    show: false
                  ),
                  sectionsSpace: 10,
                  centerSpaceRadius: MediaQuery.of(context).size.width / 7,
                  startDegreeOffset: 270,
                  sections: _showingSections(_wallets, colors)
                )
              )
            ),
          ),
          SizedBox(width: 10),
          Flexible(
            child: ListView.builder(
              shrinkWrap: true,
              physics: (_getDisplayedWallets(_wallets).length > 5) ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),
              itemCount: _getDisplayedWallets(_wallets).length + 1,
              itemBuilder: (context, i) {
                if (i < _getDisplayedWallets(_wallets).length) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.circle, color: colors[_wallets[i].coinId] ?? Colors.grey),
                      SizedBox(width: 10),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "${_getDisplayedWallets(_wallets)[i].coinSymbol.toUpperCase()} ",
                              style: _normalFont,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "${Util.formatPercentage(_shareOfTotalValue(_getDisplayedWallets(_wallets)[i]))}%",
                              style: _normalFont,
                            )
                          ],
                        ),
                      )
                    ],
                  );
                } else { //other
                  if (_getOtherTotalValue(_wallets) != 0) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.circle, color: Colors.grey,),
                        SizedBox(width: 10),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                S.of(context).portfolioPieChartOther,
                                style: _normalFont,
                              ),
                              SizedBox(width: 10),
                              Text(
                                "${Util.formatPercentage(((_getOtherTotalValue(_wallets) / widget.totalValue) * 100))}%",
                                style: _normalFont,
                              )
                            ],
                          ),
                        )
                      ],
                    );
                  } else {
                    return Row();
                  }
                }
              }
            )
          )
        ],
      ),
    );
  }

  ///Returns the list of PieChartSectionData of all sections in the pie chart
  List<PieChartSectionData> _showingSections(List<Wallet> wallets, Map<String, Color> colors) {
    wallets.sort(CoinComparators.walletTotalValueDesc);
    List<PieChartSectionData> data = [];
    double otherTotalValue = 0;
    final fontSize = 16.0;
    final radius = 15.0;

    for (int i = 0; i < wallets.length; i++) {
      if ((wallets[i].totalValue / widget.totalValue) * 100 >= 1) { //wallet share is at least 1% of total
        data.add(PieChartSectionData(
            color: colors[wallets[i].coinId] ?? Colors.grey,
            value: (wallets[i].totalValue / widget.totalValue) * 100,
            title: "",
            radius: radius,
            titleStyle: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold)
        ));
      } else {
        otherTotalValue += wallets[i].totalValue;
      }
    }
    if ((otherTotalValue / widget.totalValue) * 100 >= 1) { //"other"-share is at least 1% of portfolio
      data.add(PieChartSectionData(
          color: Colors.grey,
          value: (otherTotalValue / widget.totalValue) * 100,
          title: "",
          radius: radius,
          titleStyle: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold)
      ));
    }

    return data;
  }

  ///Gets the value share of the given wallet of the total value
  double _shareOfTotalValue(Wallet wallet) {
    return (wallet.totalValue / widget.totalValue) * 100;
  }

  ///Gets the wallets that are big enough to be displayed (>= 1% share)
  List<Wallet> _getDisplayedWallets(List<Wallet> wallets) {
    List<Wallet> displayed = [];
    wallets.forEach((wallet) {
      if ((wallet.totalValue / widget.totalValue) * 100 >= 1) {
        displayed.add(wallet);
      }
    });
    return displayed;
  }

  ///Gets the total value of all wallets that are too small to be displayed (< 1% share)
  double _getOtherTotalValue(List<Wallet> wallets) {
    double totalValue = 0;
    wallets.forEach((wallet) {
      if ((wallet.totalValue / widget.totalValue) * 100 < 1) {
        totalValue += wallet.totalValue;
      }
    });
    return totalValue;
  }

  ///Returns a map with the wallet colors. Colors are the dominant color of each coin icon
  Future<Map<String, Color>> _walletColors(List<Wallet> wallets) async {
    Map<String, Color> colors = {};
    for (int i = 0; i < wallets.length; i++) {
      final image = NetworkImage(ImageUrls().getLargeImageUrl(wallets[i].coinId));
      final paletteGenerator = await PaletteGenerator.fromImageProvider(image);
      colors[wallets[i].coinId] = paletteGenerator.dominantColor?.color ?? Colors.grey;
    }
    return colors;
  }

  ///Checks if the two wallet lists are equal by comparing wallet ids
  bool _walletsEqual(List<Wallet> l1, List<Wallet> l2) {
    if (l1.length != l2.length) {
      return false;
    }

    for (int i = 0; i < l1.length; i++) {
      if (l1[i].id != l2[i].id) {
        return false;
      }
    }
    return true;
  }
}