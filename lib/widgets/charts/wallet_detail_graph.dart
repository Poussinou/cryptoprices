import 'dart:ui';

import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/date_price.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/private_price_text.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

import '../../constants.dart';

class WalletDetailGraph extends StatefulWidget {
  final Wallet wallet;

  final List<MarketData> marketChart;

  final ValueChanged<TimePeriod> onSelectionChange;

  final ValueChanged<Tuple2<double, double>> graphTouched;

  ///The currency displayed in the graph
  final String currency;

  ///Show the coin amount instead of the value
  final bool displayCoinAmount;

  WalletDetailGraph({
    Key? key,
    required this.wallet,
    required this.marketChart,
    required this.onSelectionChange,
    required this.graphTouched,
    required this.currency,
    this.displayCoinAmount = false
    
  }) : super(key: key);

  @override
  _WalletDetailGraphState createState() => _WalletDetailGraphState();
}

class _WalletDetailGraphState extends State<WalletDetailGraph> {
  final _boldText = TextStyle(fontWeight: FontWeight.bold);
  late TextStyle _graphText;
  final _graphTextBlackBold = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);

  List<bool> _isSelected = [false, true, false, false, false ,false];

  ///Time periods ["hour", "day", "week", "month", "year", "max"]
  TimePeriod _isSelectedPeriod = TimePeriod.day;

  ///space between the graph and the min/max value
  final _minMaxTextPadding = 6.0;

  ///Padding left and right of the graph widget
  final _graphPaddingSides = 28.0;

  ///color of the graph
  late Color graphColor;

  ///text shown above the graph when touching a point on the graph
  String _selectedGraphPoint = "";

  ///is the graph being touched
  bool _touchDown = false;

  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();
    _graphText = currentTheme.isThemeDark()
      ? TextStyle(color: Colors.grey[400], fontSize: 16)
      : TextStyle(color: Colors.grey[600], fontSize: 16);

    graphColor = currentTheme.isThemeDark()
      ? Constants.accentColorDark!
      : Constants.accentColorLight!;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              _selectedGraphPoint,
              style: _graphTextBlackBold,
            )
          ],
        ),
        AspectRatio(
          aspectRatio: 16/14,
          child: Padding(
            padding: EdgeInsets.fromLTRB(_graphPaddingSides, 0, _graphPaddingSides, 0),
            child: (_isDataAvailable(widget.wallet, widget.marketChart))
              ? Listener(
                  child: LineChart(
                    _mainData(widget.wallet, widget.marketChart),
                  ),
                  onPointerDown: (pointerDownEvent) {
                    setState(() {
                      _touchDown = true;
                    });
                  },
                  //check for pointer up even if touch moves outside the graph or between the graph data points (hourly data)
                  onPointerUp: (pointerUpEvent) {
                    setState(() {
                      _touchDown = false;
                      widget.graphTouched(Tuple2<double, double>(-1, 0));
                      _selectedGraphPoint = "";
                    });
                  },
              )
              : Align(
                  alignment: Alignment.center,
                  child: Text(S.of(context).errorNoDataAvailable),
              )
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ToggleButtons(
                children: [
                  Text(
                    S.of(context).graphToggleHour,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleDay,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleWeek,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleMonth,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleYear,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleMax,
                    style: _boldText,
                  ),
                ],
                fillColor: Theme.of(context).colorScheme.secondary.withOpacity(0.2),
                selectedColor: Theme.of(context).textTheme.bodyText2!.color,
                onPressed: _onDaysSelectionButtonPressed,
                isSelected: _isSelected
            )
          ],
        )
      ],
    );
  }

  ///Changes the selected time interval based on which toggle button is pressed
  void _onDaysSelectionButtonPressed(int buttonIndex) {
    setState(() {
      _isSelectedPeriod = TimePeriod.values[buttonIndex];
      widget.onSelectionChange(_isSelectedPeriod);
      for (int i = 0; i < _isSelected.length; i++) {
        if (i == buttonIndex) {
          _isSelected[i] = true;
        } else {
          _isSelected[i] = false;
        }
      }
    });
  }

  LineChartData _mainData(Wallet wallet, List<MarketData> marketChart) {
    MarketData marketData = _getCurrentMarketData(wallet, marketChart);

    var filteredData = _filterData(wallet, marketData);
    var flSpots = _getFlSpots(filteredData);

    //get index of highest and lowest value in the data list
    final highestValue = _getHighestValue(filteredData);
    final highestIndex = filteredData.indexWhere((element) => element.price == highestValue);

    final lowestValue = _getLowestValue(filteredData);
    final lowestIndex = filteredData.indexWhere((element) => element.price == lowestValue);

    return LineChartData(
      clipData: FlClipData.all(),
      gridData: FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        show: true,
        topTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: _graphText.fontSize! + _minMaxTextPadding,
            interval: (highestIndex.toDouble() == 0) ? 1 : highestIndex.toDouble(),
            getTitlesWidget: (value, titleMeta) {
              if (!_touchDown && value == highestIndex) {
                final valueText = Util.formatPrice(highestValue, twoDecimalsAbove: 1);
                return Padding(
                  padding: EdgeInsets.only(bottom: _minMaxTextPadding).add(_correctPadding(valueText, highestIndex, filteredData.length)),
                  child: PrivatePriceText(
                    price: valueText,
                    symbol: (widget.displayCoinAmount) ? widget.wallet.coinSymbol.toUpperCase() : Constants.currencySymbols[widget.currency]!,
                    style: _graphText
                  ),
                );
              } else return Text('');
            },
          )
        ),
        bottomTitles: AxisTitles(
          sideTitles: (!_touchDown
              ? SideTitles(
                  showTitles: true,
                  reservedSize:_graphText.fontSize! + _minMaxTextPadding,
                  interval: (lowestIndex.toDouble() == 0) ? 1 : lowestIndex.toDouble(),
                  getTitlesWidget: (value, titleMeta) {
                    if (value == lowestIndex) {
                      final valueText = Util.formatPrice(lowestValue, twoDecimalsAbove: 1);
                      return Padding(
                        padding: EdgeInsets.only(top: _minMaxTextPadding).add(_correctPadding(valueText, lowestIndex, filteredData.length)),
                        child: PrivatePriceText(
                          price: valueText,
                          symbol: (widget.displayCoinAmount) ? widget.wallet.coinSymbol.toUpperCase() : Constants.currencySymbols[widget.currency]!,
                          style: _graphText
                        ),
                      );
                    } else return Text('');
                  },
              )
              : SideTitles(
                  showTitles: true,
                  reservedSize: _graphText.fontSize! + _minMaxTextPadding,
                  interval: 1,
                  getTitlesWidget: (value, titleMeta) {
                    if (value == 0
                        || value == filteredData.length ~/ 4
                        || value == filteredData.length ~/ 2
                        || value == 3 * (filteredData.length ~/ 4)
                        || value == filteredData.length - 1
                    ) {
                      return Padding(
                        padding: EdgeInsets.only(top: _minMaxTextPadding),
                        child: Text(
                          _getSelectedDate(filteredData, value.toInt()),
                          style: _graphText,
                        ),
                      );
                    } else return Text('');
                  },
              )
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          )
        ),
        rightTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          )
        ),
      ),
      borderData:
      FlBorderData(show: false, border: Border(bottom: BorderSide(color: const Color(0xff37434d), width: 1), left: BorderSide(color: const Color(0xff37434d), width: 1))),
      minX: 0,
      maxX: filteredData.length.toDouble() - 1,
      minY: lowestValue, //get Value from filtered data instead of marketData to avoid using Values that have been filtered out
      maxY: highestValue,
      lineBarsData: [
        LineChartBarData(
          spots: flSpots,
          color: graphColor,
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            color: graphColor.withOpacity(0.3),
          ),
        ),
      ],
      lineTouchData: LineTouchData(
          touchTooltipData: LineTouchTooltipData( //don't show tooltip
              tooltipBgColor: Colors.white.withAlpha(0),
              getTooltipItems: (lineBarSpots) {
                return [
                  LineTooltipItem("", TextStyle())
                ];
              }
          ),
          touchCallback: (FlTouchEvent event, LineTouchResponse? touchResponse) {
            Tuple2<double, double> valueAndChange = Tuple2(-1, 0);

            if (touchResponse?.lineBarSpots != null) {
              DateTime date = filteredData[touchResponse!.lineBarSpots!.first.x.toInt()].date;
              double value = filteredData[touchResponse.lineBarSpots!.first.x.toInt()].price;
              valueAndChange = valueAndChange.withItem1(value);

              setState(() {
                //display date of the current point
                _selectedGraphPoint = date.localeFormat();
                double changePercentage = _calculateValueChangePercentage(value, filteredData);
                //disable scrolling and return selected value and change percentage
                widget.graphTouched(valueAndChange.withItem1(value).withItem2(changePercentage));
              });

              if (event is FlLongPressEnd || event is FlPanEndEvent || event is FlTapUpEvent) {
                setState(() {
                  _touchDown = false;
                  widget.graphTouched(Tuple2<double, double>(-1, 0));
                  _selectedGraphPoint = "";
                });
              }
            }
          }
      )
    );
  }

  ///Return the the market data currently selected by the buttons
  MarketData _getCurrentMarketData(Wallet wallet, List<MarketData> marketChart) {
    switch (_isSelectedPeriod) { //api only gives daily data. Select daily data for hour too
      case TimePeriod.hour : {
        return marketChart[0]; //use day data
      }

      case TimePeriod.day : {
        return marketChart[0]; //day
      }

      case TimePeriod.week : {
        return marketChart[1]; //week use month data
      }

      case TimePeriod.month : {
        return marketChart[1]; //month
      }

      case TimePeriod.year : {
        return marketChart[2]; //year use max data
      }

      case TimePeriod.max : { //choose chart depending on when the first transaction was
        if (wallet.firstTransactionDate == null)
          return marketChart[0];

        if (DateTime.now().difference(wallet.firstTransactionDate!).inDays < 1) //less than one day of data
          return marketChart[0];
        if (DateTime.now().difference(wallet.firstTransactionDate!).inDays < 30) //less than 30 days of data
          return marketChart[1];

        return marketChart[2]; //max
      }

      default : {
        return marketChart[0];
      }
    }
  }

  ///Returns the current list of dates and prices for the selected time period
  List<DatePrice> _getCurrentDatePrices(Wallet wallet, MarketData marketData) {
    if (wallet.firstTransactionDate == null) {
      return [];
    }
    switch (_isSelectedPeriod) {
      case TimePeriod.hour : { //hour
        return marketData.getPricesSinceDate(DateTime.now().subtract(Duration(hours: 1)));
      }

      case TimePeriod.day : { //day
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 1)));
        return data.everyNthEntry(3); //every third entry, ~ every 15 min
      }

      case TimePeriod.week : { //week
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 7)));
        return data.everyNthEntry(2); //every second entry, ~ every 2 hours, using monthly data (hourly)
      }

      case TimePeriod.month : { //month
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 30)));
        return data.everyNthEntry(6); //every 6th entry, ~ every 6 hours
      }

      case TimePeriod.year : { //year
        return marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 365))); //every day, using max data (daily)
      }

      case TimePeriod.max : { //max
        return marketData.getPricesSinceDate(wallet.firstTransactionDate!);
      }

      default : {
        return [];
      }
    }
  }

  ///Filters the market data and selects only a certain number of entries
  List<DatePrice> _filterData(Wallet wallet, MarketData marketData) {
    List<DatePrice> list = [];
    final data = _getCurrentDatePrices(wallet, marketData);

    switch (_isSelectedPeriod) {
      case TimePeriod.hour : { //hour
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price); //use wallet value instead of coin price
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          )); //only the last hour, ~ every 5 min
        }
        return list;
      }

      case TimePeriod.day : { //day
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price);
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          ));
        }
        return list;
      }

      case TimePeriod.week : { //week
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price);
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          ));
        }
        return list;
      }

      case TimePeriod.month : { //month
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price);
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          ));
        }
        return list;
      }

      case TimePeriod.year : { //year
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price);
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          ));
        }
        return list;
      }

      case TimePeriod.max : { //max
        final data = _getCurrentDatePrices(wallet, marketData);
        for (int i = 0; i < data.length; i++) {
          double price = 0;
          if (widget.displayCoinAmount) {
            price = wallet.totalAmountAtDate(data[i].date);
          } else {
            price = wallet.totalValueAtDate(data[i].date, data[i].price);
          }
          list.add(DatePrice(
            data[i].date,
            price: price
          ));
        }
        return list;
      }

      default : {
        return [];
      }
    }
  }


  ///Returns a list of FlSpots with the given data for the graph to show
  List<FlSpot> _getFlSpots(List<DatePrice> filteredData) { //period from _isSelectedIndex
    List<FlSpot> list = [];

    for (int i = 0; i < filteredData.length; i++) {
      list.add(FlSpot(i.toDouble(), filteredData[i].price));
    }

    return list;
  }

  ///Returns the highest wallet value from the given list of DatePrices
  double _getHighestValue(List<DatePrice> values) {
    double max = values[0].price;
    for (int i = 0; i < values.length; i++) {
      if (values[i].price > max) {
        max = values[i].price;
      }
    }
    return max;
  }

  ///Returns the lowest wallet value from the given list of DatePrices
  double _getLowestValue(List<DatePrice> values) {
    double min = values[0].price;
    for (int i = 0; i < values.length; i++) {
      if (values[i].price < min) {
        min = values[i].price;
      }
    }
    return min;
  }

  ///Checks if the wallet value text is wider than the graph padding and is on the beginning or the end of the graph.
  ///If yes the padding of the min and max value is increased so they are not cut off
  EdgeInsets _correctPadding(String valueText, int valueIndex, int listLength) {
    final currencySymbol = (widget.displayCoinAmount) ? widget.wallet.coinSymbol.toUpperCase() : Constants.currencySymbols[widget.currency]!;
    final text = "$valueText $currencySymbol";

    final Size textSize = (TextPainter(
      text: TextSpan(text: text, style: _graphText),
      maxLines: 1,
      textScaleFactor: MediaQuery.of(context).textScaleFactor,
      textDirection: TextDirection.ltr)..layout()
    ).size;
    final halfTextWidth = textSize.width / 2;

    if (halfTextWidth >= _graphPaddingSides) { //potentially not enough room to display
      if (valueIndex <= listLength ~/ 8) { //is value at the beginning of the graph
        return EdgeInsets.only(left: halfTextWidth);
      }
      if (valueIndex >= 7 * (listLength ~/ 8)) { //is value at the end of the graph
        return EdgeInsets.only(right: halfTextWidth);
      }
    }
    return EdgeInsets.only();
  }

  ///Get the date string from the list entry with the given index from the given list.
  ///If the currently selected time period is year or max, the year will be shown
  String _getSelectedDate(List<DatePrice> data, int listIndex) {
    if (_isSelectedPeriod == TimePeriod.hour || _isSelectedPeriod == TimePeriod.day) { //hour or day
      return data[listIndex].date.getTimeOnly();
    }
    if (_isSelectedPeriod == TimePeriod.year) //year
      return _getFormatedDate(data[listIndex].date);

    return _getFormatedDate(data[listIndex].date, showYear: false); //week or month
  }

  ///Returns the change percentage between the selected Value and the first Value of the given DatePrice list
  double _calculateValueChangePercentage(double selectedValue, List<DatePrice> filteredData) {
    double firstValue = filteredData.first.price;
    if (firstValue == 0)
      if (selectedValue == 0)
        return 0;
      else
        return 100;

    return ((selectedValue - firstValue) / firstValue) * 100;
  }

  ///Checks if data is available for the currently selected period on the given market chart
  bool _isDataAvailable(Wallet wallet, List<MarketData> marketChart) {
    final marketData = _getCurrentMarketData(wallet, marketChart);
    final data = _getCurrentDatePrices(wallet, marketData);
    if (wallet.allTransactions.isEmpty || data.length <= 1) {
      return false;
    }
    return true;
  }

  ///Gets a date string in mm/yy or dd/mm format from DateTime in locale format.
  static String _getFormatedDate(DateTime dateTime, {bool showYear = true}) {
    if (showYear)
      return dateTime.yM();
    else
      return dateTime.mD();
  }
}