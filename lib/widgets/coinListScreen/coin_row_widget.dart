import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/detailScreen/coin_detail_screen.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../detailScreen/detail_screen_base.dart';
import '../favoritesScreen/favorite_widget.dart';

///Displays the given Coin information in a row.
///If openOnTap contains a route name, this widget is opened when tapping the row.
class CoinRowWidget extends StatefulWidget {
  ///Name of a route that is pushed when the widget is tapped
  final String openOnTap;

  ///An index number that is displayed at the start od the row
  final String index;

  ///The data that is displayed on the row
  final Coin coinData;

  ///If a FavoriteWidget should be displayed on the right side of the row
  final bool showFavoriteWidget;

  CoinRowWidget({
    Key? key,
    this.openOnTap = "",
    this.index = "",
    required this.coinData,
    required this.showFavoriteWidget
  }) : super(key: key);

  @override
  _CoinRowWidgetState createState() => _CoinRowWidgetState();
}

class _CoinRowWidgetState extends State<CoinRowWidget> {
  final _boldFont = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  final _smallerFont = TextStyle(fontSize: 14.0);
  late final _smallerFontRed;
  late final _smallerFontGreen;
  final _normalFont = TextStyle(fontSize: 16.0);

  late Widget _openOnTapWidget;

  String _currency = SettingsDAO().getCurrencyHive();

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _smallerFontRed = TextStyle(fontSize: 14.0, color: Constants.priceChangeRedDark);
      _smallerFontGreen = TextStyle(fontSize: 14.0, color: Constants.priceChangeGreenDark);
    } else {
      _smallerFontRed = TextStyle(fontSize: 14.0, color: Constants.priceChangeRedLight);
      _smallerFontGreen = TextStyle(fontSize: 14.0, color: Constants.priceChangeGreenLight);
    }

    if (widget.openOnTap.isNotEmpty) {
      switch (widget.openOnTap) {
        case CoinDetailScreen.routeName: {
          _openOnTapWidget = DetailScreenBase(coinId: widget.coinData.id);
          break;
        }

        default: {
          _openOnTapWidget = Row();
          break;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
          onTap: () {
            if (widget.openOnTap.isNotEmpty) {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => _openOnTapWidget
                )
              );
            }
          },
          child: Row(
            children: [
              Container(
                  margin: EdgeInsets.fromLTRB(4.0, 0, 4.0, 0),
                  child: Text(
                    widget.index.toString(),
                    style: _smallerFont,
                  )
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                child: CachedNetworkImage(
                  imageUrl: widget.coinData.imageURL,
                  placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error_outline),
                  height: 40.0,
                  width: 40.0,
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                widget.coinData.name,
                                style: _boldFont
                            ),
                            SizedBox(height: 10),
                            Text(
                                widget.coinData.symbol.toUpperCase(),
                                style: _smallerFont
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                              (Util.formatPrice(widget.coinData.price)) + " ${Constants.currencySymbols[_currency]}",
                              style: _normalFont
                          ),
                          SizedBox(height: 10),
                          Text(
                              (Util.formatPercentage(widget.coinData.priceChangePercentage24h) + "%"),
                              style: (widget.coinData.priceChangePercentage24h.isNegative ? _smallerFontRed : _smallerFontGreen)
                          )
                        ],
                      ),
                    )
                  ],
                )
              ),
              if (widget.showFavoriteWidget)
                FavoriteWidget(coin: widget.coinData)
            ],
          ),
        )
    );
  }
}