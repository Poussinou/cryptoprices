import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/coin_detail_dao.dart';
import 'package:crypto_prices/database/market_data_dao.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/models/coin_detail.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/widgets/charts/coin_detail_graph.dart';
import 'package:crypto_prices/widgets/priceAlertsScreen/price_alert_list_entry_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:tuple/tuple.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';
import '../../generated/l10n.dart';
import '../../util/util.dart';
import '../favoritesScreen/favorite_widget.dart';
import '../settingsScreens/rate_limitation_info.dart';

///Displays detailed information about a coin
class CoinDetailScreen extends StatefulWidget {
  static const routeName = "/coinDetail";
  final String coinId;

  CoinDetailScreen({Key? key, required this.coinId}) : super(key: key);

  @override
  _CoinDetailScreenState createState() => _CoinDetailScreenState();
}

class _CoinDetailScreenState extends State<CoinDetailScreen> {
  final _boldFont = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  final _smallerFont = TextStyle(fontSize: 16.0);
  late final _smallerFontRed;
  late final _smallerFontGreen;
  final _normalFont = TextStyle(fontSize: 18.0);
  late TextStyle _linkFont;
  final _headingFont = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  bool _disableScrolling = false;
  double _graphTouchedPrice = -1;
  double _graphTouchedChange = 0;

  ApiInteraction api = CoinGeckoAPI();

  late Future<CoinDetail> coinData;

  MarketDataDao _marketDataDao = MarketDataDao();
  CoinDetailDao _coinDetailDao = CoinDetailDao();

  PriceAlertListEntryDao _priceAlertListEntryDao = PriceAlertListEntryDao();

  SettingsDAO _settingsDAO = SettingsDAO();

  CoinDetail? _coinMarketData;

  PriceAlertListEntry? _entry;

  ///Selected time interval in the graph toggle buttons
  TimePeriod _selectedTimePeriod = TimePeriod.day;

  String _currency = SettingsDAO().getCurrencyHive();

  bool _refreshTriggered = false;

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _smallerFontRed = TextStyle(fontSize: 16.0, color: Constants.priceChangeRedDark);
      _smallerFontGreen = TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenDark);
    } else {
      _smallerFontRed = TextStyle(fontSize: 16.0, color: Constants.priceChangeRedLight);
      _smallerFontGreen = TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenLight);
    }

    //update data only after some time
    if (!_isDataUpdated(widget.coinId)) {
      coinData = _getCoinWithHistoricalMarketChart(widget.coinId, _currency);
    } else {
      _coinMarketData = _coinDetailDao.getCoinDetail(widget.coinId);
      //add chart if market data was changed in another widget
      _coinMarketData!.marketChart!.clear();
      _coinMarketData!.marketChart!.addAll(_marketDataDao.getCoinMarketChart(widget.coinId, _currency));
    }

    _entry = _priceAlertListEntryDao.getEntry(widget.coinId);
    
    _priceAlertListEntryDao.getAllEntriesChangeWatcher().listen((newResult) { //listen if entry was deleted
      if (this.mounted) {
        setState(() {
          _entry = _priceAlertListEntryDao.getEntry(widget.coinId);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 18.0, color: Theme.of(context).colorScheme.secondary);
    if (_coinMarketData == null) { //no data from database
      return FutureBuilder<CoinDetail>(
        future: coinData,
        builder: (context, snapshot) {
          //snapshot state doesn't change until loading is finished, so when refreshing after error
          //the state will stay 'hasError' and no loading screen will be displayed
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            );
          }

          if (snapshot.hasData) {
            CoinDetail coin = snapshot.data!;
            _coinMarketData = coin; //assign for future rebuilds

            return RefreshIndicator(
              key: _refreshIndicatorKey,
              onRefresh: _refresh,
              child: SingleChildScrollView(
                physics: (_disableScrolling ? NeverScrollableScrollPhysics() : AlwaysScrollableScrollPhysics()),
                child: Column(
                  children: [
                    _buildTopRow(coin),
                    CoinDetailGraph(
                      marketChart: coin.marketChart!.toList(),
                      onSelectionChange: _onTimePeriodSelectionChange,
                      graphTouched: _onGraphTouched,
                    ),
                    if (_entry != null)
                      Container(
                        padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                        child: PriceAlertListEntryWidget(
                          coinId: coin.id,
                          dontDisplayCoin: true,
                        ),
                      ),
                    _buildInfoRows(coin)
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            if (snapshot.error is SocketException) {
              return _buildError(S.of(context).errorMessageSocket);
            }

            if (snapshot.error is HttpException) {
              return _buildError(S.of(context).errorMessageHttp);
            }

            if (snapshot.error is RateLimitationException) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _refresh,
                child: LayoutBuilder(
                  builder: (context, constraints) => ListView( //no single child scrollview to constrain height
                    children: [
                      Container(
                        height: constraints.maxHeight,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                S.of(context).errorMessageRateLimitation,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 10),
                              RichText(
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: S.of(context).moreInformation,
                                  style: _linkFont,
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) => RateLimitationInfo())
                                    );
                                  }
                                )
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              );
            }

            return _buildError(S.of(context).error + ": " + snapshot.error.toString());
          }

          return Center(
            child: Column(
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 10),
                Text(S.of(context).loadingDataMessage)
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          );
        },
      );
    } else { //data from database
      return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: SingleChildScrollView(
          physics: (_disableScrolling ? NeverScrollableScrollPhysics() : AlwaysScrollableScrollPhysics()),
          child: Column(
            children: [
              _buildTopRow(_coinMarketData!),
              CoinDetailGraph(
                marketChart: _coinMarketData!.marketChart!.toList(),
                onSelectionChange: _onTimePeriodSelectionChange,
                graphTouched: _onGraphTouched,
              ),
              if (_entry != null)
                Container(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: PriceAlertListEntryWidget(
                    coinId: _coinMarketData!.id,
                    dontDisplayCoin: true,
                  ),
                ),
              _buildInfoRows(_coinMarketData!)
            ],
          ),
        ),
      );
    }
  }

  ///Return the top row of the widget with the coin name, logo, price
  Widget _buildTopRow(CoinDetail coin) {
    return Container(
      margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 16.0),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.all(8.0),
            child: CachedNetworkImage(
              imageUrl: coin.imageURL,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
              height: 50.0,
              width: 50.0,
            ),
          ),
          Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              coin.name,
                              style: _boldFont
                          ),
                          SizedBox(height: 10),
                          Text(
                              coin.symbol.toUpperCase(),
                              style: _smallerFont
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                            (_graphTouchedPrice != -1
                                ? (Util.formatPrice(_graphTouchedPrice)) + " ${Constants.currencySymbols[_currency]}"
                                : (Util.formatPrice(coin.price)) + " ${Constants.currencySymbols[_currency]}"
                            ),
                            style: _normalFont
                        ),
                        SizedBox(height: 10),
                        Text(
                            (_graphTouchedPrice != -1
                                ? (Util.formatPercentage(_graphTouchedChange) + "%")
                                : (Util.formatPercentage(_getPriceChangePercent(coin, _selectedTimePeriod)) + "%")
                            ),
                            style: ((_graphTouchedPrice != -1
                                ? _graphTouchedChange
                                : _getPriceChangePercent(coin, _selectedTimePeriod)).isNegative ? _smallerFontRed : _smallerFontGreen
                            )
                        )
                      ],
                    ),
                  )
                ],
              )
          ),
          FavoriteWidget(coin: coin)
        ],
      ),
    );
  }

  ///Return the coin information below the graph
  Widget _buildInfoRows(CoinDetail coin) {
    String currencySymbol = Constants.currencySymbols[_currency]!;
    return Container(
      margin: EdgeInsets.all(22.0),
      child: Column(
        children : [
          _buildHeadingRow(S.of(context).detailHeadingMarketStats, 20),
          _buildTextRow("${S.of(context).detailFieldMarketCapRank}: ", "${coin.marketCapRank}", 25),
          _buildTextRow("${S.of(context).detailFieldMarketCap}: ", "${Util.formatInt(coin.marketCap.round())} $currencySymbol", 25),
          _buildTextRow("${S.of(context).detailFieldVolume}: ", "${Util.formatInt(coin.totalVolume.round())} $currencySymbol", 25),
          _buildTextRow("${S.of(context).detailFieldSupply}: ", Util.formatInt(coin.circulatingSupply.round()), 25),
          _buildTextRow("${S.of(context).detailField24HHigh}: ", "${Util.formatPrice(coin.high24h)} $currencySymbol", 25),
          _buildTextRow("${S.of(context).detailField24HLow}: ", "${Util.formatPrice(coin.low24h)} $currencySymbol", 25),
          _buildTextRow("${S.of(context).detailFieldAth}: ", "${Util.formatPrice(coin.atHigh)} $currencySymbol", 25),
          _buildTextRow("${S.of(context).detailFieldAtl}: ", "${Util.formatPrice(coin.atLow)} $currencySymbol", 35),

          _buildHeadingRow(S.of(context).detailHeadingAbout, 20),
          if ((coin.genesisDate ?? DateTime(0)).year != 0)
            _buildTextRow("${S.of(context).detailFieldGenesisDate}: ", coin.genesisDate!.getDateOnly(), 25),
          if (coin.homepage != "")
            _buildLinkRow("${S.of(context).detailFieldHomepage}: ", coin.homepage, 25)
        ],
      ),
    );
  }

  ///Return a heading with bold, left aligned text and space below
  Widget _buildHeadingRow(String text, double spaceBelow) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            text,
            style: _headingFont,
          ),
        ),
        SizedBox(height: spaceBelow)
      ],
    );
  }

  ///Return a row with left aligned text and space below
  Widget _buildTextRow(String leftText, String rightText, double spaceBelow) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                leftText,
                style: _normalFont,
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                rightText,
                style: _normalFont,
              ),
            )
          ],
        ),
        SizedBox(height: spaceBelow)
      ],
    );
  }

  ///Return a row with left aligned text, a right aligned, clickable link and space below
  Widget _buildLinkRow(String leftText, String url, double spaceBelow) {
    String urlText = "";
    //cut off https://www. from text shown
    if (url.contains("://www")) {
      urlText = url.split("www.").last;
    } else {
      urlText = url.split("://").last;
    }
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                leftText,
                style: _normalFont,
              ),
            ),
            Flexible(
              child: RichText(
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: urlText,
                  style: _linkFont,
                  recognizer: TapGestureRecognizer()..onTap = () {
                    _launchUrl(url);
                  }
                )
              )
            )
          ],
        ),

        SizedBox(height: spaceBelow)
      ],
    );
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  ///Launches an url in the browser
  void _launchUrl(String url) async =>
      await canLaunch(url)
          ? await launch(url)
          : _showAlertDialog(
          S.of(context).urlLaunchAlertDialogTitle,
          S.of(context).urlLaunchAlertDialogMessage
      );

  ///Displays an alert dialog with the given title and message
  void _showAlertDialog(String title, String message) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).alertDialogClose)
              )
            ],
          );
        }
    );
  }

  ///Force an update of the data from the api
  Future<void> _refresh() async {
    setState(() {
      _refreshTriggered = true;
      _coinMarketData = null;
      coinData = _getCoinWithHistoricalMarketChart(widget.coinId, _currency);
    });

  }

  ///Check if the coin data and the market chart for the given coin is up to date
  bool _isDataUpdated(String coinId) {
    DateTime? lastUpdated = _coinDetailDao.getCoinDetail(coinId)?.lastUpdated;
    bool coinCurrencyChanged = _coinDetailDao.getCurrencyChanged(coinId);

    bool laterThanUpdateTimer = lastUpdated == null || DateTime.now().difference(lastUpdated).inMinutes > Constants.UPDATETIMERMINUTES;

    if (laterThanUpdateTimer || coinCurrencyChanged) //coin data not up to date
      return false;

    final marketChart = _marketDataDao.getCoinMarketChart(coinId, _currency);
    if (marketChart.length != marketDataDays.length) //some market data missing
      return false;

    for (int i = 0; i < marketChart.length; i++) { //market data in database not up to date
      if (DateTime.now().difference(marketChart[i].lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES) {
        return false;
      }
    }
    return true;
  }

  ///Fetches the detailed coin and the complete market chart from the API and caches them
  Future<CoinDetail> _getCoinWithHistoricalMarketChart(String coinId, String currency) async {
    try {
      CoinDetail? coin = _coinDetailDao.getCoinDetail(coinId);
      List<MarketData> marketChart = [];
      bool coinCurrencyChanged = _coinDetailDao.getCurrencyChanged(coinId);
      if (coin == null ||
          DateTime.now().difference(coin.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
          _refreshTriggered ||
          coinCurrencyChanged
      ) { //coin data outdated
        coin = await api.getCoinData(coinId, currency);
        _coinDetailDao.insertCoinDetail(coin);
        _coinDetailDao.setCurrencyChanged(coinId, false);
      }

      List<String> timePeriodsToGet = [];
      marketDataDays.forEach((period) {
        final data = _marketDataDao.getCoinMarketDataForPeriod(coinId, period, _currency);
        //does updated data exist in the database
        if (data?.lastUpdated == null ||
            DateTime.now().difference(data!.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
            _refreshTriggered
        ) {
          timePeriodsToGet.add(period);
        }
      });

      _refreshTriggered = false;

      if (timePeriodsToGet.length == marketDataDays.length) { //get all data from API
        marketChart = await api.getHistoricalMarketChart(coinId, currency);
        _marketDataDao.insertMultipleMarketData(marketChart);
        coin.marketChart = HiveList<MarketData>(Hive.box(Constants.MARKETDATABOXNAME), objects: marketChart); //HiveList is null from API
        coin.save();
        return coin;
      }

      List<MarketData> apiData = [];
      for (int i = 0; i < timePeriodsToGet.length; i++) { //get only necessary data from API
        final data = await api.getMarketDataDays(coinId, currency, timePeriodsToGet[i]);
        apiData.add(data);
      }
      _marketDataDao.insertMultipleMarketData(apiData);

      //HiveList is null from API
      coin.marketChart = HiveList<MarketData>(Hive.box(Constants.MARKETDATABOXNAME), objects: _marketDataDao.getCoinMarketChart(coinId, _currency));
      coin.save();

      return coin;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
    on RateLimitationException {
      throw RateLimitationException("Too many requests");
    }
  }

  ///Change the selected time period
  void _onTimePeriodSelectionChange(TimePeriod newValue) {
    setState(() {
      _selectedTimePeriod = newValue;
    });
  }

  ///Disable scrolling when the graph is touched and display the price
  ///and change percentage values of the touched point on the graph
  void _onGraphTouched(Tuple2<double, double> priceAndChange) {
    setState(() {
      _disableScrolling = priceAndChange.item1 != -1;
      _graphTouchedPrice = priceAndChange.item1;
      _graphTouchedChange = priceAndChange.item2;
    });
  }

  ///Get the correct price change percentage value for the selected time period
  double _getPriceChangePercent(CoinDetail coin, TimePeriod period) { //period selected by toggle buttons
    switch (period) {
      case TimePeriod.hour : { //hour
        return coin.priceChangePercentage60m;
      }

      case TimePeriod.day : { //day
        return coin.priceChangePercentage24h;
      }

      case TimePeriod.week : { //week
        if (coin.priceChangePercentage7d == 0) { //coin is younger than time period
          return coin.priceChangePercentageMax;
        }
        return coin.priceChangePercentage7d;
      }

      case TimePeriod.month : { //month
        if (coin.priceChangePercentage30d == 0) { //coin is younger than time period
          return coin.priceChangePercentageMax;
        }
        return coin.priceChangePercentage30d;
      }

      case TimePeriod.year : { //year
        if (coin.priceChangePercentage1y == 0) { //coin is younger than time period
          return coin.priceChangePercentageMax;
        }
        return coin.priceChangePercentage1y;
      }

      case TimePeriod.max : { //max
        return coin.priceChangePercentageMax;
      }

      default : {
        return 0;
      }
    }
  }
}