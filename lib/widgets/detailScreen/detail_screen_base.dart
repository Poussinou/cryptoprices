import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/widgets/detailScreen/coin_detail_screen.dart';
import 'package:crypto_prices/widgets/detailScreen/wallet_detail_screen.dart';
import 'package:crypto_prices/widgets/portfolioScreen/edit_portfolio_widget.dart';
import 'package:crypto_prices/widgets/portfolioScreen/portfolio_selection_dropdown.dart';
import 'package:crypto_prices/widgets/portfolioScreen/transaction_edit_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:flutter/material.dart';

import '../priceAlertsScreen/price_alert_edit_widget.dart';

///The base widget that displays the coin and wallet detail widgets
class DetailScreenBase extends StatefulWidget {
  static const routeName = "/detail";

  final String coinId;

  ///Id of the currently selected portfolio
  final int selectedPortfolioId;

  ///Show wallet detail as first widget
  final bool showWallet;

  DetailScreenBase({
    Key? key,
    required this.coinId,
    this.selectedPortfolioId = 0,
    this.showWallet = false,
  }) : super(key: key);

  @override
  _DetailScreenBaseState createState() => _DetailScreenBaseState();
}

class _DetailScreenBaseState extends State<DetailScreenBase> {
  late TextStyle _linkFont;

  bool _showWallet = false;
  String _coinName = "";
  String _coinSymbol = "";
  int _selectedPortfolioId = 0;
  Portfolio? _portfolio;
  bool _isAuthenticated = false;

  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();
    _showWallet = widget.showWallet;

    _isAuthenticated = _settingsDAO.getIsAuthenticated();

    final coin = AvailableCoinsDao().getAvailableCoin(widget.coinId)!;
    _coinName = coin.name;
    _coinSymbol = coin.symbol;

    if (widget.selectedPortfolioId == 0) {
      _selectedPortfolioId = _settingsDAO.getDefaultPortfolioIdHive();
    } else {
      _selectedPortfolioId = widget.selectedPortfolioId;
    }
    _portfolio = PortfolioDao().getPortfolio(_selectedPortfolioId);
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 15.0, color: Theme.of(context).colorScheme.secondary);

    return Scaffold(
      appBar: _buildAppBar(),
      body: (_showWallet ?
        Center(
          child: _buildWalletScreen()
        ) :
        Center(
          child: CoinDetailScreen(coinId: widget.coinId)
        )),
    );
  }

  ///Returns the AppBar with title and actions
  AppBar _buildAppBar() {
    List<Widget> actions = [];
    if (_showWallet) {
      if (_isAuthenticated && _selectedPortfolioId != Constants.NODEFAULTPORTFOLIOID) {
        if (_portfolio?.getPortfolioWalletByCoinId(widget.coinId) != null) {
          actions.add(
            IconButton(
              icon: Icon(Icons.delete_outline),
              tooltip: S.of(context).deleteWallet,
              onPressed: () {
                _onWalletDeleteButtonPressed();
              },
            )
          );
        }
        actions.add(
          IconButton(
            icon: Icon(Icons.data_saver_on),
            tooltip: S.of(context).addTransaction,
            onPressed: () async {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TransactionEditWidget(
                    transactionId: 0,
                    coinId: widget.coinId,
                    selectedPortfolioId: _selectedPortfolioId,
                  )
                )
              );
            }
          )
        );
      }

      actions.add(
        IconButton(
          icon: Icon(Icons.show_chart),
          tooltip: S.of(context).showCoinDetails,
          onPressed: () {
            setState(() {
              _showWallet = false;
            });
          }
        )
      );
    } else {
      actions.add(
        IconButton(
          icon: Icon(Icons.add_alert),
          tooltip: S.of(context).alertAddTooltip,
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PriceAlertEditWidget(
                    alertId: 0,
                    coinId: widget.coinId,
                    coinName: _coinName,
                    coinSymbol: _coinSymbol,
                  )
              )
            );
          }
        )
      );

      actions.add(
        IconButton(
          icon: Icon(Icons.pie_chart_outline),
          tooltip: S.of(context).showWallet,
          onPressed: () {
            setState(() {
              _showWallet = true;
            });
          }
        )
      );
    }
    return AppBar(
      title: (_showWallet
        ? PortfolioSelectionDropdown(
            onItemTapped: _handlePortfolioSelection,
            selectedPortfolioId: _selectedPortfolioId
        )
        : Text(S.of(context).detailAppbarTitle)),
      actions: actions,
    );
  }

  ///Returns the wallet in the selected portfolio
  ///or a button to add a new portfolio if none exists
  Widget _buildWalletScreen() {
    if (!_isAuthenticated) {
      return _buildPortfolioLockScreen();
    }
    if (_selectedPortfolioId != Constants.NODEFAULTPORTFOLIOID) {
      return WalletDetailScreen(coinId: widget.coinId, selectedPortfolioId: _selectedPortfolioId);
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            S.of(context).noPortfolios,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10),
          TextButton(
            onPressed: () async {
              final newPortfolioId = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditPortfolioWidget(
                      selectedPortfolioId: widget.selectedPortfolioId
                  )
                )
              );
              if (newPortfolioId != null) {
                _handlePortfolioSelection(newPortfolioId);
              }
            },
            child: Text("+ ${S.of(context).addPortfolio}", style: _linkFont)
          )
        ],
      );
    }
  }

  ///Builds a lock screen with a button to unlock and a message
  Widget _buildPortfolioLockScreen() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          S.of(context).portfolioLocked,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
        SizedBox(height: 20),
        ElevatedButton(
          child: Text(
            S.current.authenticationUnlockButton,
            style: TextStyle(fontSize: 18),
          ),
          onPressed: () async {
            final result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AuthenticationWidget()));
            if (result == Constants.successResult) {
              setState(() {
                _isAuthenticated = _settingsDAO.getIsAuthenticated();
              });
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(S.of(context).authenticationRequired),
                  duration: Duration(seconds: 2),
                ),
              );
            }
          }
        ),
      ],
    );
  }

  ///Changes the selected portfolio when one is selected from the menu
  void _handlePortfolioSelection(int newPortfolioId) {
    setState(() {
      _selectedPortfolioId = newPortfolioId;
    });
  }

  ///Opens an alert dialog to confirm deletion of the selected transaction.
  ///If no transaction is selected, close the widget instead
  void _onWalletDeleteButtonPressed() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).deleteWallet),
          content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                _portfolio!.deletePortfolioWalletByCoinId(widget.coinId);
                Navigator.pop(context); //pop dialog
                Navigator.pop(context, true); //pop widget
              },
              child: Text(S.of(context).alertDialogDelete)
            ),
          ],
        );
      }
    );
  }
}