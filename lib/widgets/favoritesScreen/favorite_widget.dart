import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/favorites_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:flutter/material.dart';

///Displays a star clickable icon to favorite a coin
class FavoriteWidget extends StatefulWidget {
  FavoriteWidget({Key? key, required this.coin}) : super(key: key);

  ///The coin that is being favorited
  final Coin coin;

  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = false;
  FavoritesDao _favoritesDao = FavoritesDao();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _isFavorited = _checkIfEntryExists(widget.coin.id);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.center,
            icon: (_isFavorited ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Constants.favoriteIconColor,
            onPressed: _toggleFavorite,
          ),
        )
      ],
    );
  }

  ///Add or remove a favorite when pressing the button
  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _removeFavoriteCoin(widget.coin.id);
        _isFavorited = false;
      } else {
        _addFavoriteCoin(widget.coin);
        _isFavorited = true;
      }
    });
  }

  ///Checks if a Favorite-entry already exists for the given coin
  bool _checkIfEntryExists(String id) {
    if (_favoritesDao.getFavorite(id) != null) {
      return true;
    }
    return false;
  }

  ///Add a FavoriteCoin to the database and registers a LargeMoveAlert
  void _addFavoriteCoin(Coin c) {
    var fav = FavoriteCoin(
      c.id,
      name: c.name,
      symbol: c.symbol,
      favoriteListPosition: _favoritesDao.favoritesCount()
    );
    _favoritesDao.insertFavorite(fav);
  }

  ///Remove the FavoriteCoin from the database by id and deletes the associated LargeMoveAlert
  void _removeFavoriteCoin(String id) {
    FavoriteCoin oldFav = _favoritesDao.getFavorite(id)!;
    _favoritesDao.deleteFavorite(id);

    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(S.of(context).favoriteRemoved),
        duration: Duration(seconds: 2),
        action: SnackBarAction(
          label: S.of(context).undo,
          onPressed: () {
            if (this.mounted) {
              setState(() {
                oldFav.favoriteListPosition = _favoritesDao.favoritesCount();
                _favoritesDao.insertFavorite(oldFav);
              });
            } else {
              oldFav.favoriteListPosition = _favoritesDao.favoritesCount();
              _favoritesDao.insertFavorite(oldFav);
            }
          },
        ),
      )
    );
  }
}