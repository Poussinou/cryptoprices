import 'dart:io';

import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/global_market_info_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../constants.dart';
import 'charts/coin_market_share_pie_chart.dart';
import 'settingsScreens/rate_limitation_info.dart';

class GlobalMarketInfoWidget extends StatefulWidget {
  @override
  _GlobalMarketInfoWidgetState createState() => _GlobalMarketInfoWidgetState();
}

class _GlobalMarketInfoWidgetState extends State<GlobalMarketInfoWidget> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  late TextStyle _linkFont;
  late final _biggerFontRed;
  late final _biggerFontGreen;
  final _biggerFont = TextStyle(fontSize: 18);
  final _biggerFontBold = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  GlobalMarketInfo? _globalMarketInfo;

  late Future<GlobalMarketInfo> _getGlobalMarketInfo;

  ApiInteraction _api = CoinGeckoAPI();

  GlobalMarketInfoDao _globalMarketInfoDao = GlobalMarketInfoDao();
  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedDark);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenDark);
    } else {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedLight);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenLight);
    }

    DateTime? lastUpdated = _settingsDAO.getGlobalMarketInfoLastUpdatedHive();

    if (lastUpdated == null || DateTime.now().difference(lastUpdated).inMinutes > Constants.UPDATETIMERMINUTES) {
      _getGlobalMarketInfo = _api.getGlobalMarketInfo(_settingsDAO.getCurrencyHive());
    } else {
      //get from database
      _globalMarketInfo = _globalMarketInfoDao.getGlobalMarketInfo()!;
    }
  }
  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 15.0, color: Theme.of(context).colorScheme.secondary);
    return Scaffold(
      appBar: AppBar(title: Text(S.of(context).globalMarketInfoTile)),
      body: _buildData()
    );
  }

  ///Builds the data either with FutureBuilder or not
  Widget _buildData() {
    if (_globalMarketInfo == null) {
      return FutureBuilder<GlobalMarketInfo>(
        future: _getGlobalMarketInfo,
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            );
          }
          if (snapshot.hasData) {
            _globalMarketInfo = snapshot.data!;
            _globalMarketInfoDao.insertGlobalMarketInfo(_globalMarketInfo!);
            _settingsDAO.setGlobalMarketInfoLastUpdated(DateTime.now());

            return _displayData(_globalMarketInfo!);
          } else if (snapshot.hasError) {
            if (snapshot.error is SocketException) {
              return _buildError(S.of(context).errorMessageSocket);
            }

            if (snapshot.error is HttpException) {
              return _buildError(S.of(context).errorMessageHttp);
            }

            if (snapshot.error is RateLimitationException) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _refresh,
                child: LayoutBuilder(
                  builder: (context, constraints) => ListView( //no single child scrollview to constrain height
                    children: [
                      Container(
                        height: constraints.maxHeight,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                S.of(context).errorMessageRateLimitation,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 10),
                              RichText(
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: S.of(context).moreInformation,
                                  style: _linkFont,
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) => RateLimitationInfo())
                                    );
                                  }
                                )
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              );
            }

            return _buildError(S.of(context).error + ": " + snapshot.error.toString());
          }

          return Center(
            child: Column(
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 10),
                Text(S.of(context).loadingDataMessage)
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          );
        }
      );
    } else {
      return _displayData(_globalMarketInfo!);
    }
  }

  ///Displays the given data
  Widget _displayData(GlobalMarketInfo data) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                S.of(context).globalMarketInfoMarketCap(
                  Util.formatInt(data.totalMarketCap.round()),
                  Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!
                ),
                style: _biggerFont,
              ),
              SizedBox(
                height: 32,
              ),
              Row(
                children: [
                  Text(
                    S.of(context).globalMarketInfoMarketCapChange,
                    style: _biggerFont
                  ),
                  Text(
                    "${Util.formatPercentage(data.marketCapChangePercentage24H)}%",
                    style: (data.marketCapChangePercentage24H.isNegative) ? _biggerFontRed : _biggerFontGreen,
                  ),
                ],
              ),
              SizedBox(
                height: 32,
              ),
              Text(
                S.of(context).globalMarketInfoGlobalVolume(
                  Util.formatInt(data.total24HVolume.round()),
                  Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!
                ),
                style: _biggerFont,
              ),
              SizedBox(
                height: 32,
              ),
              Text(
                S.of(context).globalMarketInfoActiveCrypto(data.activeCryptoCurrencies),
                style: _biggerFont,
              ),
              SizedBox(
                height: 32,
              ),
              Text(
                S.of(context).globalMarketInfoMarketShare,
                style: _biggerFont,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: CoinMarketSharePieChart(globalMarketInfo: data),
              )
            ],
          ),
        )
      ),
    );
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  Future _refresh() async {
    setState(() {
      _globalMarketInfo = null;
      _getGlobalMarketInfo = _api.getGlobalMarketInfo(_settingsDAO.getCurrencyHive());
    });
  }
}