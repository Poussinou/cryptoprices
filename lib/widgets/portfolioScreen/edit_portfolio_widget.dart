import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/currency_widget.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

///Shows all portfolio information that can be changed
///when editing or creating a new portfolio
class EditPortfolioWidget extends StatefulWidget {
  final int selectedPortfolioId;

  const EditPortfolioWidget({
    Key? key,
    required this.selectedPortfolioId
  }) : super(key: key);

  @override
  _EditPortfolioWidgetState createState() => _EditPortfolioWidgetState();
}

class _EditPortfolioWidgetState extends State<EditPortfolioWidget> {
  TextStyle _biggerFontBold = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  TextStyle _normalFont = TextStyle(fontSize: 16.0);

  final _formKey = GlobalKey<FormState>();
  TextEditingController _portfolioNameController = TextEditingController();

  Portfolio _selectedPortfolio = Portfolio(0, "");
  String _appBarTitle = "";
  String _currency = "";
  bool _isDefault = false;
  bool _useAppCurrency = true;

  PortfolioDao _portfolioDao = PortfolioDao();
  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();

    if (widget.selectedPortfolioId != 0) {
      _selectedPortfolio = _portfolioDao.getPortfolio(widget.selectedPortfolioId)!;
      _portfolioNameController.text = _selectedPortfolio.name;
      _currency = _selectedPortfolio.currency;
      _useAppCurrency = _selectedPortfolio.useAppCurrency;
      if (_settingsDAO.getDefaultPortfolioIdHive() == _selectedPortfolio.id) {
        _isDefault = true;
      }
      _appBarTitle = S.current.portfolioEditEditPortfolio;
    } else {
      _appBarTitle = S.current.newPortfolio;
      _currency = _settingsDAO.getCurrencyHive();
      if (_settingsDAO.getDefaultPortfolioIdHive() == Constants.NODEFAULTPORTFOLIOID) {
        _isDefault = true;
      }
    }
  }

  @override
  void dispose() {
    _portfolioNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_appBarTitle),
        actions: [
          IconButton(
            onPressed: () => _onDeleteButtonPressed(),
            icon: Icon(Icons.delete),
            tooltip: S.of(context).deletePortfolio,
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildCard(
                  title: S.of(context).name,
                  children: [
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: TextFormField(
                        controller: _portfolioNameController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                          border: OutlineInputBorder(),
                          hintText: S.of(context).portfolioEditNameHintText,
                          errorStyle: TextStyle(
                            fontSize: 14,
                            color: (currentTheme.isThemeDark())
                                ? Constants.errorColorDark
                                : Constants.errorColor,
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return S.of(context).portfolioEditNameErrorText;
                          }
                          return null;
                        },
                      ),
                    )
                  ]
                ),
                _buildCard(
                  title: S.of(context).alertCurrency,
                  children: [
                    ListTile(
                      title: Text(
                        _currency.toUpperCase(),
                        style: _biggerFontBold,
                      ),
                      leading: Image(
                        image: AssetImage("assets/images/currencies/$_currency.png"),
                        width: 25,
                        height: 25,
                      ),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () {
                        Navigator.of(context).push(Util.createSlidingRoute(CurrencyWidget(
                          onCurrencyChange: _onCurrencyChanged,
                          setCurrency: _currency
                        )));
                      },
                      enabled: !_useAppCurrency,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                      child: Row(
                        children: [
                          Checkbox(
                            fillColor: MaterialStateProperty.resolveWith((states) => Theme.of(context).colorScheme.secondary),
                            checkColor: (currentTheme.isThemeDark() ? Colors.black : Colors.white),
                            value: _useAppCurrency,
                            onChanged: (bool? value) {
                              setState(() {
                                _useAppCurrency = value!;
                                if (_useAppCurrency) {
                                  _currency = _settingsDAO.getCurrencyHive();
                                }
                              });
                            }
                          ),
                          Text(
                            S.of(context).portfolioEditUseAppCurrency,
                            style: _normalFont
                          )
                        ],
                      ),
                    )
                  ]
                ),
                SwitchListTile(
                  title: Text(S.of(context).portfolioEditDefaultPortfolio),
                  secondary: Icon(Icons.home),
                  activeColor: Theme.of(context).colorScheme.secondary,
                  value: _isDefault,
                  onChanged: (bool newValue) {
                    if (_portfolioDao.getAllPortfolios().length == 0
                        || (_portfolioDao.getAllPortfolios().length == 1 && _settingsDAO.getDefaultPortfolioIdHive() == _selectedPortfolio.id)
                    ) {
                      return null;
                    } else {
                      setState(() {
                        _isDefault = newValue;
                      });
                    }
                  }
                )
              ],
            ),
          ),
        )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: _validateForm
      ),
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children, EdgeInsets textPadding = const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0)}) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: textPadding,
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Changes the selected currency to the new one selected in the CurrencyWidget
  void _onCurrencyChanged(String newCurrency) {
    setState(() {
      _currency = newCurrency;
    });
  }

  ///Opens an alert dialog to confirm deletion of the selected portfolio.
  ///If no portfolio is selected, close the widget instead
  void _onDeleteButtonPressed() {
    if (widget.selectedPortfolioId == 0) {
      Navigator.pop(context);
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).deletePortfolio),
            content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).alertDialogCancel)
              ),
              TextButton(
                onPressed: () {
                  _portfolioDao.deletePortfolio(widget.selectedPortfolioId);
                  Navigator.pop(context); //pop dialog
                  Navigator.pop(context, Constants.DELETERESULT); //pop widget
                },
                child: Text(S.of(context).alertDialogDelete)
              ),
            ],
          );
        }
      );
    }
  }

  ///Checks if the Form is valid
  void _validateForm() {
    if (_formKey.currentState!.validate()) {
      if (widget.selectedPortfolioId != 0) {
        _selectedPortfolio.name = _portfolioNameController.text;
        _selectedPortfolio.currency = _currency;
        _selectedPortfolio.useAppCurrency = _useAppCurrency;
        if (_isDefault) {
          _settingsDAO.setDefaultPortfolioId(_selectedPortfolio.id);
        } else {
          if (_settingsDAO.getDefaultPortfolioIdHive() == _selectedPortfolio.id) { //was default portfolio
            final newDefaultId = _portfolioDao.getAllPortfolios().firstWhere((element) => element.id != _selectedPortfolio.id).id;
            _settingsDAO.setDefaultPortfolioId(newDefaultId);
          }
        }
        _selectedPortfolio.save();
        Navigator.pop(context, _selectedPortfolio.id);
      } else {
        final newPortfolio = Portfolio(
          Util.generateId(),
          _portfolioNameController.text,
          currency: _currency,
          useAppCurrency: _useAppCurrency
        );
        if (_isDefault) {
          _settingsDAO.setDefaultPortfolioId(newPortfolio.id);
        }
        _portfolioDao.insertPortfolio(newPortfolio);
        Navigator.pop(context, newPortfolio.id);
      }
    }
  }
}
