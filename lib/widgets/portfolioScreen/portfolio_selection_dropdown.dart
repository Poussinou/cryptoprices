import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/portfolioScreen/edit_portfolio_widget.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

///A dropdown menu that displays all portfolios and returns
///the id of the selected portfolio on tap
class PortfolioSelectionDropdown extends StatefulWidget {
  final int selectedPortfolioId;

  final ValueChanged<int> onItemTapped;

  ///If false, sets the text color and the dropdown theme to dark so it is visible on the white background
  final bool isDark;

  const PortfolioSelectionDropdown({
    Key? key,
    required this.onItemTapped,
    required this.selectedPortfolioId,
    this.isDark = true
  }) : super(key: key);

  @override
  _PortfolioSelectionDropdownState createState() => _PortfolioSelectionDropdownState();
}

class _PortfolioSelectionDropdownState extends State<PortfolioSelectionDropdown> {
  late TextStyle _appbarFont;
  bool _isAuthenticated = false;
  final _portfolioDao = PortfolioDao();
  final _settingsDao = SettingsDAO();
  TextEditingController _createPortfolioController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _appbarFont = TextStyle(fontSize: 20, fontWeight: FontWeight.w500, letterSpacing: 0.15, color: widget.isDark ? Colors.white : Colors.black);
    _isAuthenticated = _settingsDao.getIsAuthenticated();

    _settingsDao.getIsAuthenticatedWatcher().listen((event) {
      if (this.mounted) {
        setState(() {
          _isAuthenticated = event.value;
        });
      }
    });
    _portfolioDao.getAllPortfoliosWatcher().addListener(() { //refresh list
      if(this.mounted) {
        setState(() {
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: DropdownButton<int>(
        value: widget.selectedPortfolioId,
        icon: Icon(Icons.expand_more),
        style: _appbarFont,
        isExpanded: true,
        underline: Container(
          height: 0,
        ),
        onChanged: (int? newValue) async {
          if (newValue == 0) {
            final int? newPortfolioId = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditPortfolioWidget(selectedPortfolioId: 0)
              )
            );
            if (newPortfolioId != null) {
              widget.onItemTapped(newPortfolioId);
            }
          } else {
            widget.onItemTapped(newValue!);
          }
        },
        items: _buildDropdownList(),
      ),
      data: widget.isDark ? ThemeData.dark() : ThemeData.light(),
    );
  }

  @override
  void dispose() {
    _createPortfolioController.dispose();
    super.dispose();
  }

  ///Returns the list of selectable items of the dropdown menu
  List<DropdownMenuItem<int>> _buildDropdownList() {
    final portfolios = _portfolioDao.getAllPortfolios()..sort(CoinComparators.portfolioNameAsc);
    final defaultPortfolioId = _settingsDao.getDefaultPortfolioIdHive();
    final dropdownList = portfolios.map((e) {
      return DropdownMenuItem<int>(
        value: e.id,
        child: Row(
          children: [
            Expanded(child: Text(e.name, overflow: TextOverflow.ellipsis)),
            (e.id == defaultPortfolioId) ? Icon(Icons.home) : Container()
          ],
        ),
      );
    }).toList();

    if (_isAuthenticated) {
      dropdownList.add(
        DropdownMenuItem<int>(
          value: 0,
          child: Row(
            children: [
              Expanded(child: Text(S.of(context).newPortfolio)),
              Icon(Icons.add),
            ],
          ),
        )
      );
    }

    return dropdownList;
  }
}
