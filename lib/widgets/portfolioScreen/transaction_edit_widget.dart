import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/explorer_transaction_url_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/portfolioScreen/portfolio_selection_dropdown.dart';
import 'package:crypto_prices/widgets/settingsScreens/currency_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/explorer_url_edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';
import '../qr_scanner.dart';
import '../settingsScreens/search_available_coins_delegate.dart';

class TransactionEditWidget extends StatefulWidget {
  ///ID of an existing transaction
  final int transactionId;

  final String coinId;

  ///ID of the currently selected portfolio
  final int selectedPortfolioId;

  TransactionEditWidget({
    Key? key,
    this.transactionId = 0,
    this.coinId = "bitcoin",
    this.selectedPortfolioId = 0
  }) : super(key: key);

  @override
  _TransactionEditWidgetState createState() => _TransactionEditWidgetState();
}

class _TransactionEditWidgetState extends State<TransactionEditWidget> {
  TextStyle _normalFont = TextStyle(fontSize: 16.0);
  TextStyle _biggerFont = TextStyle(fontSize: 18.0);
  TextStyle _biggerFontBold = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  late final _biggerFontRed;
  late final _biggerFontGreen;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _volumeCoinTextController = TextEditingController();
  TextEditingController _volumeCurrencyTextController = TextEditingController();
  TextEditingController _coinPriceTextController = TextEditingController();
  TextEditingController _transactionNameController = TextEditingController();
  TextEditingController _transactionDescriptionController = TextEditingController();
  TextEditingController _transactionFeeCoinController = TextEditingController();
  TextEditingController _transactionFeeCurrencyController = TextEditingController();
  TextEditingController _transactionSourceController = TextEditingController();
  TextEditingController _transactionDestinationController = TextEditingController();
  TextEditingController _transactionIdController = TextEditingController();

  RegExp _signedNumberExp = Constants.signedNumberExp;
  RegExp _positiveNumberExp = Constants.positiveNumberExp;

  int _textFieldMaxLength = 30;

  Transaction _transaction = Transaction(0, "bitcoin", 0);

  int _selectedPortfolioId = 0;

  //don't edit _transaction fields directly so they are reset properly when closing the widget
  String _coinId = "bitcoin";
  String _coinName = "Bitcoin";
  String _coinSymbol = "btc";

  ///The current price in the text field
  double _coinPrice = 0;

  DateTime? _transactionDate;

  String _currency = SettingsDAO().getCurrencyHive();

  TransactionDao _transactionDao = TransactionDao();
  PortfolioDao _portfolioDao = PortfolioDao();

  ExplorerTransactionUrlDao _explorerTransactionDao = ExplorerTransactionUrlDao();

  late Future<double> _getCoinPrice;
  ///Use the price on the transaction date from the API as price
  bool useTransactionDatePrice = true;

  ///Expand the additional options
  bool _showAdditional = false;

  ApiInteraction _api = CoinGeckoAPI();

  ///Is set to prevent the text field where text is currently being entered from updating because the other one changed
  bool _enteringVolume = false;

  ///Price returned from the API. Used as reference if entered price is 0
  double _actualPrice = 1;

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedDark);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenDark);
    } else {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedLight);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenLight);
    }

    _coinPriceTextController.addListener(() { //entered price
      if (_positiveNumberExp.hasMatch(_coinPriceTextController.text)) {
        _coinPrice = _coinPriceTextController.text.parseDouble();
      }
    });
    
    _volumeCoinTextController.addListener(() { //entered coin amount
      if (_volumeCoinTextController.text.isEmpty && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _volumeCurrencyTextController.text = "";
          _enteringVolume = false;
        });
      }
      if (_signedNumberExp.hasMatch(_volumeCoinTextController.text) && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _volumeCurrencyTextController.text = _calculateVolume(_volumeCoinTextController.text.parseDouble(), 0).toStringAsFixed(9);
          _enteringVolume = false;
        });
      }
    });

    _volumeCurrencyTextController.addListener(() { //entered currency amount
      if (_volumeCurrencyTextController.text.isEmpty && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _volumeCoinTextController.text = "";
          _enteringVolume = false;
        });
      }
      if (_signedNumberExp.hasMatch(_volumeCurrencyTextController.text) && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _volumeCoinTextController.text = _calculateVolume(0, _volumeCurrencyTextController.text.parseDouble()).toStringAsFixed(9);
          _enteringVolume = false;
        });
      }
    });

    _transactionFeeCoinController.addListener(() { //entered coin amount
      if (_transactionFeeCoinController.text.isEmpty && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _transactionFeeCurrencyController.text = "";
          _enteringVolume = false;
        });
      }
      if (_signedNumberExp.hasMatch(_transactionFeeCoinController.text) && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _transactionFeeCurrencyController.text = _calculateVolume(_transactionFeeCoinController.text.parseDouble(), 0).toStringAsFixed(9);
          _enteringVolume = false;
        });
      }
    });

    _transactionFeeCurrencyController.addListener(() { //entered currency amount
      if (_transactionFeeCurrencyController.text.isEmpty && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _transactionFeeCoinController.text = "";
          _enteringVolume = false;
        });
      }
      if (_signedNumberExp.hasMatch(_transactionFeeCurrencyController.text) && !_enteringVolume) {
        setState(() {
          _enteringVolume = true;
          _transactionFeeCoinController.text = _calculateVolume(0, _transactionFeeCurrencyController.text.parseDouble()).toStringAsFixed(9);
          _enteringVolume = false;
        });
      }
    });

    _selectedPortfolioId = widget.selectedPortfolioId;

    if (widget.transactionId != 0) {
      _transaction = _transactionDao.getTransaction(widget.transactionId)!;
      _coinId = _transaction.coinId;
      _coinName = _transaction.coinName;
      _coinSymbol = _transaction.coinSymbol;
      _coinPriceTextController.text = _transaction.coinPrice.toString();
      _actualPrice = _transaction.actualCoinPrice;
      _volumeCoinTextController.text = _transaction.coinVolume.toString();
      _transactionDate = _transaction.transactionDate;
      useTransactionDatePrice = false;
      _currency = _transaction.currency;
      _transactionNameController.text = _transaction.transactionName;
      _transactionDescriptionController.text = _transaction.description;
      _transactionSourceController.text = _transaction.sourceAddress;
      _transactionDestinationController.text = _transaction.destinationAddress;
      _transactionIdController.text = _transaction.transactionId;

      if (_transaction.feeCoin == 0) { //leave text field empty
        _transactionFeeCoinController.text = "";
      } else {
        _transactionFeeCoinController.text = _transaction.feeCoin.toString();
      }
    } else {
      if (_selectedPortfolioId != Constants.NODEFAULTPORTFOLIOID) {
        _currency = _portfolioDao.getPortfolio(_selectedPortfolioId)!.currency;
      }
      final coin = AvailableCoinsDao().getAvailableCoin(widget.coinId)!;
      _coinId = widget.coinId;
      _coinName = coin.name;
      _coinSymbol = coin.symbol;
    }

    _getCoinPrice = _api.getCoinPriceAtDate(_coinId, _currency, _transactionDate ?? DateTime.now());
  }

  @override
  void dispose() {
    _volumeCoinTextController.dispose();
    _volumeCurrencyTextController.dispose();
    _coinPriceTextController.dispose();
    _transactionNameController.dispose();
    _transactionDescriptionController.dispose();
    _transactionFeeCoinController.dispose();
    _transactionFeeCurrencyController.dispose();
    _transactionSourceController.dispose();
    _transactionDestinationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).transaction),
        actions: [
          IconButton(
            icon: Icon(Icons.open_in_new),
            tooltip: S.of(context).transactionEditViewInExplorer,
            onPressed: _onExplorerButtonPressed
          ),
          IconButton(
            icon: Icon(Icons.delete_outlined),
            tooltip: S.of(context).deleteTransaction,
            onPressed: _onDeleteButtonPressed
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildCard(
                  title: S.of(context).portfolioTabName,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: PortfolioSelectionDropdown(
                        onItemTapped: _handlePortfolioSelection,
                        selectedPortfolioId: _selectedPortfolioId,
                        isDark: (Theme.of(context).brightness == Brightness.light) ? false : true,
                      ),
                    )
                  ]
                ),
                _buildCard(
                  title: S.of(context).alertCoin,
                  children: [
                    ListTile(
                      title: Text(
                        "$_coinName (${_coinSymbol.toUpperCase()})",
                        style: _biggerFontBold,
                      ),
                      leading: CachedNetworkImage(
                        imageUrl: ImageUrls().getSmallImageUrl(_coinId),
                        placeholder: (context, url) => CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error_outline),
                        width: 25,
                        height: 25,
                      ),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () async {
                        AvailableCoin? result = await showSearch<AvailableCoin?>(
                            context: context,
                            delegate: SearchAvailableCoinsDelegate(context)
                        );
                        if (result != null) { //coin has been selected from search
                          setState(() {
                            _coinId = result.id;
                            _coinName = result.name;
                            _coinSymbol = result.symbol;
                            _getCoinPrice = _api.getCoinPriceAtDate(
                                _coinId,
                                _currency,
                                _transactionDate ?? DateTime.now()
                            ).then((price) => _actualPrice = price);
                          });
                        }
                      }
                    ),
                  ]
                ),
                _buildCard(
                  title: S.of(context).alertCurrency,
                  children: [
                    ListTile(
                      title: Text(
                        _currency.toUpperCase(),
                        style: _biggerFontBold,
                      ),
                      leading: Image(
                        image: AssetImage("assets/images/currencies/$_currency.png"),
                        width: 25,
                        height: 25,
                      ),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () {
                        Navigator.of(context).push(Util.createSlidingRoute(CurrencyWidget(
                          onCurrencyChange: _onCurrencySelected,
                          setCurrency: _currency
                        )));
                      }
                    ),
                  ]
                ),
                _buildCard(
                  title: S.of(context).transactionEditCoinPrice,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                      child: (useTransactionDatePrice
                        ? FutureBuilder<double>(
                            future: _getCoinPrice,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState != ConnectionState.done) {
                                return Align(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              }
                              if (snapshot.hasData) {
                                if (snapshot.data! == -1) {
                                  _coinPrice = 0;
                                  return Container(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                                    child: Text(S.of(context).transactionEditNoDataForDate, style: _biggerFont,)
                                  );
                                }

                                _coinPrice = snapshot.data!;
                                _actualPrice = snapshot.data!;
                                return TextField(
                                  controller: _coinPriceTextController..text = _coinPrice.toString(),
                                  enabled: false,
                                  keyboardType: TextInputType.numberWithOptions(
                                      signed: true,
                                      decimal: true
                                  ),
                                  maxLength: _textFieldMaxLength,
                                  decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                                      border: OutlineInputBorder(),
                                      hintText: S.of(context).transactionEditCoinPriceHint,
                                      errorStyle: TextStyle(
                                        fontSize: 14,
                                        color: (currentTheme.isThemeDark())
                                            ? Constants.errorColorDark
                                            : Constants.errorColor,
                                      ),
                                      suffix: Text(Constants.currencySymbols[_currency]!)
                                  ),
                                );
                              } else if (snapshot.hasError) {
                                if (snapshot.error is SocketException) {
                                  return _buildErrorRow(S.of(context).errorMessageSocket);
                                }

                                if (snapshot.error is HttpException) {
                                  return _buildErrorRow(S.of(context).errorMessageHttp);
                                }

                                if (snapshot.error is RateLimitationException) {
                                  return _buildErrorRow(S.of(context).errorMessageRateLimitationShort);
                                }

                                return _buildErrorRow(S.of(context).error + ": " + snapshot.error.toString());
                              }

                              return Align(
                                alignment: Alignment.center,
                                child: CircularProgressIndicator(),
                              );
                            },
                        )
                      : TextFormField(
                          controller: _coinPriceTextController,
                          keyboardType: TextInputType.numberWithOptions(
                              signed: true,
                              decimal: true
                          ),
                          maxLength: _textFieldMaxLength,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                              border: OutlineInputBorder(),
                              hintText: S.of(context).transactionEditCoinPriceHint,
                              errorStyle: TextStyle(
                                fontSize: 14,
                                color: (currentTheme.isThemeDark())
                                    ? Constants.errorColorDark
                                    : Constants.errorColor,
                              ),
                              suffix: Text(Constants.currencySymbols[_currency]!)
                          ),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (!_positiveNumberExp.hasMatch(value!)) {
                              return S.of(context).alertErrorText;
                            }
                            return null;
                          },
                      ))
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                      child: Row(
                        children: [
                          Checkbox(
                            fillColor: MaterialStateProperty.resolveWith((states) => Theme.of(context).colorScheme.secondary),
                            checkColor: (currentTheme.isThemeDark() ? Colors.black : Colors.white),
                            value: useTransactionDatePrice,
                            onChanged: (bool? value) {
                              setState(() {
                                useTransactionDatePrice = value!;
                                if (useTransactionDatePrice) {
                                  _getCoinPrice = _api.getCoinPriceAtDate(
                                      _coinId,
                                      _currency,
                                      _transactionDate ?? DateTime.now()
                                  );
                                } else {
                                  if (_coinPriceTextController.text.isEmpty) {
                                    _coinPrice = 0;
                                  } else {
                                    _coinPrice = _coinPriceTextController.text.parseDouble();
                                  }
                                }
                              });
                            }
                          ),
                          Text(
                              S.of(context).transactionEditPriceFromDate,
                            style: _normalFont
                          )
                        ],
                      ),
                    )
                  ]
                ),
                _buildCard(
                  title: S.of(context).transactionEditTransactionVolume,
                  children: [
                    _buildCoinCurrencyEdit(_volumeCoinTextController, _volumeCurrencyTextController)
                  ]
                ),
                _buildCard(
                  title: S.of(context).transactionEditTransactionDate,
                  children: [
                    ListTile(
                      title: Text(
                        (_transactionDate ?? DateTime.now()).getDateOnly(),
                        style: _biggerFontBold,
                      ),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () {
                        _selectDate();
                      }
                    ),
                  ]
                ),
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text(S.of(context).transactionEditMoreOptions),
                        trailing: (_showAdditional ? Icon(Icons.keyboard_arrow_up) : Icon(Icons.keyboard_arrow_down)),
                        onTap: () {
                          setState(() {
                            _showAdditional = !_showAdditional;
                          });
                        },
                      ),
                      AnimatedCrossFade(
                        firstChild: Row(),
                        secondChild: _buildAdditionalOptions(),
                        crossFadeState: _showAdditional
                            ? CrossFadeState.showSecond
                            : CrossFadeState.showFirst,
                        duration: Duration(milliseconds: 200)
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 72,
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: _validateForm
      ),
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children, EdgeInsets textPadding = const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0)}) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: textPadding,
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Builds a column with the additional options like name and description
  Widget _buildAdditionalOptions() {
    return Container(
      child: Column(
        children: [
          _buildCard(
            title: S.of(context).transactionEditTransactionName,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: TextField(
                  controller: _transactionNameController,
                  maxLines: 1,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                    border: OutlineInputBorder(),
                    hintText: S.of(context).transactionEditTransactionNameHint,
                    errorStyle: TextStyle(
                      fontSize: 14,
                      color: (currentTheme.isThemeDark())
                          ? Constants.errorColorDark
                          : Constants.errorColor,
                    ),
                  ),
                ),
              )
            ]
          ),
          _buildCard(
            title: S.of(context).transactionEditDescription,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: TextField(
                  controller: _transactionDescriptionController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                    border: OutlineInputBorder(),
                    hintText: S.of(context).transactionEditDescriptionHint,
                    errorStyle: TextStyle(
                      fontSize: 14,
                      color: (currentTheme.isThemeDark())
                          ? Constants.errorColorDark
                          : Constants.errorColor,
                    ),
                  ),
                ),
              )
            ]
          ),
          _buildCard(
            title: S.of(context).transactionEditFee,
            children: [
              _buildCoinCurrencyEdit(
                _transactionFeeCoinController,
                _transactionFeeCurrencyController,
                allowNegatives: false,
                allowEmpty: true
              )
            ]
          ),
          _buildCard(
            title: S.of(context).transactionEditSourceAddress,
            children: [
              _buildAddressField(_transactionSourceController, S.of(context).transactionEditSourceAddressHint)
            ]
          ),
          _buildCard(
            title: S.of(context).transactionEditDestinationAddress,
            children: [
              _buildAddressField(_transactionDestinationController, S.of(context).transactionEditDestinationAddressHint)
            ]
          ),
          _buildCard(
            title: S.of(context).transactionEditTransactionId,
            children: [
              _buildAddressField(_transactionIdController, S.of(context).transactionEditTransactionIdHint)
            ]
          )
        ],
      ),
    );
  }

  ///Returns two text fields for coin and currency amount
  ///and a button between to swap the text in them.
  ///The text fields accept only valid numbers.
  ///If allowNegatives is false, negative numbers are not allowed
  ///if allowEmpty is true, the text field can be empty on validation
  ///(for non mandatory fields)
  Widget _buildCoinCurrencyEdit(
    TextEditingController coinAmountController,
    TextEditingController currencyAmountController,
    {
      bool allowNegatives = true,
      bool allowEmpty = false
    }
  ) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: TextFormField(
            controller: coinAmountController,
            keyboardType: TextInputType.numberWithOptions(
                signed: true,
                decimal: true
            ),
            maxLength: _textFieldMaxLength,
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
              border: OutlineInputBorder(),
              hintText: S.of(context).transactionEditCoinAmountHint,
              errorStyle: TextStyle(
                fontSize: 14,
                color: (currentTheme.isThemeDark())
                    ? Constants.errorColorDark
                    : Constants.errorColor,
              ),
              suffixIcon: Padding(padding: EdgeInsets.all(20), child: Text(_coinSymbol.toUpperCase())),
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) {
              if (allowEmpty) {
                if (value == null || value.isEmpty) return null;
              }

              RegExp validPattern = RegExp("");
              if (allowNegatives) {
                validPattern = _signedNumberExp;
              } else {
                validPattern = _positiveNumberExp;
              }

              if (!validPattern.hasMatch(value!) || value.parseDouble() == 0) {
                return S.of(context).alertErrorText;
              }
              return null;
            },
          ),
        ),
        Center(
          child: IconButton(
            icon: Icon(Icons.import_export),
            iconSize: 35,
            onPressed: () {
              _onSwapVolumesTapped(coinAmountController, currencyAmountController);
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(16),
          child: TextFormField(
            controller: currencyAmountController,
            keyboardType: TextInputType.numberWithOptions(
                signed: true,
                decimal: true
            ),
            maxLength: _textFieldMaxLength,
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
              border: OutlineInputBorder(),
              hintText: S.of(context).transactionEditCurrencyAmountHint,
              errorStyle: TextStyle(
                fontSize: 14,
                color: (currentTheme.isThemeDark())
                    ? Constants.errorColorDark
                    : Constants.errorColor,
              ),
              suffixIcon: Padding(padding: EdgeInsets.all(20), child: Text(Constants.currencySymbols[_currency]!))
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) {
              if (allowEmpty) {
                if (value == null || value.isEmpty) return null;
              }

              RegExp validPattern = RegExp("");
              if (allowNegatives) {
                validPattern = _signedNumberExp;
              } else {
                validPattern = _positiveNumberExp;
              }

              if (!validPattern.hasMatch(value!) || value.parseDouble() == 0) {
                return S.of(context).alertErrorText;
              }
              return null;
            },
          ),
        )
      ],
    );
  }

  ///Returns a text field with a QR-Code button to enter coin addresses
  Widget _buildAddressField(TextEditingController controller, String hintText) {
    return Container(
      padding: EdgeInsets.all(16),
      child: TextField(
        controller: controller,
        maxLines: 1,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
          border: OutlineInputBorder(),
          hintText: hintText,
          errorStyle: TextStyle(
            fontSize: 14,
            color: (currentTheme.isThemeDark())
                ? Constants.errorColorDark
                : Constants.errorColor,
          ),
          suffixIcon: IconButton(
            icon: Icon(Icons.qr_code),
            tooltip: S.of(context).transactionEditQrCodeTooltip,
            onPressed: () async {
              final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => QrScanner()
                  )
              );
              if (result != null) {
                controller.text = result;
              }
            },
          )
        ),
      ),
    );
  }
  
  ///Builds a row to display the error message and refresh button
  Widget _buildErrorRow(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(text, style: _normalFont),
        IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {
            _refresh();
          },
        )
      ],
    );
  }
  
  ///Refreshes the coin price
  Future _refresh() async {
    setState(() {
      _getCoinPrice = _api.getCoinPriceAtDate(
        _coinId,
        _currency,
        _transactionDate ?? DateTime.now()
      );
    });
  }

  ///Changes the selected portfolio when one is selected from the menu
  void _handlePortfolioSelection(int newPortfolioId) {
    setState(() {
      _selectedPortfolioId = newPortfolioId;
      _onCurrencySelected(_portfolioDao.getPortfolio(_selectedPortfolioId)!.currency);
    });
  }

  ///Opens an alert dialog to confirm deletion of the selected transaction.
  ///If no transaction is selected, close the widget instead
  void _onDeleteButtonPressed() {
    if (widget.transactionId == 0) {
      Navigator.pop(context);
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).deleteTransaction),
            content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(S.of(context).alertDialogCancel)
              ),
              TextButton(
                  onPressed: () {
                    _transactionDao.deleteTransaction(widget.transactionId);
                    Navigator.pop(context); //pop dialog
                    Navigator.pop(context, true); //pop widget
                  },
                  child: Text(S.of(context).alertDialogDelete)
              ),
            ],
          );
        }
      );
    }
  }

  ///Open the transaction in a block explorer, if one is set
  void _onExplorerButtonPressed() {
    if (_transactionIdController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(S.of(context).transactionEditNoTransactionId),
          duration: Duration(seconds: 2),
        )
      );
      return;
    }
    ExplorerTransactionUrl? explorerTransactionUrl = _explorerTransactionDao.getCoinExplorerTransactionUrl(_coinId);
    if (explorerTransactionUrl == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(S.of(context).transactionEditNoExplorerUrl),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label: S.of(context).transactionEditSetUrl,
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ExplorerUrlEdit(coinId: _coinId)));
            }
          ),
        )
      );
      return;
    }
    _launchUrl(explorerTransactionUrl.url + _transactionIdController.text);
  }

  ///Launches an url in the browser
  void _launchUrl(String url) async =>
    await canLaunch(url)
        ? await launch(url)
        : showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).urlLaunchAlertDialogTitle,),
            content: Text(S.of(context).urlLaunchAlertDialogMessage),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(S.of(context).alertDialogClose)
              )
            ],
          );
        }
    );

  ///Changes the selected currency to the new one selected in the CurrencyWidget
  void _onCurrencySelected(String newValue) {
    setState(() {
      _currency = newValue;
      _getCoinPrice = _api.getCoinPriceAtDate(
          _coinId,
          _currency,
          _transactionDate ?? DateTime.now()
      ).then((price) => _actualPrice = price);
    });
  }

  ///Returns how much an amount of coins is worth in currency and vice versa.
  ///If both are not 0 the amount of currency from coins is returned
  double _calculateVolume(double coinAmount, double currencyAmount) {
    double currentPrice = 0;
    if (_coinPrice == 0) { //if coin price is 0 use 1 to prevent calculation errors
      currentPrice = 1;
    } else {
      currentPrice = _coinPrice;
    }
    if (coinAmount == 0) { //entering currency
      return currencyAmount / currentPrice;
    } else { //entering coin
      return coinAmount * currentPrice;
    }
  }

  ///Swaps the value entered in the two given text fields
  void _onSwapVolumesTapped(TextEditingController firstController, TextEditingController secondController) {
    String temp = firstController.text;
    _enteringVolume = true;
    firstController.text = secondController.text;
    secondController.text = temp;
    _enteringVolume = false;
  }

  ///Opens a date picker dialog to select the transaction date
  void _selectDate() async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: _transactionDate ?? DateTime.now(),
        firstDate: DateTime(2009),
        lastDate: DateTime.now()
    );
    if (picked != null && picked != _transactionDate) {
      setState(() {
        _transactionDate = picked;
        _getCoinPrice = _api.getCoinPriceAtDate(
            _coinId,
            _currency,
            _transactionDate ?? DateTime.now()
        ).then((price) => _actualPrice = price);
      });
    }
  }

  ///Checks if the Form is valid
  void _validateForm() {
    if (_formKey.currentState!.validate()) {
      _transaction.coinId = _coinId;
      _transaction.coinName = _coinName;
      _transaction.coinSymbol = _coinSymbol;
      _transaction.coinPrice = _coinPrice;
      _transaction.actualCoinPrice = _actualPrice;
      _transaction.currency = _currency;
      _transaction.coinVolume = _volumeCoinTextController.text.parseDouble();
      _transaction.currencyVolume = _volumeCurrencyTextController.text.parseDouble();
      _transaction.transactionDate = _transactionDate ?? DateTime.now();

      _transaction.transactionName = _transactionNameController.text;
      _transaction.description = _transactionDescriptionController.text;
      _transaction.sourceAddress = _transactionSourceController.text;
      _transaction.destinationAddress = _transactionDestinationController.text;
      _transaction.transactionId = _transactionIdController.text;

      if (_transactionFeeCoinController.text.isEmpty || _transactionFeeCurrencyController.text.isEmpty) {
        _transaction.feeCoin = 0;
        _transaction.feeCurrency = 0;
      } else {
        _transaction.feeCoin = _transactionFeeCoinController.text.parseDouble();
        _transaction.feeCurrency = _transactionFeeCurrencyController.text.parseDouble();
      }

      //save transaction
      print(_transaction.id);
      print(_transaction.coinId);
      print(_transaction.coinName);
      print(_transaction.coinSymbol);
      print(_transaction.coinVolume);
      print(_transaction.currencyVolume);
      print(_transaction.coinPrice);
      print(_transaction.transactionDate?.toIso8601String());
      print(_transaction.transactionName);
      print(_transaction.description);
      print(_transaction.sourceAddress);
      print(_transaction.destinationAddress);
      print(_transaction.transactionId);

      if (_selectedPortfolioId != 0) {
        if (_selectedPortfolioId != widget.selectedPortfolioId) { //remove from old portfolio
          _transactionDao.deleteTransaction(_transaction.id);
        }
        _portfolioDao.getPortfolio(_selectedPortfolioId)!.insertTransaction(_transaction);
        Navigator.pop(context, true);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).transactionEditSelectPortfolio)),
        );
      }
    }
  }
}