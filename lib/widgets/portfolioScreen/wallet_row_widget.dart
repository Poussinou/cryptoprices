import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/detailScreen/detail_screen_base.dart';
import 'package:crypto_prices/widgets/detailScreen/wallet_detail_screen.dart';
import 'package:crypto_prices/widgets/private_price_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../constants.dart';

///Displays the given Wallet information in a row.
///If openOnTap contains a route name, this widget is opened when tapping the row.
class WalletRowWidget extends StatefulWidget {
  ///Name of a route that is pushed when the widget is tapped
  final String openOnTap;

  ///An index number that is displayed at the start od the row
  final String index;

  ///The id of the data that is displayed on the row
  final int walletId;

  WalletRowWidget({
    Key? key,
    this.openOnTap = "",
    this.index = "",
    required this.walletId,
  }) : super(key: key);

  @override
  _WalletRowWidgetState createState() => _WalletRowWidgetState();
}

class _WalletRowWidgetState extends State<WalletRowWidget> {
  final _boldFont = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  final _smallerFont = TextStyle(fontSize: 14.0);
  late final _smallerFontRed;
  late final _smallerFontGreen;
  final _normalFont = TextStyle(fontSize: 16.0);

  late Widget _openOnTapWidget;

  String _currency = SettingsDAO().getCurrencyHive();
  WalletDao _walletDao = WalletDao();
  PortfolioDao _portfolioDao = PortfolioDao();

  String _imageUrl = "";

  late Wallet _wallet;

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _smallerFontRed = TextStyle(fontSize: 14.0, color: Constants.priceChangeRedDark);
      _smallerFontGreen = TextStyle(fontSize: 14.0, color: Constants.priceChangeGreenDark);
    } else {
      _smallerFontRed = TextStyle(fontSize: 14.0, color: Constants.priceChangeRedLight);
      _smallerFontGreen = TextStyle(fontSize: 14.0, color: Constants.priceChangeGreenLight);
    }

    _wallet = _walletDao.getWallet(widget.walletId)!;

    _currency = _portfolioDao.getPortfolio(_wallet.connectedPortfolioId)!.currency;

    _portfolioDao.getPortfolioWatcher(_wallet.connectedPortfolioId).addListener(_portfolioUpdateListener);

    _imageUrl = ImageUrls().getLargeImageUrl(_wallet.coinId);

    if (widget.openOnTap.isNotEmpty) {
      switch (widget.openOnTap) {
        case WalletDetailScreen.routeName: {
          _openOnTapWidget = DetailScreenBase(coinId: _wallet.coinId, selectedPortfolioId: _wallet.connectedPortfolioId, showWallet: true);
          break;
        }

        default: {
          _openOnTapWidget = Row();
          break;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          if (widget.openOnTap.isNotEmpty) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => _openOnTapWidget
                )
            );
          }
        },
        onLongPress: () {
          if (SettingsDAO().getPrivateMode()) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).notAvailableInPrivateMode),
                duration: Duration(seconds: 2),
              ),
            );
          } else {
            Clipboard.setData(ClipboardData(text: _wallet.totalCoinAmount.toString()));
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).coinAmountCopied(_wallet.coinSymbol.toUpperCase())),
                duration: Duration(seconds: 2),
              ),
            );
          }
        },
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(4.0, 0, 4.0, 0),
                child: Text(
                  widget.index.toString(),
                  style: _smallerFont,
                )
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: CachedNetworkImage(
                imageUrl: _imageUrl,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error_outline),
                height: 40.0,
                width: 40.0,
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              _wallet.coinName,
                              style: _boldFont
                          ),
                          SizedBox(height: 10),
                          Text(
                              _wallet.coinSymbol.toUpperCase(),
                              style: _smallerFont
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        PrivatePriceText(
                          price: Util.formatPrice(_wallet.totalCoinAmount),
                          symbol: _wallet.coinSymbol.toUpperCase(),
                          style: _normalFont,
                        ),
                        SizedBox(height: 10),
                        PrivatePriceText(
                          price: Util.formatPrice(_wallet.totalValue, twoDecimalsAbove: 1),
                          symbol: Constants.currencySymbols[_currency]!,
                          style: _normalFont,
                        ),
                      ],
                    ),
                  )
                ],
              )
            ),
          ],
        ),
      )
    );
  }

  ///Listener function that is triggered by the portfolio listener
  void _portfolioUpdateListener() {
    if (this.mounted) {
      setState(() {
        if (_portfolioDao.getPortfolio(_wallet.connectedPortfolioId) != null) { //don't call after deleting portfolio
          _currency = _portfolioDao.getPortfolio(_wallet.connectedPortfolioId)!.currency;
        }
      });
    }
  }
}