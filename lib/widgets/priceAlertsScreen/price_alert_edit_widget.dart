import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/util/workmanager_handler.dart';
import 'package:crypto_prices/widgets/settingsScreens/currency_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../constants.dart';
import '../settingsScreens/search_available_coins_delegate.dart';

///Used to create and edit price alert
class PriceAlertEditWidget extends StatefulWidget {
  static const routeName = "/priceAlertEdit";

  ///ID of an existing alert
  final int alertId;

  final String coinId;

  final String coinName;

  final String coinSymbol;

  PriceAlertEditWidget({
    Key? key,
    this.alertId = 0,
    this.coinId = "bitcoin",
    this.coinName = "Bitcoin",
    this.coinSymbol = "BTC"
  });

  @override
  _PriceAlertEditWidgetState createState() => _PriceAlertEditWidgetState();
}

class _PriceAlertEditWidgetState extends State<PriceAlertEditWidget> {

  String _coinId = "";
  String _coinName = "";
  String _coinSymbol = "";
  String _currency = SettingsDAO().getCurrencyHive();

  List<bool> _valueTypeSelected = [true, false];
  TargetValueType _targetValueType = TargetValueType.price;

  AlertFrequency _frequency = AlertFrequency.once;
  PriceMovement _movement = PriceMovement.above;
  double _currentCoinPrice = 0;
  double _currentChangePercentage = 0;
  late Future<Map<String, double>> _getCoinPrice;

  TextStyle _normalFont = TextStyle(fontSize: 16.0);
  TextStyle _biggerFont = TextStyle(fontSize: 18.0);
  TextStyle _biggerFontBold = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  late final _biggerFontRed;
  late final _biggerFontGreen;

  TextEditingController _textController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  int _textFieldMaxLength = 20;

  ApiInteraction api = CoinGeckoAPI();
  PriceAlertDao _priceAlertDao = PriceAlertDao();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedDark);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenDark);
    } else {
      _biggerFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedLight);
      _biggerFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenLight);
    }

    _coinId = widget.coinId;
    _coinName = widget.coinName;
    _coinSymbol = widget.coinSymbol;

    //load existing alert
    if (widget.alertId != 0) {
      PriceAlert? alert = _priceAlertDao.getPriceAlert(widget.alertId);
      if (alert == null) { //alert was deleted but UI has not been updated
        Navigator.pop(context);
      } else {
        _coinId = alert.coinId;
        _coinName = alert.coinName;
        _coinSymbol = alert.coinSymbol;
        _currency = alert.currency;
        _textController.text = alert.targetValue.toString();
        _targetValueType = alert.targetValueType;
        _toggleButtons();
        _movement = alert.priceMovement;
        _frequency = alert.alertFrequency;
      }
    }

    _getCoinPrice = api.getCoinPrice(_coinId, _currency, changePercentage: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).alertTitle),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_outlined),
            tooltip: S.of(context).alertDeleteTooltip,
            onPressed: _onDeleteButtonPressed
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildCard(
                    title: S.of(context).alertCoin,
                    children: [
                      ListTile(
                        title: Text(
                          "$_coinName (${_coinSymbol.toUpperCase()})",
                          style: _biggerFontBold,
                        ),
                        leading: CachedNetworkImage(
                          imageUrl: ImageUrls().getSmallImageUrl(_coinId),
                          placeholder: (context, url) => CircularProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.error_outline),
                          width: 25,
                          height: 25,
                        ),
                        trailing: Icon(Icons.chevron_right),
                        onTap: () async {
                          AvailableCoin? result = await showSearch<AvailableCoin?>(
                              context: context,
                              delegate: SearchAvailableCoinsDelegate(context)
                          );
                          if (result != null) { //coin has been selected from search
                            setState(() {
                              _coinId = result.id;
                              _coinName = result.name;
                              _coinSymbol = result.symbol;
                              _getCoinPrice = api.getCoinPrice(_coinId, _currency, changePercentage: true);
                            });
                          }
                        }
                      ),
                    ]
                ),
                _buildCard(
                    title: S.of(context).alertCurrency,
                    children: [
                      ListTile(
                        title: Text(
                          _currency.toUpperCase(),
                          style: _biggerFontBold,
                        ),
                        leading: Image(
                          image: AssetImage("assets/images/currencies/$_currency.png"),
                          width: 25,
                          height: 25,
                        ),
                        trailing: Icon(Icons.chevron_right),
                        onTap: () {
                          Navigator.of(context).push(Util.createSlidingRoute(CurrencyWidget(
                            onCurrencyChange: _onCurrencySelected,
                            setCurrency: _currency
                          )));
                        }
                      ),
                    ]
                ),
                _buildCard(
                    title: S.of(context).alertCurrentPrice,
                    children: [
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: FutureBuilder<Map<String, double>>(
                          future: _getCoinPrice,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState != ConnectionState.done) {
                              return Align(
                                alignment: Alignment.center,
                                child: CircularProgressIndicator(),
                              );
                            }
                            if (snapshot.hasData) {
                              _currentCoinPrice = snapshot.data![Constants.APIGETCOINPRICEKEY]!;
                              _currentChangePercentage = snapshot.data![Constants.APIGETCOINCHANGEPERCENTAGEKEY]!;
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${Util.formatPrice(_currentCoinPrice)} ${Constants.currencySymbols[_currency]}",
                                    style: _biggerFont,
                                  ),
                                  Text(
                                    (Util.formatPercentage(_currentChangePercentage) + "%"),
                                    style: (_currentChangePercentage.isNegative ? _biggerFontRed : _biggerFontGreen)
                                  )
                                ],
                              );
                            } else if (snapshot.hasError) {
                              if (snapshot.error is SocketException) {
                                return _buildErrorRow(S.of(context).errorMessageSocket);
                              }

                              if (snapshot.error is HttpException) {
                                return _buildErrorRow(S.of(context).errorMessageHttp);
                              }

                              if (snapshot.error is RateLimitationException) {
                                return _buildErrorRow(S.of(context).errorMessageRateLimitationShort);
                              }

                              return _buildErrorRow(S.of(context).error + ": " + snapshot.error.toString());
                            }

                            return Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(),
                            );
                          },
                        )
                      )
                    ]
                ),
                _buildCard(
                    title: S.of(context).alertTargetValue,
                    children: [
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: TextFormField(
                          controller: _textController,
                          keyboardType: TextInputType.numberWithOptions(
                              signed: true,
                              decimal: true
                          ),
                          maxLength: _textFieldMaxLength,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                              border: OutlineInputBorder(),
                              hintText: S.of(context).alertHintText,
                              errorStyle: TextStyle(
                                fontSize: 14,
                                color: (currentTheme.isThemeDark())
                                    ? Constants.errorColorDark
                                    : Constants.errorColor,
                              ),
                              suffix: (_targetValueType == TargetValueType.price)
                                  ? Text(Constants.currencySymbols[_currency]!)
                                  : Text("%")
                          ),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: _validateInput,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 16.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: ToggleButtons(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    S.of(context).alertPrice,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                                Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      S.of(context).alertChange,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                              ],
                              fillColor: Theme.of(context).colorScheme.secondary.withOpacity(0.2),
                              selectedColor: Theme.of(context).textTheme.bodyText2!.color,
                              onPressed: _onValueTypeSelectionButtonPressed,
                              isSelected: _valueTypeSelected
                          ),
                        ),
                      ),
                    ]
                ),
                _buildCard(
                    title: S.of(context).alertPriceMovement,
                    children: [
                      RadioListTile(
                        title: Text(S.of(context).alertAbove),
                        activeColor: (currentTheme.isThemeDark() ? Constants
                            .accentColorDark : Constants.accentColorLight),
                        value: PriceMovement.above,
                        groupValue: _movement,
                        onChanged: (PriceMovement? value) {
                          setState(() {
                            _movement = value!;
                          });
                        },
                      ),
                      RadioListTile(
                        title: Text(S.of(context).alertEqual),
                        activeColor: (currentTheme.isThemeDark() ? Constants
                            .accentColorDark : Constants.accentColorLight),
                        value: PriceMovement.equal,
                        groupValue: _movement,
                        onChanged: (PriceMovement? value) {
                          setState(() {
                            _movement = value!;
                          });
                        },
                      ),
                      RadioListTile(
                        title: Text(S.of(context).alertBelow),
                        activeColor: (currentTheme.isThemeDark() ? Constants
                            .accentColorDark : Constants.accentColorLight),
                        value: PriceMovement.below,
                        groupValue: _movement,
                        onChanged: (PriceMovement? value) {
                          setState(() {
                            _movement = value!;
                          });
                        },
                      ),
                    ]
                ),
                _buildCard(
                    title: S.of(context).alertFrequency,
                    children: [
                      RadioListTile(
                        title: Text(S.of(context).alertOnce),
                        activeColor: (currentTheme.isThemeDark() ? Constants
                            .accentColorDark : Constants.accentColorLight),
                        value: AlertFrequency.once,
                        groupValue: _frequency,
                        onChanged: (AlertFrequency? value) {
                          setState(() {
                            _frequency = value!;
                          });
                        },
                      ),
                      RadioListTile(
                        title: Text(S.of(context).alertRepeat),
                        activeColor: (currentTheme.isThemeDark() ? Constants
                            .accentColorDark : Constants.accentColorLight),
                        value: AlertFrequency.repeating,
                        groupValue: _frequency,
                        onChanged: (AlertFrequency? value) {
                          setState(() {
                            _frequency = value!;
                          });
                        },
                      )
                    ]
                ),
                SizedBox(
                  height: 72,
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: _validateForm
      ),
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children}) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0),
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Builds a row to display the error message and refresh button
  Widget _buildErrorRow(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(text, style: _normalFont),
        IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {
            _refresh();
          },
        )
      ],
    );
  }

  ///Refreshes the coin price
  Future _refresh() async {
    setState(() {
      setState(() {
        _getCoinPrice = api.getCoinPrice(_coinId, _currency, changePercentage: true);
      });
    });
  }

  ///Changes the selected target value type based on which toggle button is pressed
  void _onValueTypeSelectionButtonPressed(int buttonIndex) {
    setState(() {
      switch (buttonIndex) {
        case 0: {
          _targetValueType = TargetValueType.price;
          break;
        }

        case 1: {
          _targetValueType = TargetValueType.percentage;
          break;
        }
      }

      for (int i = 0; i < _valueTypeSelected.length; i++) {
        if (i == buttonIndex) {
          _valueTypeSelected[i] = true;
        } else {
          _valueTypeSelected[i] = false;
        }
      }
    });
  }

  ///Changes the selected currency to the new one selected in the CurrencyWidget
  void _onCurrencySelected(String newValue) {
    setState(() {
      _currency = newValue;
      _getCoinPrice = api.getCoinPrice(_coinId, _currency, changePercentage: true);
    });
  }

  ///Opens an alert dialog to confirm deletion of the selected alert.
  ///If no alert is selected, close the widget instead
  void _onDeleteButtonPressed() {
    if (widget.alertId == 0) {
      Navigator.pop(context);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(S.of(context).alertDialogDeletePriceAlertTitle),
              content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(S.of(context).alertDialogCancel)
                ),
                TextButton(
                    onPressed: () {
                      WorkmanagerHandler().deleteAlert(widget.alertId);
                      Navigator.pop(context); //pop dialog
                      Navigator.pop(context); //pop widget
                    },
                    child: Text(S.of(context).alertDialogDelete)
                ),
              ],
            );
          }
      );
    }
  }

  ///Toggles the buttons according to the set targetValueType.
  ///Used when loading an existing alert
  void _toggleButtons() {
    var buttonIndex;
    switch (_targetValueType) {
      case TargetValueType.price: {
        buttonIndex = 0;
        break;
      }

      case TargetValueType.percentage: {
        buttonIndex = 1;
        break;
      }
    }

    for (int i = 0; i < _valueTypeSelected.length; i++) {
      if (i == buttonIndex) {
        _valueTypeSelected[i] = true;
      } else {
        _valueTypeSelected[i] = false;
      }
    }
  }

  ///Validator function for the TextInputForm. Checks if a valid number is entered
  ///only (signed) int or double (. and ,) without thousand separator eg. 1; 1.2; -1,0
  String? _validateInput(String? inputNumber) {
    RegExp exp;

    if (_targetValueType == TargetValueType.percentage) {
      //allow negative values
      exp = Constants.signedNumberExp;
    } else {
      exp = Constants.positiveNumberExp;
    }

    if (!exp.hasMatch(_textController.text)) {
      return S.of(context).alertErrorText;
    }
    return null;
  }

  ///Checks if the Form is valid
  void _validateForm() {
    if (_formKey.currentState!.validate()) {
      double currentTargetValue;
      if (_targetValueType == TargetValueType.percentage) {
        currentTargetValue = _currentChangePercentage;
      } else {
        currentTargetValue = _currentCoinPrice;
      }

      PriceAlert alert = PriceAlert(
        id: widget.alertId,
        coinId: _coinId,
        coinName: _coinName,
        coinSymbol: _coinSymbol,
        currency: _currency,
        targetValue: _textController.text.parseDouble(),
        targetValueType: _targetValueType,
        priceMovement: _movement,
        alertFrequency: _frequency,
        dateAdded: DateTime.now(),
        lastTargetValue: currentTargetValue
      );

      WorkmanagerHandler().registerPriceAlert(alert);

      Navigator.pop(context);
    }
  }

}