import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/material.dart';

///Censors the displayed price when private mode is enabled
class PrivatePriceText extends StatefulWidget {
  final String price;

  final String symbol;

  ///Is the displayed price a percentage
  final bool percentage;

  final TextStyle? style;

  const PrivatePriceText({
    Key? key,
    required this.price,
    required this.symbol,
    this.percentage = false,
    this.style
  }) : super(key: key);

  @override
  _PrivatePriceTextState createState() => _PrivatePriceTextState();
}

class _PrivatePriceTextState extends State<PrivatePriceText> {
  bool _privateMode = SettingsDAO().getPrivateMode();

  @override
  void initState() {
    super.initState();

    SettingsDAO().getPrivateModeWatcher().listen((event) {
      if (this.mounted) {
        setState(() {
          _privateMode = event.value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      _getText(),
      style: (widget.style != null) ? widget.style : TextStyle()
    );
  }

  ///Returns the censored or uncensored text
  String _getText() {
    if (_privateMode) {
      if (widget.percentage) {
        return "*****${widget.symbol}";
      }
      return "***** ${widget.symbol}";
    } else {
      if (widget.percentage) {
        return widget.price + widget.symbol;
      }
      return widget.price + " " + widget.symbol;
    }
  }
}
