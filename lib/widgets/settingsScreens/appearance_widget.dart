import 'package:crypto_prices/changeNotifiers/theme_change.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/main.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/date_format_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/number_format_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import '../../constants.dart';

///Displays the appearance settings
class AppearanceWidget extends StatefulWidget {

  @override
  State<AppearanceWidget> createState() => _AppearanceWidgetState();
}

class _AppearanceWidgetState extends State<AppearanceWidget> {
  Color pickerColor = colorChange.currentPrimaryColor();
  Color currentColor = colorChange.currentPrimaryColor();

  Themes _theme = currentTheme.currentTheme();

  int _sdkNumber = androidDeviceInfo?.version.sdkInt ?? 28;

  String _dateFormatLocale = SettingsDAO().getDateFormatLocaleHive();
  String _numberFormatLocale = SettingsDAO().getNumberFormatLocaleHive();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(S.of(context).settingsAppearance)
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            dense: true,
            title: Text(
              S.of(context).settingsAppearanceApp,
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
                fontSize: 15,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          ListTile(
            title: Text(S.of(context).settingsAppearanceColor),
            subtitle: Text(S.of(context).settingsAppearanceColorSubtitle),
            leading: Icon(Icons.palette),
            onTap: () {
              _showPrimaryColorDialog();
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsAppearanceTheme),
            subtitle: _getCurrentThemeText(),
            leading: Icon(Icons.brightness_6),
            onTap: () {
              _showThemeModeDialog();
            },
          ),
          ListTile(
            dense: true,
            title: Text(
              S.of(context).settingsAppearanceNumbers,
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
                fontSize: 15,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          ListTile(
            title: Text(S.of(context).settingsAppearanceNumberFormat),
            subtitle: Text(Util.formatPrice(12345.67)),
            leading: Icon(Icons.onetwothree),
            onTap: () {
              Navigator.of(context).push(Util.createSlidingRoute(NumberFormatWidget(
                onFormatLocaleChange: _onNumberFormatLocaleChange,
                setFormatLocale: _numberFormatLocale
              )));
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsAppearanceDateFormat),
            subtitle: Text(DateTime.now().localeFormat()),
            leading: Icon(Icons.date_range),
            onTap: () {
              Navigator.of(context).push(Util.createSlidingRoute(DateFormatWidget(
                onFormatLocaleChange: _onDateFormatLocaleChange,
                setFormatLocale: _dateFormatLocale
              )));
            },
          ),
        ],
      ),
    );
  }

  ///Shows a dialog to select the primary color
  void _showPrimaryColorDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).settingsAppearanceColorDialogTitle),
          content: SingleChildScrollView(
            child: BlockPicker(
              pickerColor: currentColor,
              onColorChanged: _changeColor,
              availableColors: Constants.primaryColors,
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(S.of(context).alertDialogCancel),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(S.of(context).alertDialogSelect),
              onPressed: () {
                setState(() => currentColor = pickerColor);
                colorChange.changePrimaryColor(pickerColor);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
    );
  }

  ///Called when tapping a new color in the color picker. Sets pickerColor to the tapped color
  void _changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  ///Returns the text to be displayed in the Theme list tile subtitle
  Text _getCurrentThemeText() {
    switch (_theme) {
      case Themes.SYSTEM: {
        return Text(S.of(context).settingsAppearanceThemeSystem);
      }
      case Themes.SYSTEM_AMOLED: {
        return Text(S.of(context).settingsAppearanceThemeSystemAmoled);
      }
      case Themes.LIGHT: {
        return Text(S.of(context).settingsAppearanceThemeLight);
      }
      case Themes.DARK: {
        return Text(S.of(context).settingsAppearanceThemeDark);
      }
      case Themes.AMOLED_DARK: {
        return Text(S.of(context).settingsAppearanceThemeAmoled);
      }
    }
  }

  ///Shows a dialog to select the app theme mode
  void _showThemeModeDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(S.of(context).settingsAppearanceThemeDialogTitle),
          children: [
            if (_sdkNumber >= 29) //system dark mode since Android 10
              SimpleDialogOption(
                child: RadioListTile<Themes>(
                    title: Text(S.of(context).settingsAppearanceThemeSystem),
                    activeColor: (currentTheme.isThemeDark()
                        ? Constants.accentColorDark
                        : Constants.accentColorLight),
                    value: Themes.SYSTEM,
                    groupValue: _theme,
                    onChanged: (Themes? value) {
                      setState(() {
                        _theme = value!;
                        currentTheme.changeTheme(_theme);
                        Navigator.pop(context);
                      });
                    }
                ),
              ),
            if (_sdkNumber >= 29) //system dark mode since Android 10
              SimpleDialogOption(
                child: RadioListTile<Themes>(
                    title: Text(S.of(context).settingsAppearanceThemeSystemAmoled),
                    activeColor: (currentTheme.isThemeDark()
                        ? Constants.accentColorDark
                        : Constants.accentColorLight),
                    value: Themes.SYSTEM_AMOLED,
                    groupValue: _theme,
                    onChanged: (Themes? value) {
                      setState(() {
                        _theme = value!;
                        currentTheme.changeTheme(_theme);
                        Navigator.pop(context);
                      });
                    }
                ),
              ),
            SimpleDialogOption(
              child: RadioListTile<Themes>(
                  title: Text(S.of(context).settingsAppearanceThemeLight),
                  activeColor: (currentTheme.isThemeDark()
                      ? Constants.accentColorDark
                      : Constants.accentColorLight),
                  value: Themes.LIGHT,
                  groupValue: _theme,
                  onChanged: (Themes? value) {
                    setState(() {
                      _theme = value!;
                      currentTheme.changeTheme(_theme);
                      Navigator.pop(context);
                    });
                  }
              ),
            ),
            SimpleDialogOption(
              child: RadioListTile<Themes>(
                  title: Text(S.of(context).settingsAppearanceThemeDark),
                  activeColor: (currentTheme.isThemeDark()
                      ? Constants.accentColorDark
                      : Constants.accentColorLight),
                  value: Themes.DARK,
                  groupValue: _theme,
                  onChanged: (Themes? value) {
                    setState(() {
                      _theme = value!;
                      currentTheme.changeTheme(_theme);
                      Navigator.pop(context);
                    });
                  }
              ),
            ),
            SimpleDialogOption(
              child: RadioListTile<Themes>(
                  title: Text(S.of(context).settingsAppearanceThemeAmoled),
                  activeColor: (currentTheme.isThemeDark()
                      ? Constants.accentColorDark
                      : Constants.accentColorLight),
                  value: Themes.AMOLED_DARK,
                  groupValue: _theme,
                  onChanged: (Themes? value) {
                    setState(() {
                      _theme = value!;
                      currentTheme.changeTheme(_theme);
                      Navigator.pop(context);
                    });
                  }
              ),
            ),
          ],
        );
      }
    );
  }

  ///Change displayed date format if it is changed in the DateFormatWidget
  void _onDateFormatLocaleChange(String locale) {
    setState(() {
      _dateFormatLocale = locale;
    });
  }

  ///Change displayed number format if it is changed in the NumberFormatWidget
  void _onNumberFormatLocaleChange(String locale) {
    setState(() {
      _numberFormatLocale = locale;
    });
  }
}