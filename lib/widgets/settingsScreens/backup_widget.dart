import 'dart:convert';
import 'dart:io';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/import_export.dart';
import 'package:crypto_prices/util/manual_migration_handler.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/migration_assistant.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

///Displays the Import and Export settings
class ImportExportWidget extends StatefulWidget {

  @override
  State<ImportExportWidget> createState() => _ImportExportWidgetState();
}

class _ImportExportWidgetState extends State<ImportExportWidget> {
  bool _autoBackupEnabled = false;
  String _autoBackupPath = "";
  String _autoBackupFileName = "";
  DateTime? _autoBackupDate;
  bool _autoBackupOverwriteOldFile = true;
  bool _isAuthenticated = false;

  SettingsDAO _settingsDAO = SettingsDAO();

  final TextEditingController _fileNameController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _autoBackupPath = _settingsDAO.getAutoBackupPath();
    _autoBackupEnabled =_autoBackupPath.isNotEmpty;
    _autoBackupDate = _settingsDAO.getAutoBackupDate();
    _autoBackupFileName = _settingsDAO.getAutoBackupFileName();
    _autoBackupOverwriteOldFile = _settingsDAO.getAutoBackupOverwriteFile();
    _isAuthenticated = _settingsDAO.getIsAuthenticated();
  }

  @override
  void dispose() {
    _fileNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(S.of(context).settingsImportExport)
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(S.of(context).settingsImportExportImportFileTitle),
            subtitle: Text(S.of(context).settingsImportExportImportFileSubtitle),
            onTap: () async {
              _importData();
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsImportExportExportFileTitle),
            subtitle: Text(S.of(context).settingsImportExportExportFileSubtitle),
            onTap: () {
              if (!_isAuthenticated) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(S.of(context).authenticationRequired),
                    duration: Duration(seconds: 2),
                  ),
                );
              } else {
                _exportData();
              }
            },
          ),
          SwitchListTile(
            title: Text(S.of(context).settingsImportExportAutoBackup),
            subtitle: Text(_getBackupDateText(_autoBackupDate)),
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _autoBackupEnabled,
            onChanged: (bool newValue) async {
              if (!_isAuthenticated) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(S.of(context).authenticationRequired),
                    duration: Duration(seconds: 2),
                  ),
                );
                return;
              }
              if (newValue) {
                final folderPath = await _selectFolder();
                if (folderPath.isNotEmpty) {
                  String fileName = _autoBackupFileName;
                  if (!_autoBackupOverwriteOldFile) {
                    fileName = await Util.getDuplicateFileName(fileName, folderPath);
                  }

                  setState(() {
                    _autoBackupEnabled = newValue;
                    _autoBackupPath = folderPath;
                    _autoBackupDate = DateTime.now();
                    _settingsDAO.setAutoBackupPath(folderPath);
                    _settingsDAO.setAutoBackupDate(DateTime.now());
                    String backupFilePath = folderPath + "/$fileName.json";
                    ImportExport().exportData(backupFilePath);
                  });
                }
              } else {
                setState(() {
                  _autoBackupEnabled = newValue;
                  _autoBackupPath = "";
                  _settingsDAO.setAutoBackupPath("");
                });
              }
            },
          ),
          if (_autoBackupEnabled)
            ListTile(
              title: Text(S.of(context).settingsAutoBackupLocation),
              subtitle: Text(_getBackupLocationText()),
              leading: Icon(Icons.folder),
              onTap: () async {
                if (!_isAuthenticated) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(S.of(context).authenticationRequired),
                      duration: Duration(seconds: 2),
                    ),
                  );
                  return;
                }
                final folderPath = await _selectFolder();
                if (folderPath.isNotEmpty) {
                  String fileName = _autoBackupFileName;
                  if (!_autoBackupOverwriteOldFile) {
                    fileName = await Util.getDuplicateFileName(fileName, folderPath);
                  }

                  setState(() {
                    _autoBackupPath = folderPath;
                    _autoBackupDate = DateTime.now();
                    _settingsDAO.setAutoBackupPath(folderPath);
                    _settingsDAO.setAutoBackupDate(DateTime.now());
                    String backupFilePath = folderPath + "/$fileName.json";
                    ImportExport().exportData(backupFilePath);
                  });
                }
              },
            ),
          if (_autoBackupEnabled)
            ListTile(
              title: Text(S.of(context).settingsAutoBackupFileName),
              subtitle: Text(_autoBackupFileName),
              leading: Icon(Icons.description),
              onTap: () async {
                _showRenameFileDialog();
              },
            ),
          if (_autoBackupEnabled)
            SwitchListTile(
              title: Text(S.of(context).settingsAutoBackupOverwriteTitle),
              subtitle: Text(S.of(context).settingsAutoBackupOverwriteSubtitle),
              isThreeLine: true,
              activeColor: Theme.of(context).colorScheme.secondary,
              value: _autoBackupOverwriteOldFile,
              onChanged: (bool newValue) async {
                setState(() {
                  _autoBackupOverwriteOldFile = newValue;
                  _settingsDAO.setAutoBackupOverwriteFile(newValue);
                });
              },
            ),

        ],
      ),
    );
  }

  ///Displays an alert to confirm data can be overwritten. Returns true if data can be overwritten
  Future<bool> _showOverwriteAlert() async {
    bool overwriteData = false;

    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).settingsImportExportOverwriteAlertTitle),
          content: Text(S.of(context).settingsImportExportOverwriteAlertMessage),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                overwriteData = true;
                Navigator.pop(context); //pop dialog
              },
              child: Text(S.of(context).alertDialogContinue)
            ),
          ],
        );
      }
    );
    return overwriteData;
  }

  ///Opens a load file dialog and imports favorites, alerts, portfolios and transactions from a json file
  void _importData() async {
    final params = OpenFileDialogParams(
      dialogType: OpenFileDialogType.document,
      mimeTypesFilter: ["application/json"]
    );
    String? importFilePath = await FlutterFileDialog.pickFile(params: params);
    final importExport = ImportExport();

    if(importFilePath != null) {
      if (await importExport.willOverwriteData(importFilePath)) {
        final overwriteData = await _showOverwriteAlert();

        if (!overwriteData) {
          return;
        }
      }

      try {
        await importExport.importData(importFilePath);

        if (ManualMigrationHandler().isManualMigrationNeeded()) {
          await Navigator.of(context).push(MaterialPageRoute(builder: (context) => MigrationAssistant()));
        }

        final snackBar = SnackBar(
          content: Text(S.of(context).settingsImportExportImportSuccessful),
          duration: Duration(seconds: 1),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } catch (e) {
        final snackBar = SnackBar(
          content: Text(S.of(context).settingsImportExportWrongFormat),
          duration: Duration(seconds: 1),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }
  }

  ///True if the file at the given path is from a previous database version
  Future<bool> _isLegacyImport(String importFilePath) async {
    File import = File(importFilePath);
    String jsonString = await import.readAsString();

    Map<String, dynamic> json = JsonDecoder().convert(jsonString);

    int importDatabaseVersion = json[Constants.DATABASEVERSIONKEY] ?? Constants.DATABASEVERSIONMULTIPLEPORTFOLIOS;

    if (importDatabaseVersion != Constants.DATABASEVERSION) {
      return true;
    } else {
      return false;
    }
  }

  ///Opens a save file dialog to export favorites, alerts, portfolios and transactions to a json file
  void _exportData() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String exportPath = appDocDir.path + "/${Constants.EXPORTFILENAME}.json";
    await ImportExport().exportData(exportPath);

    final params = SaveFileDialogParams(
      sourceFilePath: exportPath,
      fileName: Constants.EXPORTFILENAME,
      mimeTypesFilter: ["application/json"]
    );
    String? savedFilePath = await FlutterFileDialog.saveFile(params: params);
    File(exportPath).delete();

    if (savedFilePath != null) {
      final snackBar = SnackBar(
        content: Text(S.of(context).settingsImportExportExportSuccessful),
        duration: Duration(seconds: 1),
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  ///Opens the file picker to select a directory and returns its path
  Future<String> _selectFolder() async {
    var status = await Permission.storage.request();
    print(status.name);
    if (status.isGranted) {
      String? selectedDirectory = await FilePicker.platform.getDirectoryPath();

      if (selectedDirectory != null) {
        print(selectedDirectory);
        return selectedDirectory;
      }
    }

    bool showRationale = await Permission.storage.shouldShowRequestRationale;
    if (showRationale || status.isPermanentlyDenied) {
      await _showStoragePermissionRationale();
    }
    return "";
  }

  ///Shows the rationale for the storage permission
  Future<void> _showStoragePermissionRationale() async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).autoBackupPermissionRationaleTitle),
          content: Text(S.of(context).autoBackupPermissionRationaleText),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () async {
                var status = await Permission.storage.request();
                print(status.name);
                if (status.isPermanentlyDenied) {
                  openAppSettings();
                }

                Navigator.pop(context); //pop dialog
              },
              child: Text(S.of(context).alertDialogGrant)
            ),
          ],
        );
      }
    );
  }

  ///Returns the date of the last backup depending on how much time has passed
  String _getBackupDateText(DateTime? date) {
    if (date != null) {
      final difference = DateTime.now().difference(date);
      if (difference.inDays < 1) {
        if (difference.inMinutes < 5) {
          return S.of(context).autoBackupJustNow(date.getTimeOnly());
        } 
        
        if (difference.inMinutes < 60) {
          return S.of(context).autoBackupMinutes(difference.inMinutes) + date.getTimeOnly();
        }
        //# hours ago, hh:mm
        return S.of(context).autoBackupHours(difference.inHours) + date.getTimeOnly();
      } 
      
      if (difference.inDays < 2) {
        //yesterday, hh:mm
        return S.of(context).autoBackupYesterday(date.getTimeOnly());
      } 
      //dd.mm.yy, hh:mm
      return S.of(context).autoBackupDate(date.getDateOnly(), date.getTimeOnly());
    }
    return S.of(context).never;
  }

  ///Returns the backup location path text
  ///or a censored version if user is not authenticated
  String _getBackupLocationText() {
    if (!_isAuthenticated) {
      return "/*****/*****";
    }
    if (_autoBackupPath.isEmpty) {
      return S.of(context).autoBackupNoBackupLocation;
    } else {
      return _autoBackupPath;
    }
  }

  ///Displays the dialog to rename the backup file
  void _showRenameFileDialog() {
    final reservedChars = {"|", "\\", "?", "*", "<", "\"", ":", ">", "/"};
    final formKey = GlobalKey<FormState>();
    _fileNameController.text = _autoBackupFileName;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).autoBackupChangeFileName),
          content: Form(
            key: formKey,
            child: TextFormField(
              controller: _fileNameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: S.of(context).autoBackupEnterFileNameHint,
                errorStyle: TextStyle(
                  fontSize: 14,
                  color: (currentTheme.isThemeDark())
                      ? Constants.errorColorDark
                      : Constants.errorColor,
                ),
              ),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return S.of(context).autoBackupErrorNoFileName;
                }
                
                for(int i = 0; i < value.length; i++) {
                  if (reservedChars.contains(value[i])) {
                    return S.of(context).autoBackupErrorInvalidCharacter(value[i]);
                  }
                }
                return null;
              },
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                if (formKey.currentState!.validate()) {
                  setState(() {
                    _autoBackupFileName = _fileNameController.text;
                    _settingsDAO.setAutoBackupFileName(_fileNameController.text);
                  });
                  _fileNameController.text = "";
                  Navigator.pop(context); //pop dialog
                }
              },
              child: Text(S.of(context).alertDialogRename)
            ),
          ],
        );
      }
    );
  }
}