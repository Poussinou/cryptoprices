import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

///Displays donation methods
class DonationWidget extends StatefulWidget {
  @override
  _DonationWidgetState createState() => _DonationWidgetState();
}

class _DonationWidgetState extends State<DonationWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).donateTitle),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              _buildCard(
                title: S.of(context).bitcoin,
                children: [
                  ListTile(
                    title: Text(
                      Constants.btcDonationAddress
                    ),
                    leading: Image(
                      image: AssetImage("assets/images/currencies/btc.png"),
                      width: 25,
                      height: 25,
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.copy),
                      tooltip: S.of(context).copyToClipBoard,
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: Constants.btcDonationAddress));
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(S.of(context).copyToClipBoardAddressMessage),
                            duration: Duration(seconds: 2),
                          ),
                        );
                      },
                    ),
                  )
                ]
              ),
              _buildCard(
                title: S.of(context).ethereumAndTokens,
                children: [
                  ListTile(
                    title: Text(
                      Constants.ethDonationAddress
                    ),
                    leading: Image(
                      image: AssetImage("assets/images/currencies/eth.png"),
                      width: 25,
                      height: 25,
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.copy),
                      tooltip: S.of(context).copyToClipBoard,
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: Constants.ethDonationAddress));
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(S.of(context).copyToClipBoardAddressMessage),
                            duration: Duration(seconds: 2),
                          ),
                        );
                      },
                    ),
                  )
                ]
              ),
              _buildCard(
                title: S.of(context).liberapay,
                children: [
                  ListTile(
                    title: Text(S.of(context).monthlyDonation),
                    leading: Image(
                      image: AssetImage("assets/images/liberapay_icon.png"),
                      width: 25,
                      height: 25,
                    ),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () {
                      _launchUrl(Constants.liberapayUrl);
                    },
                  )
                ]
              )
            ],
          ),
        ),
      ),
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({
    required String title,
    required List<Widget> children
  }) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0),
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Launches an url in the browser
  void _launchUrl(String url) async =>
      await canLaunch(url)
          ? await launch(url)
          : _showAlertDialog(
          S.of(context).urlLaunchAlertDialogTitle,
          S.of(context).urlLaunchAlertDialogMessage
      );

  ///Displays an alert dialog with the given title and message
  void _showAlertDialog(String title, String message) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).alertDialogClose)
            )
          ],
        );
      }
    );
  }
}