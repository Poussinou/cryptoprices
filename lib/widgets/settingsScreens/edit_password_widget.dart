import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/material.dart';

///Shoes text fields to edit the password
class EditPasswordWidget extends StatefulWidget {
  const EditPasswordWidget({Key? key}) : super(key: key);

  @override
  _EditPasswordWidgetState createState() => _EditPasswordWidgetState();
}

class _EditPasswordWidgetState extends State<EditPasswordWidget> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _repeatPasswordController = TextEditingController();

  SettingsDAO _settingsDAO = SettingsDAO();

  bool _showPassword = false;
  String _password = "";
  bool _biometricsAvailable = false;
  bool _biometricsAllowed = true;

  @override
  void initState() {
    super.initState();

    _password = _settingsDAO.getPassword();
    _biometricsAvailable = _settingsDAO.getBiometricsAvailable();
    _biometricsAllowed = _settingsDAO.getBiometricsAllowed();
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _repeatPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).settingsSecurityChangePassword),
        actions: [
          IconButton(
            onPressed: () => _onDeleteButtonPressed(),
            icon: Icon(Icons.delete),
            tooltip: S.of(context).editPasswordRemove,
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildCard(
                  title: S.of(context).password,
                  children: [
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: TextFormField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                          border: OutlineInputBorder(),
                          hintText: S.of(context).editPasswordEnterHint,
                          suffixIcon: IconButton(
                            icon: Icon(_showPassword ? Icons.visibility_off : Icons.visibility),
                            tooltip: S.of(context).editPasswordShowPassword,
                            onPressed: () {
                              setState(() {
                                _showPassword = !_showPassword;
                              });
                            },
                          ),
                          errorStyle: TextStyle(
                            fontSize: 14,
                            color: (currentTheme.isThemeDark())
                                ? Constants.errorColorDark
                                : Constants.errorColor,
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        obscureText: !_showPassword,
                        autocorrect: false,
                        enableSuggestions: false,
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return S.of(context).editPasswordNoPasswordError;
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: TextFormField(
                        controller: _repeatPasswordController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                          border: OutlineInputBorder(),
                          hintText: S.of(context).editPasswordRepeatHint,
                          suffixIcon: IconButton(
                            icon: Icon(_showPassword ? Icons.visibility_off : Icons.visibility),
                            tooltip: S.of(context).editPasswordShowPassword,
                            onPressed: () {
                              setState(() {
                                _showPassword = !_showPassword;
                              });
                            },
                          ),
                          errorStyle: TextStyle(
                            fontSize: 14,
                            color: (currentTheme.isThemeDark())
                                ? Constants.errorColorDark
                                : Constants.errorColor,
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        obscureText: !_showPassword,
                        autocorrect: false,
                        enableSuggestions: false,
                        validator: (value) {
                          if (value != _passwordController.text) {
                            return S.of(context).editPasswordRepeatPasswordError;
                          }
                          return null;
                        },
                      ),
                    )
                  ]
                ),
                if (_biometricsAvailable)
                  SwitchListTile(
                    title: Text(S.of(context).settingsSecurityBiometricUnlock),
                    secondary: Icon(Icons.fingerprint),
                    activeColor: Theme.of(context).colorScheme.secondary,
                    value: _biometricsAllowed,
                    onChanged: (bool newValue) {
                      setState(() {
                        _biometricsAllowed = newValue;
                      });
                    }
                  )
              ],
            ),
          ),
        )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: _validateForm
      ),
    );
  }

  ///Opens an alert dialog to confirm removal of the password lock.
  ///If no password is set, close the widget instead
  void _onDeleteButtonPressed() {
    if (_password.isEmpty) {
      Navigator.pop(context);
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).editPasswordRemoveDialogTitle),
            content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).alertDialogCancel)
              ),
              TextButton(
                onPressed: () {
                  _settingsDAO.setPassword(Constants.passwordDefault);
                  Navigator.pop(context); //pop dialog
                  Navigator.pop(context, Constants.DELETERESULT); //pop widget
                },
                child: Text(S.of(context).alertDialogRemove)
              ),
            ],
          );
        }
      );
    }
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children, EdgeInsets textPadding = const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0)}) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: textPadding,
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Checks if the Form is valid
  void _validateForm() {
    if (_formKey.currentState!.validate()) {
      _settingsDAO.setPassword(_passwordController.text);
      _settingsDAO.setBiometricsAllowed(_biometricsAllowed);
      _settingsDAO.setIsAuthenticated(true);
      Navigator.pop(context, Constants.successResult);
    }
  }
}
