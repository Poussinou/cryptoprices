import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/material.dart';

///Displays all available languages
class LanguageWidget extends StatefulWidget {
  LanguageWidget({Key? key, required this.onLanguageChanged});

  ///notifies the parent which language has been selected
  final ValueChanged<String> onLanguageChanged;

  @override
  _LanguageWidgetState createState() => _LanguageWidgetState();
}

class _LanguageWidgetState extends State<LanguageWidget> {
  String _locale = SettingsDAO().getLocaleHive();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).selectLanguageTitle),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text(
                S.of(context).settingsEnglish
            ),
            trailing: (_locale == "en") ? Icon(Icons.check) : null,
            onTap: () => _changeLanguage("en"),
          ),
          ListTile(
            title: Text(
                S.of(context).settingsGerman
            ),
            trailing: (_locale == "de") ? Icon(Icons.check) : null,
            onTap: () => _changeLanguage("de"),
          ),
        ],
      ),
    );
  }

  ///Changes the language of the app with the LanguageChange notifier
  void _changeLanguage(String locale) {
    languageChange.changeLanguage(locale);
    setState(() {
      _locale = locale;
      widget.onLanguageChanged(locale);
      Navigator.of(context).pop();
    });
  }
}