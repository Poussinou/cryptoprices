import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/manual_migration_handler.dart';
import 'package:crypto_prices/widgets/settingsScreens/migration_data_entry_widget.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

///Helps with the migration of coin ids
class MigrationAssistant extends StatefulWidget {
  static const routeName = "/migrationAssistant";

  const MigrationAssistant({
    Key? key
  }) : super(key: key);

  @override
  _MigrationAssistantState createState() => _MigrationAssistantState();
}

class _MigrationAssistantState extends State<MigrationAssistant> {
  TextStyle _normalFont = TextStyle(fontSize: 16);

  int _page = 1;
  Map<String, String> _oldIdToNewId = {};

  ManualMigrationHandler _migrationHandler = ManualMigrationHandler();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(title: Text(S.of(context).migrationAssistantTitle)),
        body: Container(
          child: _buildPage(),
          padding: EdgeInsets.all(8.0),
        ),
      ),
      onWillPop: () async {
        bool closeAssistant = false;

        if (_page == 2) {
          setState(() {
            _page--;
          });
        } else {
          await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(S.of(context).alertDialogCloseAndDelete),
                content: Text(S.of(context).migrationAssistantAlertDialogMessage),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(S.of(context).alertDialogCancel)
                  ),
                  TextButton(
                    onPressed: () {
                      _migrationHandler.getAllDataToMigrate().forEach((element) {
                        _migrationHandler.deleteCoinIdData(element.oldCoinId);
                      });
                      closeAssistant = true;
                      Navigator.pop(context); //pop dialog
                    },
                    child: Text(S.of(context).alertDialogDelete)
                  ),
                ],
              );
            }
          );
        }
        return closeAssistant;
      }
    );
  }

  Widget _buildPage() {
    switch (_page) {
      case 1: {
        return _page1();
      }

      case 2: {
        return _page2();
      }

      default: {
        return Container();
      }
    }
  }

  ///Builds page 1
  Widget _page1() {
    return Column(
      children: [
        Text(
          S.of(context).migrationAssistantText1,
          style: _normalFont,
        ),
        SizedBox(height: 10),
        Text(
          S.of(context).migrationAssistantText2,
          style: _normalFont,
        ),
        SizedBox(height: 10),
        Text(
          S.of(context).migrationAssistantText3,
          style: _normalFont,
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Container(),
            TextButton(
              child: Text(S.of(context).migrationAssistantContinueButton, style: _normalFont,),
              onPressed: () {
                setState(() {
                  _page++;
                });
              },
            )
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        )
      ],
    );
  }

  ///Builds page 2
  Widget _page2() {
    final _dataToMigrate = _migrationHandler.getAllDataToMigrate();
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Column(
        children: [
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _dataToMigrate.length,
            itemBuilder: (context, i) {
              String oldName = "";
              String oldSymbol = "";
              if (_dataToMigrate[i].oldCoinName.isNotEmpty){
                oldName = _dataToMigrate[i].oldCoinName;
                oldSymbol = _dataToMigrate[i].oldCoinSymbol;
              } else {
                oldName = _dataToMigrate[i].oldCoinId;
              }
              return MigrationDataEntryWidget(
                oldCoinId: _dataToMigrate[i].oldCoinId,
                oldCoinName: oldName,
                oldCoinSymbol: oldSymbol,
                onCoinMigrate: _onCoinMigrate,
                newCoinId: _oldIdToNewId[_dataToMigrate[i].oldCoinId] ?? "",
              );
            }
          ),
          Row(
            children: [
              TextButton(
                child: Text(S.of(context).migrationAssistantBackButton, style: _normalFont,),
                onPressed: () {
                  setState(() {
                    _page--;
                  });
                },
              ),
              if (_isDataMigrated())
                TextButton(
                  child: Text(S.of(context).migrationAssistantFinishButton, style: _normalFont,),
                  onPressed: () {
                    setState(() {
                      _migrateCoins();
                      Navigator.pop(context);
                    });
                  },
                )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          )
        ],
      ),
    );
  }

  ///Adds the old coin id and the new coin id, to which it is being migrated, to a map
  ///or removes them when the selection is cancelled.
  ///Called when a new coin id is selected
  void _onCoinMigrate(Tuple2<String, String> oldIdNewId) {
    setState(() {
      if (oldIdNewId.item2.isNotEmpty) {
        _oldIdToNewId[oldIdNewId.item1] = oldIdNewId.item2;
      } else {
        _oldIdToNewId.remove(oldIdNewId.item1);
      }
    });
  }

  ///Migrates all old coin ids to the new coin ids
  void _migrateCoins() {
    _oldIdToNewId.forEach((oldCoinId, newCoinId) {
      _migrationHandler.migrateCoinId(oldCoinId, newCoinId);
    });
  }

  ///True if new coin ids where selected for all incorrect data
  bool _isDataMigrated() {
    return _oldIdToNewId.length == _migrationHandler.getAllDataToMigrate().length;
  }
}
