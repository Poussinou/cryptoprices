import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

///Displays the notification settings
class NotificationsWidget extends StatefulWidget {
  @override
  _NotificationsWidgetState createState() => _NotificationsWidgetState();
}

class _NotificationsWidgetState extends State<NotificationsWidget> {
  SettingsDAO _settingsDAO = SettingsDAO();

  String _largeMoveThreshold = SettingsDAO().getLargeMoveAlertThresholdHive().toString();

  TextEditingController _thresholdController = TextEditingController();

  @override
  void dispose() {
    _thresholdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).settingsNotifications)
      ),
      body: ListView(
        children: <Widget>[
          SwitchListTile(
            title: Text(S.of(context).settingsNotificationsShowPriceTitle),
            subtitle: Text(S.of(context).settingsNotificationsShowPriceSubTitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getShowPriceInPercentageAlertsHive(),
            onChanged: (bool enabled) {
              setState(() {
                _settingsDAO.setShowPriceInPercentageAlerts(enabled);
              });
            },
          ),
          ListTile(
            dense: true,
            title: Text(
              S.of(context).settingsNotificationsLargePriceMoveAlerts,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          SwitchListTile(
            title: Text(S.of(context).settingsNotificationsEnableMoveAlerts),
            subtitle: Text(S.of(context).settingsNotificationsEnableAlertsText),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getLargeMoveAlertsEnabledHive(),
            onChanged: (bool enabled) {
              setState(() {
                _settingsDAO.setLargeMoveAlertsEnabled(enabled);
              });
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsNotificationsMoveThreshold),
            subtitle: Text("${Util.formatPercentage(_largeMoveThreshold.parseDouble())}%"),
            onTap: () {
              _showThresholdDialog();
            },
          ),
        ],
      ),
    );
  }

  ///Shows a dialog to enter the large move alert threshold
  void _showThresholdDialog() {
    final formKey = GlobalKey<FormState>();
    _thresholdController.text = _largeMoveThreshold.toString();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).settingsNotificationsMoveThreshold),
          content: Form(
            key: formKey,
            child: TextFormField(
              controller: _thresholdController,
              decoration: InputDecoration(
                suffix: Text("%"),
                border: OutlineInputBorder(),
                hintText: S.of(context).settingsNotificationsMoveThresholdHint,
                errorStyle: TextStyle(
                  fontSize: 14,
                  color: (currentTheme.isThemeDark())
                      ? Constants.errorColorDark
                      : Constants.errorColor,
                ),
              ),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return S.of(context).alertErrorText;
                }

                if (!Constants.positiveNumberExp.hasMatch(value) || value.parseDouble() == 0) {
                  return S.of(context).alertErrorText;
                }
                return null;
              },
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                if (formKey.currentState!.validate()) {
                  setState(() {
                    _largeMoveThreshold = _thresholdController.text;
                    _settingsDAO.setLargeMoveAlertThreshold(_thresholdController.text.parseDouble());
                  });
                  Navigator.pop(context); //pop dialog
                }
              },
              child: Text(S.of(context).alertDialogSelect)
            ),
          ],
        );
      }
    );
  }
}