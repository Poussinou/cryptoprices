import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/material.dart';

///Displays all available number formats
class NumberFormatWidget extends StatefulWidget {
  ///Notifies the parent which date format locale has been selected
  final ValueChanged<String> onFormatLocaleChange;

  ///The date format locale that is set on widget launch
  final String setFormatLocale;

  const NumberFormatWidget({
    Key? key,
    required this.onFormatLocaleChange,
    required this.setFormatLocale
  }) : super(key: key);

  @override
  _NumberFormatWidgetState createState() => _NumberFormatWidgetState();
}

class _NumberFormatWidgetState extends State<NumberFormatWidget> {
  String _locale = "";

  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();
    _locale = widget.setFormatLocale;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).settingsAppearanceNumberFormatTitle),
      ),
      body: ListView.builder(
        itemCount: Constants.numberFormatLocales.length,
        itemBuilder: (context, i) {
          final locale = Constants.numberFormatLocales[i];
          return ListTile(
            title: Text(
              Util.formatPrice(12345.67, locale: locale)
            ),
            trailing: (_locale == locale) ? Icon(Icons.check) : null,
            onTap: () => _changeLocale(locale),
          );
        }
      ),
    );
  }

  ///Changes the format locale selected in the list and notifies the parent widget of the new value
  void _changeLocale(String locale) {
    setState(() {
      _locale = locale;
      widget.onFormatLocaleChange(locale);
      _settingsDAO.setNumberFormatLocale(locale);
      Navigator.of(context).pop();
    });
  }
}
