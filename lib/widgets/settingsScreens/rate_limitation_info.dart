import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/donation_widget.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

///Displays information about the API request rate limitation
class RateLimitationInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).rateLimitInfoTitle),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              _buildCard(
                title: S.of(context).rateLimitInfoExplanation,
                titleColor: Theme.of(context).colorScheme.secondary,
                children: [
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        Text(
                          S.of(context).rateLimitInfoExplanationFirst,
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Text(
                          S.of(context).rateLimitInfoExplanationSecond,
                          style: TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                  )
                ]
              ),
              SizedBox(height: 16),
              _buildCard(
                title: S.of(context).rateLimitInfoSolution,
                titleColor: Theme.of(context).colorScheme.secondary,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          S.of(context).rateLimitInfoSolutionFirst,
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Text(
                          S.of(context).rateLimitInfoSolutionSecond,
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Text(
                          S.of(context).rateLimitInfoSolutionThird,
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Text(
                          S.of(context).rateLimitInfoSolutionFourth,
                          style: TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                  ),ListTile(
                    title: Text(
                        S.of(context).rateLimitInfoCoinGeckoPrices
                    ),
                    leading: Image(
                      image: AssetImage("assets/images/coinGecko_logo.png"),
                      width: 25,
                      height: 25,
                    ),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () {
                      _launchUrl(Constants.coinGeckoPricingUrl, context);
                    },
                  ),
                  ListTile(
                    title: Text(
                        S.of(context).donateTitle
                    ),
                    leading: Icon(Icons.paid),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () {
                      Navigator.of(context).push(Util.createSlidingRoute(DonationWidget()));
                    },
                  )
                ]
              )
            ],
          ),
        ),
      )
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({
    required String title,
    required Color titleColor,
    required List<Widget> children
  }) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0),
            child: Text(
              title,
              style: TextStyle(
                color: titleColor,
                fontSize: 15.0,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }

  ///Launches an url in the browser
  void _launchUrl(String url, BuildContext context) async =>
      await canLaunch(url)
          ? await launch(url)
          : _showAlertDialog(
          S.of(context).urlLaunchAlertDialogTitle,
          S.of(context).urlLaunchAlertDialogMessage,
          context
      );

  ///Displays an alert dialog with the given title and message
  void _showAlertDialog(String title, String message, BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(S.of(context).alertDialogClose)
              )
            ],
          );
        }
    );
  }
}
