
import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:flutter/material.dart';

import '../../database/available_coins_dao.dart';
import '../../generated/l10n.dart';

///Displays a search page to search all available coins
class SearchAvailableCoinsDelegate extends SearchDelegate<AvailableCoin?> {
  BuildContext _context;

  AvailableCoinsDao _availableCoinsDao = AvailableCoinsDao();
  ApiInteraction api = CoinGeckoAPI();

  SearchAvailableCoinsDelegate(this._context);

  @override
  ThemeData appBarTheme(BuildContext context) {
    ThemeData theme = Theme.of(context).copyWith(
      textTheme: TextTheme(headline6: TextStyle(color: Colors.white, fontSize: 20)),
      inputDecorationTheme: InputDecorationTheme(hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)))

    );
    return theme;
  }

  @override
  String get searchFieldLabel => S.of(_context).searchFieldHintText;

  @override
  List<Widget> buildActions(BuildContext context) => [IconButton(icon: Icon(Icons.clear), onPressed: () => query = "")];

  @override
  Widget buildLeading(BuildContext context) => IconButton(icon: Icon(Icons.chevron_left), onPressed: () => close(context, null));

  @override
  Widget buildResults(BuildContext context) {
    return _buildSearchList();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _buildSearchList();
  }

  ///Returns a list of entries that fit the current search query
  Widget _buildSearchList() {
    List<AvailableCoin> coinList = _availableCoinsDao.getAllAvailableCoins();
    List<AvailableCoin> searchHistoryList = _availableCoinsDao.getSearchHistory();
    //show last searched on top of the current search
    coinList = _addHistoryToSearchList(coinList, searchHistoryList)..sort(CoinComparators.lastSearched);

    List<AvailableCoin> listToShow = [];
    if (query.isNotEmpty) {
      listToShow = coinList.where((element) => element.name.toLowerCase().contains(query.toLowerCase())
          || element.symbol.toLowerCase().startsWith(query.toLowerCase())).toList();
    } else {
      listToShow = searchHistoryList;
    }

    return ListView.builder(
      itemCount: listToShow.length,
      itemBuilder: (context, i) {
        AvailableCoin coin = listToShow[i];
        return ListTile(
          title: Text("${coin.name} (${coin.symbol.toUpperCase()})"),
          leading: CachedNetworkImage(
            imageUrl: ImageUrls().getSmallImageUrl(coin.id),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error_outline),
            width: 25,
            height: 25,
          ),
          trailing: ((coin.lastSearched != null) ? Icon(Icons.history) : Container(width: 0, height: 0,)),
          onTap: () {
            _availableCoinsDao.addToSearchHistory(coin);
            close(context, coin); //return selected coin
          },
          onLongPress: () {
            if (coin.lastSearched != null) {
              _showDeleteHistoryEntryDialog(coin);
            }
          },
        );
      }
    );
  }

  ///Add the entries of the search history (where lastSearched != null) to the search list.
  ///Remove the old entries in the search list where lastSearched == null
  List<AvailableCoin> _addHistoryToSearchList(List<AvailableCoin> searchList, List<AvailableCoin> historyList) {
    historyList.map((historyElement) =>
        searchList.removeWhere((searchListElement) => searchListElement.id == historyElement.id)
    ).toList();
    searchList.addAll(historyList);
    return searchList;
  }

  ///Shows a dialog to delete an entry from the search history
  void _showDeleteHistoryEntryDialog(AvailableCoin coin) {
    showDialog(
      context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(coin.name),
          content: Text(S.of(context).deleteSearchEntryDialogMessage),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
                onPressed: () {
                  _availableCoinsDao.deleteSearchedCoin(coin.id);
                  this.query = this.query; //build new suggestions
                  Navigator.pop(context); //pop dialog
                },
                child: Text(S.of(context).alertDialogDelete)
            ),
          ],
        );
      }
    );
  }
}