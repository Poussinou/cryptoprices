import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/material.dart';

///Displays all available start screens
class StartScreenWidget extends StatefulWidget {
  ///notifies the parent which start screen has been selected
  final ValueChanged<StartScreen> onStartScreenChanged;

  const StartScreenWidget({
    Key? key,
    required this.onStartScreenChanged
  }) : super(key: key);

  @override
  _StartScreenWidgetState createState() => _StartScreenWidgetState();
}

class _StartScreenWidgetState extends State<StartScreenWidget> {
  StartScreen _startScreen = SettingsDAO().getStartScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).selectStartScreenTitle),
      ),
      body: ListView.builder(
        itemCount: StartScreen.values.length,
        itemBuilder: (context, i) {
          return ListTile(
            title: Text(
              Util.startScreenToName(StartScreen.values[i], context)
            ),
            leading: Constants.TABICONS[i],
            trailing: (_startScreen == StartScreen.values[i]) ? Icon(Icons.check) : null,
            onTap: () => _changeStartScreen(StartScreen.values[i]),
          );
        }),
    );
  }

  ///Changes the start screen of the app and notifies the ValueNotifier
  void _changeStartScreen(StartScreen startScreen) {
    setState(() {
      _startScreen = startScreen;
      SettingsDAO().setStartScreen(startScreen);
      widget.onStartScreenChanged(startScreen);
      Navigator.of(context).pop();
    });
  }
}
